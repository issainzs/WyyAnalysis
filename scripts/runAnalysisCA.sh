#! /bin/bash
#
# Script for running locally
#
nentry=10000

#Monte Carlo Samples
#W(lnu)yy - Signal 
#analyses="enuyyMC"
#analyses="munuyyMC"

#Z(ll)y - Bkg
#analyses="eeyyMC"
#analyses="mumuyyMC"

#Z(ll)y - Bkg
#analyses="eeyMC"
#analyses="mumuyMC"

#Data 
#analyses="enuyyData"
#analyses="munuyyData"

#Trigger efficiencies
analyses="eyyTrigLegacy"
#analyses="eyyTrigPhaseI"
#analyses="muyyTrigLegacy"


# For algs that need to set parameters provide single string with triple commas to separate args (no spaces allowed)
electronAnalysisDict="ElectronChannel:True,,,MuonChannel:False,,,"
muonAnalysisDict="ElectronChannel:False,,,MuonChannel:True"


# make a list of analysis argument dictionaries
analysesDictList=""

for analysis in $analyses; do 
    echo "Adding analysis" $analysis
    if [[ "$analysis" == *"enuyyMC"* ]]; then
	analysesDictList+=" $electronAnalysisDict"
	#infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/Signal/mc23_13p6TeV.700870.Sh_2214_enugammagamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/*
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/Signal/mc23_13p6TeV.700870.Sh_2214_enugammagamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/DAOD_PHYSLITE.35009800._000024.pool.root.1
	outfile=physlite.log
   	elif [[ "$analysis" == *"munuyyMC"* ]]; then
	analysesDictList+=" $muonAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/Signal/mc23_13p6TeV.700871.Sh_2214_munugammagamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/*
	outfile=physlite.log
    elif [[ "$analysis" == *"eeyyMC"* ]]; then
	analysesDictList+=" $electronAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/bkg/mc23_13p6TeV.700873.Sh_2214_eegammagamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/*
	outfile=physlite.log
    elif [[ "$analysis" == *"mumuyyMC"* ]]; then
	analysesDictList+=" $muonAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/bkg/mc23_13p6TeV.700874.Sh_2214_mumugammagamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/*
	outfile=physlite.log
    elif [[ "$analysis" == *"eeyMC"* ]]; then
	analysesDictList+=" $electronAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/bkg/mc23_13p6TeV.700770.Sh_2214_eegamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/* 
	outfile=physlite.log
    elif [[ "$analysis" == *"mumuyMC"* ]]; then
	analysesDictList+=" $muonAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/bkg/mc23_13p6TeV.700771.Sh_2214_mumugamma.deriv.DAOD_PHYSLITE.e8514_e8528_s4162_s4114_r14622_r14663_p5855/* 
	outfile=physlite.log
	
	
    elif [[ "$analysis" == *"enuyyData"* ]]; then
	analysesDictList+=" $electronAnalysisDict"
	#infiles=/data/silo01/users/isabssd/datasets/PHYS/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp22_v03_p5858/* 
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/data_PHYSLITE/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp22_v03_p5858/DAOD_PHYSLITE.34859691._000015.pool.root.1
	outfile=physlite.log
    elif [[ "$analysis" == *"munuyyData"* ]]; then
	analysesDictList+=" $muonAnalysisDict"
	#infiles=/data/silo01/users/isabssd/datasets/PHYS/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp22_v03_p5858/* 
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/data_PHYSLITE/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp22_v03_p5858/DAOD_PHYSLITE.34859691._000015.pool.root.1
	outfile=physlite.log
	
    elif [[ "$analysis" == *"eyyTrigLegacy"* ]]; then
	analysesDictList+=" $electronAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/data_PHYSLITE/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp22_v03_p5858/DAOD_PHYSLITE.34859691._000015.pool.root.1
	outfile=physlite.log
	
    elif [[ "$analysis" == *"muyyTrigLegacy"* ]]; then
	analysesDictList+=" $muonAnalysisDict"
	infiles=/data/silo01/users/isabssd/WyyAnalysisSamples/data_PHYSLITE/data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYSLITE.grp22_v03_p5858/DAOD_PHYSLITE.34859691._000015.pool.root.1
	outfile=physlite.log
    
    
    else
	analysesDictList+=" ,,,"
	outfile=empty.log
    
    fi
done 


echo "Analyses: $analyses"
echo "Analyses args: $analysesDictList"
echo "Input files: $infiles"
echo "logfile: $outfile"
echo "nentry: $nentry"

python -m WyyAnalysis.WyyAnalysisConfig --filesInput=${infiles} --evtMax ${nentry} --outputLevel INFO \
    --analyses $analyses --analysesArgs $analysesDictList\
    2>&1 | tee $outfile

#athena ../sourceTest/WyyAnalysis/python/WyyAnalysisConfig.py --filesInput=${infiles} --debug exec \
  # 2>&1 | tee $outfile
