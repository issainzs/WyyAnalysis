#include <WyyAnalysis/WyyMCAnalysis.h>
#include <WyyAnalysis/WyyDataAnalysis.h>
#include <WyyAnalysis/WyyTrigEfficiency.h>

DECLARE_COMPONENT (WyyMCAnalysis)
DECLARE_COMPONENT (WyyDataAnalysis)
DECLARE_COMPONENT (WyyTrigEfficiency)