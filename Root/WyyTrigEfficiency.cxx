#include <AsgMessaging/MessageCheck.h>
#include <WyyAnalysis/WyyTrigEfficiency.h>
#include <TH1.h>
#include <TProfile.h>

#include "StoreGate/ReadHandleKey.h"

#include <xAODEventInfo/EventInfo.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODTrigger/EmTauRoIContainer.h"
#include "xAODTrigger/EmTauRoI.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/Photon.h"
#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"
#include "xAODEgamma/EgammaDefs.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTrigL1Calo/TriggerTowerAuxContainer.h"

#include "IsolationSelection/IIsolationSelectionTool.h"
#include "xAODCore/ShallowCopy.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
//#include "ElectronPhotonSelectorTools/EGammaAmbiguityTool.h"

#include <math.h>
#include <vector>
#include <bitset>
#include <boost/any.hpp>
//#include <unordered_map>



WyyTrigEfficiency :: WyyTrigEfficiency (const std::string& name, //the vars called here come from the headers that we included
  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
  m_isolationSelectionTool("CP::IsolationSelectionTool/IIsolationSelectionTool", this),
  m_MuonSelectionTool("CP::MuonSelectionTool/MuonSelectionTool", this),
  //m_EgammaCalibrationAndSmearingTool("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
  //m_MuonCalibTool("CP::MuonCalibTool/MuonCalibTool", this),
  m_vTrigChainNames({})
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
    
    //declareProperty( "EgammaCalibrationAndSmearingTool", m_EgammaCalibrationAndSmearingTool);
    //declareProperty( "MuonCalibTool", m_MuonCalibTool);
    declareProperty("IsolationSelectionTool", m_isolationSelectionTool);
    declareProperty( "MuonSelectionTool", m_MuonSelectionTool);
    
    declareProperty("SelectionLabel", m_selectionLabel="selected", "Input label for the OverlapRemovalTool");
    declareProperty("OverlapLabel", m_overlapLabel="overlaps", "Output label for the OverlapRemovalTool");

  }

  Float_t DR (float eta1, float phi1, float eta2, float phi2){
    float deta=fabs(eta1-eta2);
    float dphi=fabs(phi1-phi2);
    if(dphi>M_PI){dphi=2*M_PI-dphi;}
    return sqrt(pow(deta,2)+pow(dphi,2));
  }
  
  

  StatusCode WyyTrigEfficiency :: initialize ()
  {
    ANA_MSG_DEBUG ("Initializing - Wyy Trigger Studies");
    if (m_ElectronChannel){
      ANA_MSG_INFO("eyy chain Analysis");
      
    } else if (m_MuonChannel){
      ANA_MSG_INFO("muyy chain Analysis");
    }
    if (m_PHYS){
      ANA_MSG_INFO ("DAOD format used: PHYS");
    } else {
      ANA_MSG_INFO ("DAOD format used: PHYSLITE");
    }
    
    
    ANA_MSG_INFO ("Trigger system enabled: ");
    if (m_Legacy){
      ANA_MSG_INFO("Legacy");
    } else if (m_PhaseI){
      ANA_MSG_INFO("Phase I");
    } else if ( !m_Legacy && !m_PhaseI ){
      ANA_MSG_ERROR("\n\n Neither Legacy nor PhaseI is enabled \n");
      return StatusCode::FAILURE;
    }

    
    // Retrieve the trigger decision tool if requested
    if ( !m_trigDecTool.empty() ) {
      ANA_CHECK( m_trigDecTool.retrieve() );
      ANA_MSG_DEBUG( "TDT retrieved" );
      
      if (!m_r3MatchingTool.empty()) {
	ANA_CHECK(m_r3MatchingTool.retrieve());
	ANA_MSG_DEBUG( "MT retrieved" );
      }
    }
    
    ANA_CHECK(m_ElectronContainerKey.initialize());
    ANA_CHECK(m_PhotonContainerKey.initialize());
    ANA_CHECK(m_MuonContainerKey.initialize());
    ANA_CHECK(m_JetPflowContainerKey.initialize());
    ANA_CHECK(m_VertexContainerKey.initialize());
    ANA_CHECK(m_isolationSelectionTool.retrieve());
    ANA_CHECK(m_MuonSelectionTool.retrieve()); 
    ANA_CHECK(m_orTool.retrieve());
      
    
    if (m_PHYS){
      ANA_CHECK(m_EgammaCalibrationAndSmearingTool.retrieve());   
      ANA_CHECK(m_MuonCalibTool.retrieve()); 
      
      // Setting properties for the Jet Calibration tool - Taken from Felix code
      //tbh, not sure if i need this in the first place, since we only need the container to check for overlaps
      TString jetAlgo = "AntiKt4EMPFlow";
      TString config = "PreRec_R22_PFlow_ResPU_EtaJES_GSC_February23_230215.config";
      TString calibSeq = "JetArea_Residual_EtaJES_GSC_Insitu";
      TString calibArea = "00-04-82";
      bool isData = true;
      
      m_JetCalibrationTool.setTypeAndName("JetCalibrationTool/name");
      if( !m_JetCalibrationTool.isUserConfigured() ){
	ANA_CHECK( m_JetCalibrationTool.setProperty("JetCollection",jetAlgo.Data()) );
	ANA_CHECK( m_JetCalibrationTool.setProperty("ConfigFile",config.Data()) );
	ANA_CHECK( m_JetCalibrationTool.setProperty("CalibSequence",calibSeq.Data()) );
	ANA_CHECK( m_JetCalibrationTool.setProperty("CalibArea",calibArea.Data()) );
	ANA_CHECK( m_JetCalibrationTool.setProperty("IsData",isData) );
	ANA_CHECK( m_JetCalibrationTool.retrieve() );
      }
      m_JetCleaningTool =  new JetCleaningTool(JetCleaningTool::LooseBad,false);
      ANA_CHECK(m_JetCleaningTool->initialize());
    }
    
      
    // If the trigger chain is specified, parse it into a list.
    if ( m_triggerChainString!="" ) {
      StatusCode sc = parseList(m_triggerChainString,m_vTrigChainNames);
      // print outs like this cause trouble for AnalysisBase
      ANA_MSG_DEBUG("in parse list"<<m_triggerChainString);
      ANA_MSG_DEBUG("out parse list"<<m_vTrigChainNames);
      if ( !sc.isSuccess() ) {
	ANA_MSG_WARNING("Error parsing trigger chain list, using empty list instead.");
	m_vTrigChainNames.clear();
      }
    }
    

    //GENERAL
    ANA_CHECK (book (TH1F ("h_Cutflow_selection_lyy", "Cutflow", 6, 0, 6)));
    
    if (m_PHYS){
      ANA_CHECK (book (TH1F ("h_AllPhotonspt_noCalib", "All Photons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllElectronspt_noCalib", "All Electrons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllMuonspt_noCalib", "All Muons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllTauJetspt_noCalib", "All Tau Jets", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllJetspt_noCalib", "All Jets before calibration", 50, 0., 200)));
    }
    
    ANA_CHECK (book (TH1F ("h_AllPhotonspt", "All Photons after EC", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllPhotonseta", "All Photons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllPhotonsphi", "All Photons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberPhotons", "Number of Photons after EC", 13, 0, 13)));
    ANA_CHECK (book (TH1F ("h_AllElectronspt", "All Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllElectronseta", "All Electrons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllElectronsphi", "All Electrons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_AllElectronscharge", "All Electrons Charge", 3, -1.0, 2.0)));
    ANA_CHECK (book (TH1F ("h_NumberElectrons", "Number of Electrons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_AllMuonspt", "All Muons after EC", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllMuonseta", "All Muons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllMuonsphi", "All Muons Phi", 32, -3.2, 3.2))); 
    ANA_CHECK (book (TH1F ("h_AllMuonscharge", "All Muons Charge", 3, -1.0, 2.0)));
    ANA_CHECK (book (TH1F ("h_NumberMuons", "Number of Muons", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_AllTauJetspt", "All Tau Jets", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllTauJetseta", "All Tau Jets Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllTauJetsphi", "All Tau Jets Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTauJets", "Number of Tau Jets", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_AllJetspt", "All Jets - PFlow", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllJetseta", "All PFlow Jets Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllJetsphi", "All PFlow Jets Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberJets", "Number of PFlow Jets", 10, 0, 10)));
    
    //Overlaps
    ANA_CHECK (book (TH1F("h_DRej", "DR electrons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgj", "DR photons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRge", "DR photons to electrons", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgmu", "DR photons to muons", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmue", "DR muons to electrons", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmuj", "DR muons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRej_noOL", "DR electrons to jets - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgj_noOL", "DR photons to jets - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRge_noOL", "DR photons to electrons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgmu_noOL", "DR photons to muons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmue_noOL", "DR muons to electrons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmuj_noOL", "DR muons to jets - after OR", 42, -0.2, 4.)));
    
    ANA_CHECK (book (TH1F ("h_onlygampt", "Baseline Photons - after OR", 50, 0., 200))); //=noOL
    ANA_CHECK (book (TH1F ("h_onlyelept", "Preselect Electrons - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlymupt", "Preselect Muons - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlyjetpt", "Baseline PFlow Jets - after OR", 50, 0., 200)));
    
    //PHOTONS
    ANA_CHECK (book (TH1F ("h_gCutflow_selection", "Photon Selection Cutflow", 8, 0, 8)));
    ANA_CHECK (book (TH1F ("h_NumbergoodPhotons", "Number of Photons after selection", 7, 0, 7)));
    ANA_CHECK (book (TH1F ("h_goodphotonspt", "Photons after selection", 50, 0., 200)));
    
    ANA_CHECK (book (TH1F ("h_gamID_Tests", "GamID Histo", 3, 0, 3)));
    ANA_CHECK (book (TH1F ("h_gamAuthor_Tests", "Ambiguities in Photon Container", 4, 0, 4)));
    //Isolation 
    ANA_CHECK (book (TH1F ("h_topoetcone40_giso", "Calorimeter Isolation FixedCutLoose", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_topoetcone20_giso", "Calorimeter Isolation FixedCutLoose", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_ptcone20_giso", "Track Isolation FixedCutLoose", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_g", "Calorimeter Isolation", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_topoetcone20_g", "Calorimeter Isolation", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_g", "Track Isolation", 60, -0.5, 0.1)));
    
    //Leading and subleading photons
    ANA_CHECK (book (TH1F ("h_gLeadingpt", "Leading pt", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_gLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_gSubleadingpt", "Subleading pt", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gSubleadingeta", "Subleading Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_gSubleadingphi", "Subleading Phi", 32, -3.2, 3.2)));   
    
        
    if (m_ElectronChannel){
      ANA_CHECK (book (TH1F ("h_Cutflow_events", "HLT Chains ", 5, 0, 5)));
      ANA_CHECK (book (TH1F ("h_eCutflow_selection", "Electron Selection Cutflow", 9, 0, 9)));
      ANA_CHECK (book (TH1F ("h_NumbergoodElectrons", "Number of Electrons after selection", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_goodelectronspt", "Electrons after selection", 50, 0., 200)));
      
      ANA_CHECK (book (TH1F ("h_EleID_Tests", "Electron ID", 6, 0, 6)));
      ANA_CHECK (book (TH1F ("h_EleAuthor_Tests", "Ambiguities in Electron Container", 4, 0, 4)));
      //Isolation
      ANA_CHECK (book (TH1F ("h_ptvarcone30_e", "Track Isolation ", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_e", "Calorimeter Isolation ", 120, -0.5, 0.1)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_eiso", "Track Isolation Loose_VarRad", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_eiso", "Calorimeter Isolation Loose_VarRad", 120, -0.5, 0.1)));
      
      //leading and subleading electrons
      ANA_CHECK (book (TH1F ("h_eLeadingpt", "Leading pt", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_eLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
      ANA_CHECK (book (TH1F ("h_eLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
      
      ANA_CHECK (book (TH1F ("h_effHLTWyy_gsublead", "eyy Trigger chain efficiency", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_effHLTeyy_gsublead_crack", "eyy Trigger chain efficiency - Crack region", 50, 0., 200)));
      
      ANA_CHECK (book (TH1F ("h_num_HLTeyy_gsublead_crack", "subleading photon pt - Crack region", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_denom_HLTeyy_gsublead_crack", "subleading photon pt - Crack region", 50, 0., 200)));
      
      
    } else if (m_MuonChannel){
      ANA_CHECK (book (TH1F ("h_Cutflow_events", "HLT Chains ", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_muCutflow_selection", "Muon Selection Cutflow", 11, 0, 11)));
      ANA_CHECK (book (TH1F ("h_NumbergoodMuons", "Number of Muons after selection", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_goodmuonspt", "Electrons after selection", 50, 0., 200)));
      
      //ANA_CHECK (book (TH1F ("h_MuID_Tests", "Muon ID", 8, 0, 8)));
      //Isolation
      ANA_CHECK (book (TH1F ("h_ptvarcone30_mu", "Track Isolation ", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_mu", "Calorimeter Isolation ", 120, -0.5, 0.1)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_muiso", "Track Isolation Loose_VarRad", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_muiso", "Calorimeter Isolation Loose_VarRad", 120, -0.5, 0.1)));
      
      //leading and subleading muons
      ANA_CHECK (book (TH1F ("h_muLeadingpt", "Leading pt", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_muLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
      ANA_CHECK (book (TH1F ("h_muLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
      
      ANA_CHECK (book (TH1F ("h_effHLTWyy_gsublead", "muyy Trigger chain efficiency", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_effHLTmuyy_gsublead_barrel", "muyy Trigger chain efficiency", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_effHLTmuyy_gsublead_endcap", "muyy Trigger chain efficiency", 50, 0., 200)));
      
      ANA_CHECK (book (TH1F ("h_num_HLTmuyy_gsublead_barrel", "subleading photon pt - Barrel", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_denom_HLTmuyy_gsublead_barrel", "subleading photon pt - Barrel", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_num_HLTmuyy_gsublead_endcap", "subleading photon pt - Endcap", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_denom_HLTmuyy_gsublead_endcap", "subleading photon pt - Endcap", 50, 0., 200)));
      
    }
    
    ANA_CHECK (book (TH1F("h_DR_glead_leplead", "DR leading photon to leading lepton", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DR_gsublead_leplead", "DR subleading photon to leading lepton", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DR_glead_gsublead", "DR leading photon to subleading lepton", 42, -0.2, 4.)));
  
    
    ANA_CHECK (book (TH1F ("h_num_HLTWyy_gsublead", "subleading photon pt", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_denom_HLTWyy_gsublead", "subleading photon pt", 50, 0., 200)));
    
    
    return StatusCode::SUCCESS;
  }



  StatusCode WyyTrigEfficiency :: execute ()
  {
    ANA_MSG_INFO ("in execute");
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
    hist("h_Cutflow_events")->Fill(0.5,1);
    hist("h_Cutflow_selection_lyy")->Fill(0.5,1);
    
    ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    /*
    if (!m_PHYS || !m_PHYSLITE){
      // Check for Background
      m_isBG = false;
      m_isLArBurst = false;
      
      m_isBG = eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->HaloMuonTwoSided);
      m_isLArBurst = eventInfo->isEventFlagBitSet(eventInfo->LAr,eventInfo->LArECTimeDiffHalo);
      if ( m_isBG || m_isLArBurst ) return StatusCode::SUCCESS;
    }
    */
    //hist("h_Cutflow_selection_lyy")->Fill(1.5,1);
    
    static ort::inputDecorator_t selDec(m_selectionLabel);
    static ort::outputAccessor_t overlapAcc(m_overlapLabel);
    
    // Trigger:
    
    if ( !m_trigDecTool.empty()) {
      for ( auto& chainName : m_vTrigChainNames ) {
        ANA_MSG_DEBUG("Chain "<<chainName);
        std::vector<std::string> L1Triggeritems = m_trigDecTool->getChainGroup(chainName)->getListOfTriggers();  //L1 trigger
        for (std::string &trigName : L1Triggeritems){
          ANA_MSG_DEBUG("Trigger "<<trigName);
          bool isPassed = m_trigDecTool->isPassed(trigName);
          ANA_MSG_DEBUG("Trigger passed:"<<isPassed);
        }
      }
    }
    
 
    /*
    //Events that have fired L1_EM22VHI
    m_isL1_EM22VHIPassed=false; 
    auto allChains = m_trigDecTool->getChainGroup("L1_.*");
    for(auto& trig : allChains->getListOfTriggers()){
      if(m_trigDecTool->getChainGroup(trig)->isPassed()){
        if(trig=="L1_EM22VHI"){m_isL1_EM22VHIPassed=true;} 
      }
    }
    std::cout << "ELECTRON TRIGGER CHAINS" <<"\n";
    auto ElectronChains = m_trigDecTool->getChainGroup("HLT_e.*"); 
    for(auto& trig : ElectronChains->getListOfTriggers()){ //loop over all of them 
      if(m_trigDecTool->getChainGroup(trig)->isPassed()){
        std::cout << trig <<"\n";
      }
    }
 
    auto WggChains = m_trigDecTool->getChainGroup("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L");
    std::cout << "Wgg TRIGGER CHAINS" <<"\n";
    for(auto& trig : WggChains->getListOfTriggers()){ //loop over all of them 
      if(m_trigDecTool->getChainGroup(trig)->isPassed()){
        std::cout << trig <<"\n";
      }
    }
    */
  
    
    ANA_MSG_DEBUG ("Checking triggers that fired ");
    if (m_ElectronChannel){
      //Legacy: HLT_e26_lhtight_ivarloose_L1EM22VHI
      //Phase I: HLT_e26_lhtight_ivarloose_L1eEM26M
      if(m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1EM22VHI")){
	hist("h_Cutflow_events")->Fill(1.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1eEM26M")){
	hist("h_Cutflow_events")->Fill(2.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      
      //Legacy: HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH
      //Phase I: HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L
      if(m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH")){
	hist("h_Cutflow_events")->Fill(3.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L")){
	hist("h_Cutflow_events")->Fill(4.5);
      } else {
	//return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel){
      //Legacy: HLT_mu24_ivarmedium_L1MU14FCH
      if(m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(1.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Legacy A: "HLT_2g10_loose_mu20_L1MU14FCH"
      if(m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(3.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(2.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Legacy B: HLT_2g10_loose_mu20_L1MU18VFCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(4.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      
      //Phase I single mu doesnt change?
      
      //Phase I A: HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(5.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Phase I B: HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(6.5);
      } else {
	//return StatusCode::SUCCESS;
      }
      
    }
  
    ANA_MSG_INFO("Trigger pass requirements");
    if (m_ElectronChannel && m_Legacy){
      if(!m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH") && !m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1EM22VHI")){
	return StatusCode::SUCCESS;
      }
    } else if (m_ElectronChannel && m_PhaseI){
      if(!m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L") && !m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1eEM26M")){
	return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel && m_Legacy){
      if(!m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU14FCH") && !m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU14FCH")){
	return StatusCode::SUCCESS;
      }
      /*
      if(!m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU18VFCH") && !m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU18VFCH")){
	return StatusCode::SUCCESS;
      }
      */
    } else if (m_MuonChannel && m_PhaseI){
      if(!m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH") && !m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU14FCH")){
	return StatusCode::SUCCESS;
      }
      /*
      if(!m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH") && !m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU18VFCH")){
	return StatusCode::SUCCESS;
      }
      */
    }
    
    /*****************************
    * 
    *          OFFLINE OBJ - BEFORE SELECTION
    * 
    *****************************/
    //Retrieve containers
    SG::ReadHandle<xAOD::PhotonContainer> gammaTES(m_PhotonContainerKey);
    if ( ! gammaTES.isValid() ) {
      ANA_MSG_ERROR("No photon container" <<  m_PhotonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("PhotonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::ElectronContainer> elecTES(m_ElectronContainerKey);
    if ( ! elecTES.isValid() ) {
      ANA_MSG_ERROR("No electron container" <<  m_ElectronContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("ElectronContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::MuonContainer> muTES(m_MuonContainerKey);
    if ( ! muTES.isValid() ) {
      ANA_MSG_ERROR("No muon container" <<  m_MuonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("MuonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::JetContainer> jetCon(m_JetPflowContainerKey);
    if(!jetCon.isValid()){
      ANA_MSG_ERROR("No jet container" << m_JetPflowContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("JetContainer successfully retrieved");
    
    
  //PRIMARY VERTEX
    const xAOD::Vertex* pVtx(0);
    SG::ReadHandle<xAOD::VertexContainer> vertices(m_VertexContainerKey);
    if (vertices.isValid()) {
      ANA_MSG_DEBUG("Collection with name " << m_VertexContainerKey << " with size " << vertices->size() << " found in evtStore()");
      xAOD::VertexContainer::const_iterator vxItr = vertices->begin();
      xAOD::VertexContainer::const_iterator vxItrE = vertices->end();
      for (; vxItr != vxItrE; ++vxItr) {
        if ((*vxItr)->vertexType() == xAOD::VxType::PriVtx) {
          pVtx = *vxItr;
        } 
      }
    } else {
      ANA_MSG_WARNING("No collection with name " << m_VertexContainerKey << " found in evtStore()");
    }

    hist("h_Cutflow_selection_lyy")->Fill(1.5,1);
    
    //Copying containers to add flags, calibrate, sort, etc.
    auto gcopy = xAOD::shallowCopyContainer(*gammaTES);
    ANA_CHECK(evtStore()->record(gcopy.first,"PhotonsCopy"));
    ANA_CHECK(evtStore()->record(gcopy.second, "PhotonsCopyAux"));
    xAOD::PhotonContainer* selPhotons = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* selPhotons_aux = new xAOD::PhotonAuxContainer;
    selPhotons->setStore(selPhotons_aux);
    ANA_CHECK(evtStore()->record(selPhotons,"SelectedPhotons"));
    ANA_CHECK(evtStore()->record(selPhotons_aux, "SelectedPhotonsAux"));
    
    auto ecopy = xAOD::shallowCopyContainer(*elecTES);
    ANA_CHECK(evtStore()->record(ecopy.first,"ElectronsCopy"));
    ANA_CHECK(evtStore()->record(ecopy.second, "ElectronsCopyAux"));
    xAOD::ElectronContainer* selElectrons = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* selElectrons_aux = new xAOD::ElectronAuxContainer;
    selElectrons->setStore(selElectrons_aux);
    ANA_CHECK(evtStore()->record(selElectrons,"SelectedElectrons"));
    ANA_CHECK(evtStore()->record(selElectrons_aux, "SelectedElectronsAux"));
    
    auto jcopy = xAOD::shallowCopyContainer(*jetCon);
    ANA_CHECK(evtStore()->record(jcopy.first,"JetsCopy"));
    ANA_CHECK(evtStore()->record(jcopy.second, "JetsCopyAux"));
    xAOD::JetContainer* selJets = new xAOD::JetContainer;
    xAOD::JetAuxContainer* selJets_aux = new xAOD::JetAuxContainer;
    selJets->setStore(selJets_aux);
    ANA_CHECK(evtStore()->record(selJets,"SelectedJets"));
    ANA_CHECK(evtStore()->record(selJets_aux, "SelectedJetsAux"));
    
    auto mucopy = xAOD::shallowCopyContainer(*muTES);
    ANA_CHECK(evtStore()->record(mucopy.first,"MuonsCopy"));
    ANA_CHECK(evtStore()->record(mucopy.second, "MuonsCopyAux"));
    xAOD::MuonContainer* selMuons = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* selMuons_aux = new xAOD::MuonAuxContainer;
    selMuons->setStore(selMuons_aux);
    ANA_CHECK(evtStore()->record(selMuons,"SelectedMuons"));
    ANA_CHECK(evtStore()->record(selMuons_aux, "SelectedMuonsAux"));
    
    /*****************************
    *           PHOTONS 
    *****************************/
    // Plot all pt, check iso, flag overlaps
    Int_t Allg_N = 0;
    for (auto gam: *gcopy.first){
      Allg_N++;
      hist("h_gCutflow_selection")->Fill(0.5,1);
      if (m_PHYS){
	hist("h_AllPhotonspt_noCalib")->Fill((gam)->pt()/1e3);
	if (m_EgammaCalibrationAndSmearingTool->applyCorrection(*gam) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");}
      }
      hist("h_gCutflow_selection")->Fill(1.5,1); //after EC
      
      hist("h_AllPhotonspt")->Fill(((gam)->pt())/1e3);
      hist("h_AllPhotonseta")->Fill((gam)->eta());
      hist("h_AllPhotonsphi")->Fill((gam)->phi());
      
      xAOD::Photon* selPhoton = new xAOD::Photon;
      *selPhoton = *gam;
      selDec(*selPhoton) = true; //Overlap removal - Input decorator
      selPhotons->push_back(selPhoton);
      
      hist("h_gamID_Tests")->Fill(0.5);       
      if ((gam)->passSelection("DFCommonPhotonsIsEMLoose")){
        hist("h_gamID_Tests")->Fill(1.5);
      }  
      if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){
        hist("h_gamID_Tests")->Fill(2.5);
      }
      //Ambiguities
      hist("h_gamAuthor_Tests")->Fill(0.5);
      
      if (gam->author()==xAOD::EgammaParameters::AuthorPhoton){ 
        hist("h_gamAuthor_Tests")->Fill(1.5);
      }  
      if (gam->author()==xAOD::EgammaParameters::AuthorElectron){ 
        hist("h_gamAuthor_Tests")->Fill(2.5);
      }
      if (gam->author()==xAOD::EgammaParameters::AuthorAmbiguous){
        hist("h_gamAuthor_Tests")->Fill(3.5);
      }
      
      //ISOLATION no WP- without OR or ambiguities 
      float topoetcone40_g;
      float topoetcone20_g;
      float ptcone20_g;
      (gam)->isolation(topoetcone40_g,xAOD::Iso::topoetcone40);
      (gam)->isolation(topoetcone20_g,xAOD::Iso::topoetcone20);
      (gam)->isolation(ptcone20_g,xAOD::Iso::ptcone20);
      hist("h_topoetcone40_g")->Fill(topoetcone40_g/1e3 -0.022*(gam)->pt()/1e3);
      hist("h_topoetcone20_g")->Fill(topoetcone20_g/1e3 -0.065*(gam)->pt()/1e3);
      hist("h_ptcone20_g")->Fill(ptcone20_g/1e3 - 0.05*(gam)->pt()/1e3);
      
    }
    hist("h_NumberPhotons")->Fill(Allg_N,1);
    selPhotons->sort([](xAOD::Photon* gam1, xAOD::Photon* gam2){return gam1->pt() > gam2->pt();});
    
    
     /*****************************
    *          ELECTRONS 
    *****************************/
    Int_t AllEl_N = 0;
    for (auto ele: *ecopy.first){
      AllEl_N++;
      if (m_ElectronChannel){ hist("h_eCutflow_selection")->Fill(0.5,1);}
      if (m_PHYS){
	hist("h_AllElectronspt_noCalib")->Fill((ele)->pt()/1e3);
	if (m_EgammaCalibrationAndSmearingTool->applyCorrection(*ele) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");} 
      }
      hist("h_AllElectronspt")->Fill((ele)->pt()/1e3);
      hist("h_AllElectronseta")->Fill((ele)->caloCluster()->etaBE(2));
      hist("h_AllElectronsphi")->Fill((ele)->phi());
      hist("h_AllElectronscharge")->Fill((ele)->charge());
      
      xAOD::Electron* selElectron = new xAOD::Electron;
      *selElectron = *ele;
      selDec(*selElectron) = true;
      selElectrons->push_back(selElectron);
      
      if (m_ElectronChannel){
	hist("h_eCutflow_selection")->Fill(1.5,1); //after EC
	//Isolation without OR or ambiguities out
	float topoetcone20_e;
	float ptvarcone30_e;
	(ele)->isolation(ptvarcone30_e,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
	(ele)->isolation(topoetcone20_e,xAOD::Iso::topoetcone20);
	hist("h_ptvarcone30_e")->Fill(ptvarcone30_e/((ele)->pt())-0.15);
	hist("h_topoetcone20_e")->Fill(topoetcone20_e/((ele)->pt())-0.2);
	
	ANA_MSG_DEBUG("Electron Identification");
	hist("h_EleID_Tests")->Fill(0.5);
	if ((ele)->passSelection("DFCommonElectronsLHVeryLoose")){
	  hist("h_EleID_Tests")->Fill(1.5);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHLoose")){
	  hist("h_EleID_Tests")->Fill(2.5);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHLooseBL")){
	  hist("h_EleID_Tests")->Fill(3.5);
	}
	if ((ele)->passSelection("DFCommonElectronsLHMedium")){
	  hist("h_EleID_Tests")->Fill(4.5);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHTight")){
	  hist("h_EleID_Tests")->Fill(5.5);
	} 
	ANA_MSG_DEBUG("Electron Author");
	hist("h_EleAuthor_Tests")->Fill(0.5);
	if (ele->author()==xAOD::EgammaParameters::AuthorPhoton){ 
	  hist("h_EleAuthor_Tests")->Fill(1.5);
	}  
	if (ele->author()==xAOD::EgammaParameters::AuthorElectron){ 
	  hist("h_EleAuthor_Tests")->Fill(2.5);
	}
	if (ele->author()==xAOD::EgammaParameters::AuthorAmbiguous){ 
	  hist("h_EleAuthor_Tests")->Fill(3.5);
	}
      }
    
    }
    hist("h_NumberElectrons")->Fill(AllEl_N,1);
    selElectrons->sort([](xAOD::Electron* ele1, xAOD::Electron* ele2){return ele1->pt() > ele2->pt();});
    
    
    /*****************************
    *          MUONS 
    *****************************/
    Int_t AllMu_N=0;
    for (auto mu: *mucopy.first){
      AllMu_N++;
      if (m_MuonChannel){ hist("h_muCutflow_selection")->Fill(0.5,1);}
      if (m_PHYS){
	hist("h_AllMuonspt_noCalib")->Fill((mu)->pt()/1e3);
	if (m_MuonCalibTool->applyCorrection(*mu) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");}
      }
      
      hist("h_AllMuonspt")->Fill((mu)->pt()/1e3);
      hist("h_AllMuonseta")->Fill((mu)->eta());
      hist("h_AllMuonsphi")->Fill((mu)->phi());
      hist("h_AllMuonscharge")->Fill((mu)->charge());
      
      xAOD::Muon* selMuon = new xAOD::Muon;
      *selMuon = *mu;
      selDec(*selMuon) = true;
      selMuons->push_back(selMuon);
      
      if (m_MuonChannel){
	hist("h_muCutflow_selection")->Fill(1.5,1); //after EC
	float topoetcone20_mu;
	float ptvarcone30_mu;
	(mu)->isolation(ptvarcone30_mu,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt1000);
	(mu)->isolation(topoetcone20_mu,xAOD::Iso::topoetcone20);
	hist("h_ptvarcone30_mu")->Fill(ptvarcone30_mu/((mu)->pt()) - 0.15);
	hist("h_topoetcone20_mu")->Fill(topoetcone20_mu/((mu)->pt()) - 0.3);
      }
    }
    hist("h_NumberMuons")->Fill(AllMu_N,1);
    selMuons->sort([](xAOD::Muon* mu1, xAOD::Muon* mu2){return mu1->pt() > mu2->pt();});
    
      
    
    
    /*****************************
    *          JETS 
    *****************************/
    Int_t AllJet_N=0;
    //here makes sense to select only non PU jets for the OR
    for (auto jet: *jcopy.first){
      AllJet_N++;
      if (m_PHYS){
	hist("h_AllJetspt_noCalib")->Fill((jet)->pt()/1e3);
	/*
	if(!static_cast<bool>(m_JetCleaningTool->accept(*jet))){ //not sure if works
	    return StatusCode::SUCCESS;
	}
	*/
      }
      xAOD::Jet* selJet = new xAOD::Jet();
      *selJet = *jet;
      selDec(*selJet) = true;
      selJets->push_back((selJet));
    }
    if (m_PHYS){
      if(m_JetCalibrationTool->applyCalibration(*selJets) != StatusCode::SUCCESS){ANA_MSG_WARNING("Cannot calibrate jet!");}
    }
    for (auto jet: *selJets){
      hist("h_AllJetspt")->Fill((jet)->pt()/1e3);
      hist("h_AllJetseta")->Fill((jet)->eta());
      hist("h_AllJetsphi")->Fill((jet)->phi());
      
    }
    hist("h_NumberJets")->Fill(AllJet_N,1);
    
    
    /*****************************
    *      OVERLAP REMOVAL TOOL
    *****************************/
    //Check whether we have a primary vertex
    if(!pVtx){
      return StatusCode::SUCCESS;
    }
    //We create containers of the other types of particles (as empty dynamical objects) bc we need inputs of the OR tool
    std::vector<boost::any> otherContainers;
    xAOD::TauJetContainer* taujets_empty = new xAOD::TauJetContainer;
    xAOD::JetContainer* fatjets_empty = new xAOD::JetContainer;
    otherContainers.push_back(taujets_empty);
    otherContainers.push_back(fatjets_empty);
    
    ANA_CHECK( m_orTool->removeOverlaps(selElectrons, selMuons, selJets, taujets_empty, selPhotons, fatjets_empty) );
    
    //New containers without the overlaps
    xAOD::PhotonContainer* onlygam = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* onlygam_aux = new xAOD::PhotonAuxContainer; 
    onlygam->setStore(onlygam_aux);
    ANA_CHECK(evtStore()->record(onlygam,"SelectedPhotons_noOL"));
    ANA_CHECK(evtStore()->record(onlygam_aux, "SelectedPhotons_noOLAux"));
    
    xAOD::ElectronContainer* onlyele = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* onlyele_aux = new xAOD::ElectronAuxContainer; 
    onlyele->setStore(onlyele_aux);
    ANA_CHECK(evtStore()->record(onlyele,"SelectedElectrons_noOL"));
    ANA_CHECK(evtStore()->record(onlyele_aux, "SelectedElectrons_noOLAux"));
    
    xAOD::JetContainer* onlyjet = new xAOD::JetContainer;
    xAOD::JetAuxContainer* onlyjet_aux = new xAOD::JetAuxContainer; 
    onlyjet->setStore(onlyjet_aux);
    ANA_CHECK(evtStore()->record(onlyjet,"SelectedJets_noOL"));
    ANA_CHECK(evtStore()->record(onlyjet_aux, "SelectedJets_noOLAux"));
    
    xAOD::MuonContainer* onlymu = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* onlymu_aux = new xAOD::MuonAuxContainer; 
    onlymu->setStore(onlymu_aux);
    ANA_CHECK(evtStore()->record(onlymu,"SelectedMuons_noOL"));
    ANA_CHECK(evtStore()->record(onlymu_aux, "SelectedMuons_noOLAux"));
    
    
    
    for (xAOD::PhotonContainer::const_iterator itr=selPhotons->begin(); itr != selPhotons->end(); ++itr) {
      static ort::outputAccessor_t overlapAcc(m_overlapLabel);
      if(!overlapAcc(**itr)){
	xAOD::Photon* noOL_gam = new xAOD::Photon();
	*noOL_gam = (**itr);
	onlygam->push_back(noOL_gam);
      } else if (overlapAcc(**itr)){
	  //std::cout << "Overlap found in Photon Container" << "\n";
      }
    }
    
    for (xAOD::ElectronContainer::const_iterator itr=selElectrons->begin(); itr != selElectrons->end(); ++itr) {
      static ort::outputAccessor_t overlapAcc(m_overlapLabel);
      if(!overlapAcc(**itr)){
	xAOD::Electron* noOL_ele = new xAOD::Electron();
	*noOL_ele = (**itr);
	onlyele->push_back(noOL_ele);
      } else if (overlapAcc(**itr)){
	  //std::cout << "Overlap found in Electron Container" << "\n";
      } 
    }
    
    for (xAOD::MuonContainer::const_iterator itr=selMuons->begin(); itr != selMuons->end(); ++itr) {
      static ort::outputAccessor_t overlapAcc(m_overlapLabel);
      if(!overlapAcc(**itr)){
	xAOD::Muon* noOL_mu = new xAOD::Muon();
	*noOL_mu = (**itr);
	onlymu->push_back(noOL_mu);
      } else if (overlapAcc(**itr)){
	  //std::cout << "Overlap found in Muon Container" << "\n";
      } 
    }
    
    for (xAOD::JetContainer::const_iterator itr=selJets->begin(); itr != selJets->end(); ++itr) {
      static ort::outputAccessor_t overlapAcc(m_overlapLabel);
      if(!overlapAcc(**itr)){
	xAOD::Jet* noOL_jet = new xAOD::Jet();
	*noOL_jet = (**itr);
	onlyjet->push_back(noOL_jet);
      } else if (overlapAcc(**itr)){
	  //std::cout << "Overlap found in Jet Container" << "\n";
      }
    }
    
    
    //Control plots for Overlaps
    for(xAOD::JetContainer::const_iterator jet=selJets->begin(); jet != selJets->end(); ++jet) {
      for (xAOD::ElectronContainer::const_iterator ele=selElectrons->begin(); ele != selElectrons->end(); ++ele) {
	hist("h_DRej")->Fill(DR((*ele)->caloCluster()->etaBE(2), (*ele)->phi(), (*jet)->eta(), (*jet)->phi()));
      }
      for (xAOD::PhotonContainer::const_iterator gam=selPhotons->begin(); gam != selPhotons->end(); ++gam) {
	hist("h_DRgj")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*jet)->eta(), (*jet)->phi()));
	for (xAOD::ElectronContainer::const_iterator ele=selElectrons->begin(); ele != selElectrons->end(); ++ele) {
	  hist("h_DRge")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()));
	}
	for (xAOD::MuonContainer::const_iterator mu=selMuons->begin(); mu != selMuons->end(); ++mu) {
	  hist("h_DRgmu")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*mu)->eta(), (*mu)->phi()));
	}
      }
      for (xAOD::MuonContainer::const_iterator mu=selMuons->begin(); mu != selMuons->end(); ++mu) {
	hist("h_DRmuj")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*jet)->eta(), (*jet)->phi()));
	for (xAOD::ElectronContainer::const_iterator ele=selElectrons->begin(); ele != selElectrons->end(); ++ele) {
	  hist("h_DRmue")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()));
	}
      }
    }
    
    //After OverlapRemoval
    for(xAOD::JetContainer::const_iterator jet=onlyjet->begin(); jet != onlyjet->end(); ++jet) {
      for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	hist("h_DRej_noOL")->Fill(DR((*ele)->caloCluster()->etaBE(2), (*ele)->phi(), (*jet)->eta(), (*jet)->phi()));
      }
      for (xAOD::PhotonContainer::const_iterator gam=onlygam->begin(); gam != onlygam->end(); ++gam) {
	hist("h_DRgj_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*jet)->eta(), (*jet)->phi()));
	for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	    hist("h_DRge_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()));
	}
	for (xAOD::MuonContainer::const_iterator mu=onlymu->begin(); mu != onlymu->end(); ++mu) {
	  hist("h_DRgmu_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*mu)->eta(), (*mu)->phi()));
	}
      }
      for (xAOD::MuonContainer::const_iterator mu=onlymu->begin(); mu != onlymu->end(); ++mu) {
	hist("h_DRmuj_noOL")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*jet)->eta(), (*jet)->phi()));
	for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	  hist("h_DRmue_noOL")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()));
	}
      }
    }
    
    
    
    /*****************************
    * 
    *       OFFLINE SELECTION 
    * 
    *****************************/
    ANA_MSG_DEBUG("Start photon selection");
    Int_t Goodg_N = 0;
    xAOD::PhotonContainer* goodphotons = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* goodphotons_aux = new xAOD::PhotonAuxContainer;
    goodphotons->setStore(goodphotons_aux);
    ANA_CHECK(evtStore()->record(goodphotons,"GoodPhotons"));
    ANA_CHECK(evtStore()->record(goodphotons_aux, "GoodPhotonsAux"));
    
    for (auto gam: *onlygam) {
      hist("h_gCutflow_selection")->Fill(2.5,1); //after OR
      hist("h_onlygampt")->Fill((gam)->pt()/1e3);

      //ISOLATION 
      if (m_isolationSelectionTool->accept(*gam)){
	float topoetcone40_giso;
	float topoetcone20_giso;
	float ptcone20_giso;
	(gam)->isolation(topoetcone40_giso,xAOD::Iso::topoetcone40);
	(gam)->isolation(topoetcone20_giso,xAOD::Iso::topoetcone20);
	hist("h_topoetcone40_giso")->Fill(topoetcone40_giso/1e3 -0.022*(gam)->pt()/1e3);
	hist("h_topoetcone20_giso")->Fill(topoetcone20_giso/1e3 -0.065*(gam)->pt()/1e3);
	(gam)->isolation(ptcone20_giso,xAOD::Iso::ptcone20);
	hist("h_ptcone20_giso")->Fill(ptcone20_giso/1e3 - 0.05*(gam)->pt()/1e3);
      }
      
      m_AmbiguityTool->ambiguityResolve(*gam);
      if (((gam->author()==xAOD::EgammaParameters::AuthorAmbiguous) || (gam->author()==xAOD::EgammaParameters::AuthorPhoton)) && m_AmbiguityTool->accept(*gam)){ 
	hist("h_gCutflow_selection")->Fill(3.5,1);
	if ((fabs((gam)->caloCluster()->etaBE(2))<1.37 || fabs((gam)->caloCluster()->etaBE(2)) > 1.52) && fabs((gam)->caloCluster()->etaBE(2)) < 2.37){
	  hist("h_gCutflow_selection")->Fill(4.5,1);
	  if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){  //Identification
	    hist("h_gCutflow_selection")->Fill(5.5,1);
	    if (m_isolationSelectionTool->accept(*gam)){
	      hist("h_gCutflow_selection")->Fill(6.5,1);
	      if (((gam)->pt()/1e3 > 7.)){ 
		hist("h_gCutflow_selection")->Fill(7.5,1);
		
		ANA_MSG_DEBUG("Good photon");
		xAOD::Photon* goodphoton = new xAOD::Photon();
		*goodphoton = (*gam);
		goodphotons->push_back(goodphoton);
		hist("h_goodphotonspt")->Fill((gam)->pt()/1e3);
		Goodg_N++;
	      }
	    }
	  }
	}
      }
    }
    hist("h_NumbergoodPhotons")->Fill(Goodg_N,1);
    
 
    //Defining good lepton containers
    Int_t GoodEl_N = 0;
    xAOD::ElectronContainer* goodelectrons = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* goodelectrons_aux = new xAOD::ElectronAuxContainer;
    goodelectrons->setStore(goodelectrons_aux);
    ANA_CHECK(evtStore()->record(goodelectrons,"GoodElectrons"));
    ANA_CHECK(evtStore()->record(goodelectrons_aux, "GoodElectronsAux"));
    
    Int_t GoodMu_N = 0;
    xAOD::MuonContainer* goodmuons = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* goodmuons_aux = new xAOD::MuonAuxContainer;
    goodmuons->setStore(goodmuons_aux);
    ANA_CHECK(evtStore()->record(goodmuons,"GoodMuons"));
    ANA_CHECK(evtStore()->record(goodmuons_aux, "GoodMuonsAux"));
      
    if (m_ElectronChannel){
      ANA_MSG_DEBUG("Start electron selection");
      for (auto ele: *onlyele) { 
	hist("h_eCutflow_selection")->Fill(2.5,1); //after OR
	hist("h_onlyelept")->Fill((ele)->pt()/1000);
	
	auto elTrk = (ele)->trackParticle();
	if (!elTrk) {
	  ANA_MSG_WARNING("Electron with no track particle " << eventInfo->runNumber() << " " << eventInfo->eventNumber());
	  continue;
	}
	float d0sig;
	float Dz0sin = (elTrk->z0()+elTrk->vz()-pVtx->z())*std::sin(elTrk->theta());
	try {
	  d0sig = xAOD::TrackingHelpers::d0significance(elTrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY());
	} catch (...) {
	  ANA_MSG_DEBUG("Invalid beamspot - electron");
	  try {
	    d0sig = xAOD::TrackingHelpers::d0significance(elTrk);
	  } catch (...) {
	    ANA_MSG_WARNING("Ridiculous exception thrown - electron");
	    continue;
	  }
	}
	//Isolation 
	if (m_isolationSelectionTool->accept(*ele)){
	  float topoetcone20_eiso;
	  float ptvarcone30_eiso;
	  (ele)->isolation(ptvarcone30_eiso,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
	  (ele)->isolation(topoetcone20_eiso,xAOD::Iso::topoetcone20);
	  hist("h_ptvarcone30_eiso")->Fill(ptvarcone30_eiso/((ele)->pt())-0.15);
	  hist("h_topoetcone20_eiso")->Fill(topoetcone20_eiso/((ele)->pt())-0.2);
	}
      
	m_AmbiguityTool->ambiguityResolve(*ele);
	if (((ele->author()==xAOD::EgammaParameters::AuthorAmbiguous) || (ele->author()==xAOD::EgammaParameters::AuthorElectron)) && m_AmbiguityTool->accept(*ele)){
	  hist("h_eCutflow_selection")->Fill(3.5,1);
	  if (fabs((ele)->caloCluster()->etaBE(2)) < 2.47){
	    hist("h_eCutflow_selection")->Fill(4.5,1);
	    if ((ele)->passSelection("DFCommonElectronsLHMedium")){
	      hist("h_eCutflow_selection")->Fill(5.5,1);
	      if (fabs(d0sig)<5 && fabs(Dz0sin)<0.5){
		hist("h_eCutflow_selection")->Fill(6.5,1);
		if (m_isolationSelectionTool->accept(*ele)){ 
		  hist("h_eCutflow_selection")->Fill(7.5,1);
		  if (((ele)->pt()/1e3 > 20.)){
		    hist("h_eCutflow_selection")->Fill(8.5,1);
		    ANA_MSG_DEBUG("Good electron");
		    GoodEl_N++;
		    xAOD::Electron* goodelectron = new xAOD::Electron();
		    *goodelectron = (*ele);
		    goodelectrons->push_back(goodelectron);
		    hist("h_goodelectronspt")->Fill((ele)->pt()/1000);
		  }
		}
	      }
	    }
	  }
	}
      }
      hist("h_NumbergoodElectrons")->Fill(GoodEl_N,1);
      
    } else if (m_MuonChannel){
      ANA_MSG_DEBUG("Start muon selection");
    
      for (auto mu: *onlymu) { 
	hist("h_muCutflow_selection")->Fill(2.5,1); //after OR
	hist("h_onlymupt")->Fill((mu)->pt()/1000);
      
	auto muTrk = (mu)->primaryTrackParticle();
	if (!muTrk) {
	  ANA_MSG_WARNING("Muon with no track particle " << eventInfo->runNumber() << " " << eventInfo->eventNumber());
	  continue;
	}
	float d0sig;
	float Dz0sin = (muTrk->z0()+muTrk->vz()-pVtx->z())*std::sin(muTrk->theta());
	try {
	  d0sig = xAOD::TrackingHelpers::d0significance(muTrk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY());
	} catch (...) {
	  ANA_MSG_DEBUG("Invalid beamspot - muon");
	  try {
	    d0sig = xAOD::TrackingHelpers::d0significance(muTrk);
	  } catch (...) {
	    ANA_MSG_WARNING("Ridiculous exception thrown - muon");
	    continue;
	  }
	}
	//Isolation 
	if (m_isolationSelectionTool->accept(*mu)){
	  float topoetcone20_muiso;
	  float ptvarcone30_muiso;
	  (mu)->isolation(ptvarcone30_muiso,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt1000);
	  (mu)->isolation(topoetcone20_muiso,xAOD::Iso::topoetcone20);
	  hist("h_ptvarcone30_muiso")->Fill(ptvarcone30_muiso/((mu)->pt()) - 0.15);
	  hist("h_topoetcone20_muiso")->Fill(topoetcone20_muiso/((mu)->pt()) - 0.3);
	}
     
	if (m_MuonSelectionTool->accept(*mu)){//ID and eta - MuonSelectionTool 
	  hist("h_muCutflow_selection")->Fill(3.5,1);
	  if ((fabs(d0sig)<3.) && (fabs(Dz0sin)<0.5)){//TTVA 
	    hist("h_muCutflow_selection")->Fill(4.5,1);
	    if (m_isolationSelectionTool->accept(*mu)){ 
	      hist("h_muCutflow_selection")->Fill(5.5,1);
	      if (((mu)->pt()/1e3 > 20.)){
		hist("h_muCutflow_selection")->Fill(6.5,1);
		ANA_MSG_DEBUG("Good muon");
		GoodMu_N++;
		xAOD::Muon* goodmuon = new xAOD::Muon();
		*goodmuon = (*mu);
		goodmuons->push_back(goodmuon);
		hist("h_goodmuonspt")->Fill((mu)->pt()/1e3);
	      }
	    }
	  }
	}
	
      }
      hist("h_NumbergoodMuons")->Fill(GoodMu_N,1);
    }
    

    const xAOD::IParticle* leadingGam(0);
    const xAOD::IParticle* subleadingGam(0);
    const xAOD::IParticle* leadingLep(0);
    
    if (goodphotons->size() > 1){
      hist("h_Cutflow_selection_lyy")->Fill(2.5,1);
      leadingGam = (*goodphotons)[0];
      subleadingGam = (*goodphotons)[1];
    } else {
      return StatusCode::SUCCESS;
    } 
    
    hist("h_gLeadingpt")->Fill(leadingGam->pt()/1e3);
    hist("h_gLeadingeta")->Fill(leadingGam->eta());
    hist("h_gLeadingphi")->Fill(leadingGam->phi());
  
    hist("h_gSubleadingpt")->Fill(subleadingGam->pt()/1e3);
    hist("h_gSubleadingeta")->Fill(subleadingGam->eta());
    hist("h_gSubleadingphi")->Fill(subleadingGam->phi());
    
    if (m_ElectronChannel){
      if (goodelectrons->size() > 0){
	hist("h_Cutflow_selection_lyy")->Fill(3.5,1);
	leadingLep = (*goodelectrons)[0];
	hist("h_eLeadingpt")->Fill(leadingLep->pt()/1e3);
	hist("h_eLeadingeta")->Fill(leadingLep->eta());
	hist("h_eLeadingphi")->Fill(leadingLep->phi());
      } else {
	return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel){
      if (goodmuons->size() > 0){
	hist("h_Cutflow_selection_lyy")->Fill(3.5,1);
	leadingLep = (*goodmuons)[0];
	hist("h_muLeadingpt")->Fill(leadingLep->pt()/1e3);
	hist("h_muLeadingeta")->Fill(leadingLep->eta());
	hist("h_muLeadingphi")->Fill(leadingLep->phi());
      } else {
	return StatusCode::SUCCESS;
      }
    }
 
    Float_t DRg1l = DR(leadingGam->eta(), leadingGam->phi(), leadingLep->eta(), leadingLep->phi());
    Float_t DRg2l = DR(subleadingGam->eta(), subleadingGam->phi(), leadingLep->eta(), leadingLep->phi());
    Float_t DRg1g2 = DR(leadingGam->eta(), leadingGam->phi(), subleadingGam->eta(), subleadingGam->phi());
    hist("h_DR_glead_leplead")->Fill(DRg1l);
    hist("h_DR_gsublead_leplead")->Fill(DRg2l);
    hist("h_DR_glead_gsublead")->Fill(DRg1g2);
   
    if (DRg1g2>0.2 && DRg1l>0.2 && DRg2l>0.2){
      hist("h_Cutflow_selection_lyy")->Fill(4.5);
    } else {
      return StatusCode::SUCCESS;
    }
    
   
    if (m_ElectronChannel){
      if (fabs(leadingLep->eta())>1.37 && fabs(leadingLep->eta())<1.52){
	hist("h_denom_HLTeyy_gsublead_crack")->Fill(subleadingGam->pt()/1e3);
      } else if ((fabs(leadingLep->eta())<1.37 || fabs(leadingLep->eta())>1.52) && fabs(leadingLep->eta())<2.47){
	hist("h_denom_HLTWyy_gsublead")->Fill(subleadingGam->pt()/1e3);
      } else {
	return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel){
      if (fabs(leadingLep->eta())<2.5){
	hist("h_denom_HLTWyy_gsublead")->Fill(subleadingGam->pt()/1e3);
	if (fabs(leadingLep->eta())<1.05){
	  hist("h_denom_HLTmuyy_gsublead_barrel")->Fill(subleadingGam->pt()/1e3);
	} else if (fabs(leadingLep->eta())>1.05){
	  hist("h_denom_HLTmuyy_gsublead_endcap")->Fill(subleadingGam->pt()/1e3);
	}
      } else {
	return StatusCode::SUCCESS;
      }
    }
    
     
    /*****************************
    * 
    *          EFFICIENCY HLT
    * 
    *****************************/   
    /*
    //Considering all of the combinatons till it finds the trio that triggered -> NO
    hist("h_denom_HLTWgg_elead")->Fill(leadingEle->pt()/1000);
    for (UInt_t e = 0; e < goodelectrons.size(); e++) {
      for (UInt_t k = 0; k < goodphotons.size(); k++) {
	for (UInt_t l = 0; l < goodphotons.size(); l++) {
	  if (k==l){
	    continue;
	  } else {
	    //hist("h_denom_HLTWgg_glead")->Fill(goodphotons[k]->pt()/1000);
	    std::vector<const xAOD::IParticle*> Vec{goodphotons[k],goodphotons[l],goodelectrons[e]};
	    bool Wggismatched = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH",0.1,false);
	    if (Wggismatched){
	      //hist("h_num_HLTWgg_glead")->Fill(goodphoton->pt()/1000);
	      hist("h_num_HLTWgg_elead")->Fill(leadingEle->pt()/1000);
	      return StatusCode::SUCCESS;
	    } else {
	      continue;
	    }
	  }
	}
      }
    }
    */
    
    std::vector<const xAOD::IParticle*> Vec{leadingGam,subleadingGam,leadingLep};
    bool Wyyismatched = false;
    if (m_ElectronChannel && m_Legacy){
      Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH",0.1,false);
    } else if (m_ElectronChannel && m_PhaseI) {
      Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L",0.1,false);
    } else if (m_MuonChannel && m_Legacy){
      Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_2g10_loose_mu20_L1MU14FCH",0.1,false);
    } else if (m_MuonChannel && m_PhaseI){
      Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH",0.1,false);
    }
    
    
    if (Wyyismatched){
      hist("h_Cutflow_selection_lyy")->Fill(5.5);
      if (m_ElectronChannel){
	if (fabs(leadingLep->eta())>1.37 && fabs(leadingLep->eta())<1.52){
	  hist("h_num_HLTeyy_gsublead_crack")->Fill(subleadingGam->pt()/1e3);
	} else if ((fabs(leadingLep->eta())<1.37 || fabs(leadingLep->eta())>1.52) && fabs(leadingLep->eta())<2.47){
	  hist("h_num_HLTWyy_gsublead")->Fill(subleadingGam->pt()/1e3);
	} else {
	  return StatusCode::SUCCESS;
	}
      } else if (m_MuonChannel){
	if (fabs(leadingLep->eta())<2.5){
	  hist("h_num_HLTWyy_gsublead")->Fill(subleadingGam->pt()/1e3);
	  if (fabs(leadingLep->eta())<1.05){
	    hist("h_num_HLTmuyy_gsublead_barrel")->Fill(subleadingGam->pt()/1e3);
	  } else if (fabs(leadingLep->eta())>1.05){
	    hist("h_num_HLTmuyy_gsublead_endcap")->Fill(subleadingGam->pt()/1e3);
	  }
	} else {
	  return StatusCode::SUCCESS;
	}
      }
    } else {
      return StatusCode::SUCCESS;
    }
    
    
    
    return StatusCode::SUCCESS;
  }


  StatusCode WyyTrigEfficiency :: finalize ()
  {
    
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(2, "Tight");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(3, "Loose");
    
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(2, "Photons");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(3, "Electrons");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(4, "Ambiguous");
    
    //Photon cutflow
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(2, "EC");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(3, "OR");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(4, "Ambiguities");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(5, "Eta");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(6, "Tight ID");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(7, "Loose Iso");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(8, "pt > 7 GeV");
    
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(1, "All");
    //hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(2, "Bkg");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(2, "PV");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(3, "signal y > 1");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(4, "signal l > 0");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(5, "DRij > 0.2");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(6, "TrigMatch");
    
    
    
    if (m_ElectronChannel){
      //Trigger chains
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(2, "Single e Legacy");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(3, "Single e Phase I");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(4, "eyy Legacy");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(5, "eyy Phase I");
      
      //Electron cutflow
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(2, "EC");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(3, "OR");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(4, "Ambiguities");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(5, "Eta");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(6, "LHMedium ID");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(7, "TTVA");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(8, "Loose Iso");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(9, "pt > 20 GeV");
      
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(2, "DFCommonElectronsLHVeryLoose");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(3, "DFCommonElectronsLHLoose");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(4, "DFCommonElectronsLHLooseBL");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(5, "DFCommonElectronsLHMedium");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(6, "DFCommonElectronsLHTight");
      
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(2, "Photons");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(3, "Electrons");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(4, "Ambiguous");
      
      hist("h_effHLTeyy_gsublead_crack")->Divide(hist("h_num_HLTeyy_gsublead_crack"),hist("h_denom_HLTeyy_gsublead_crack"),1.,1.,"B");
      
    } else if (m_MuonChannel){
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(2, "Single mu L+PI14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(3, "Single mu L+PI18");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(4, "muyy Legacy14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(5, "muyy Legacy18");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(6, "muyy PhaseI14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(7, "muyy PhaseI18");
      
      //Muon cutflow
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(2, "EC");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(3, "OR");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(4, "Eta + LHMedium ID");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(5, "TTVA");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(6, "Loose Iso");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(7, "pt > 20 GeV");
      
      hist("h_effHLTmuyy_gsublead_barrel")->Divide(hist("h_num_HLTmuyy_gsublead_barrel"),hist("h_denom_HLTmuyy_gsublead_barrel"),1.,1.,"B");
      hist("h_effHLTmuyy_gsublead_endcap")->Divide(hist("h_num_HLTmuyy_gsublead_endcap"),hist("h_denom_HLTmuyy_gsublead_endcap"),1.,1.,"B");
    }
    
   
    //Efficiency
    hist("h_effHLTWyy_gsublead")->Divide(hist("h_num_HLTWyy_gsublead"),hist("h_denom_HLTWyy_gsublead"),1.,1.,"B");
    
    
    
    return StatusCode::SUCCESS;
  }


  StatusCode WyyTrigEfficiency::parseList(const std::string& line, std::vector<std::string>& result) const {
    //
    // Copied from AthMonitorAlgorithm - parse the supplied trigger chains
    //
    std::string item;
    std::stringstream ss(line);

    ANA_MSG_DEBUG( "HLTAODAnalysis::parseList()" );

    while ( std::getline(ss, item, ',') ) {
      std::stringstream iss(item); // remove whitespace
      iss >> item;
      result.push_back(item);
    }

    return StatusCode::SUCCESS;
  }
