#include <AsgMessaging/MessageCheck.h>
#include <WyyAnalysis/WyyMCAnalysis.h>
#include <TH1.h>
#include <TProfile.h>

#include "StoreGate/ReadHandleKey.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"

#include <xAODEventInfo/EventInfo.h>
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaDefs.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODJet/Jet.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTrigL1Calo/TriggerTowerAuxContainer.h"
#include "MCTruthClassifier/MCTruthClassifier.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAssociationHelper.h"

//#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
//#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
//#include "JetMomentTools/JetVertexNNTagger.h"
#include "METUtilities/METMaker.h"
//#include "METInterface/IMETMaker.h"

#include "IsolationSelection/IIsolationSelectionTool.h"
//#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "xAODCore/ShallowCopy.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "xAODBase/IParticleHelpers.h"


#include <math.h>
#include <vector>
#include <bitset>
#include <boost/any.hpp>


WyyMCAnalysis :: WyyMCAnalysis(const std::string& name, //the vars called here come from the headers that we included
  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
  m_isolationSelectionTool("CP::IsolationSelectionTool/IIsolationSelectionTool", this),
  m_MuonSelectionTool("CP::MuonSelectionTool/MuonSelectionTool", this),
  //m_MuonCalibTool("CP::MuonCalibTool/MuonCalibTool", this),
  //m_JvtToolHandle("CP::JetJvtEfficiency/NNJvt",this),
  m_METMaker("met::METMaker/METMaker", this),
  m_vTrigChainNames({})
  {
    declareProperty("IsolationSelectionTool", m_isolationSelectionTool);
    declareProperty( "MuonSelectionTool", m_MuonSelectionTool);
    //declareProperty( "MuonCalibTool", m_MuonCalibTool);
    //declareProperty("JvtToolHandle", m_JvtToolHandle);
    declareProperty( "METMaker", m_METMaker);
    declareProperty("METSystematicVariations", m_METSysNames,"The names of all systematic variations to be applied to the MET" );
    
    declareProperty("SelectionLabel", m_selectionLabel="selected", "Input label for the OverlapRemovalTool");
    declareProperty("OverlapLabel", m_overlapLabel="overlaps", "Output label for the OverlapRemovalTool");
  }

  StatusCode WyyMCAnalysis :: initialize ()
  {
    ANA_MSG_DEBUG ("Initializing - Wyy MC Analysis");
    if (m_ElectronChannel){
      ANA_MSG_INFO("enuyy Analysis");
      
    } else if (m_MuonChannel){
      ANA_MSG_INFO("munuyy Analysis");
    }
    ANA_MSG_INFO("Analysis Flags: " << m_ElectronChannel << ", " << m_MuonChannel);
    ANA_MSG_INFO("Trigger System: Legacy " << m_Legacy << ", Phase I " << m_PhaseI);
    
    //Reco level containers
    ANA_CHECK(m_PhotonContainerKey.initialize());
    ANA_CHECK(m_ElectronContainerKey.initialize());
    ANA_CHECK(m_MuonContainerKey.initialize());
    ANA_CHECK(m_TauJetContainerKey.initialize());
    ANA_CHECK(m_JetPflowContainerKey.initialize());
    ANA_CHECK(m_VertexContainerKey.initialize());
    
    //Truth level containers
    ANA_CHECK(m_TruthPhotonContainerKey.initialize());
    ANA_CHECK(m_TruthElectronContainerKey.initialize());
    ANA_CHECK(m_TruthMuonContainerKey.initialize());
    ANA_CHECK(m_TruthTauContainerKey.initialize());
    ANA_CHECK(m_TruthNeutrinoContainerKey.initialize());
    ANA_CHECK(m_METTruthContainerKey.initialize());
    
    ANA_CHECK(m_AmbiguityTool.retrieve());
    ANA_CHECK(m_isolationSelectionTool.retrieve());
    ANA_CHECK(m_MuonSelectionTool.retrieve()); 
    ANA_CHECK(m_orTool.retrieve());
    //ANA_CHECK(m_JvtToolHandle.retrieve());
    
    
    
    ANA_CHECK(m_METMaker.retrieve());
    asg::AnaToolHandle<IMETSystematicsTool> m_METSystTool;
    m_METSystTool.setTypeAndName("met::METSystematicsTool/metSystTool");
    ANA_CHECK( m_METSystTool.setProperty("ConfigPrefix", "METUtilities/R22_PreRecs"));
    ANA_CHECK( m_METSystTool.setProperty("ConfigSoftTrkFile", "TrackSoftTerms-pflow.config")); //to do the jet track systematics
    //ANA_CHECK( m_METSystTool.setProperty("UseDevArea",true )); // To get the configs from GROUPDATA/dev/METUtilities
    ANA_CHECK( m_METSystTool.retrieve() );
    
    /*
    if (!m_PHYS){
      ANA_MSG_DEBUG( "Using DAOD_PHYSLITE data format \n" );
    } else {
      ANA_MSG_DEBUG( "Using DAOD_PHYS data format - Container calibration needed \n" );
      
      //Electron and photon calibration
      ANA_CHECK(m_EgammaCalibrationAndSmearingTool.retrieve()); 
      
      //Jet calibration and cleaning
      m_JetPflowCalibrationTool.setTypeAndName("JetCalibrationTool/JPflowCalibTool");
      if( !m_JetPflowCalibrationTool.isUserConfigured() ){
      ANA_CHECK( m_JetPflowCalibrationTool.setProperty("JetCollection","AntiKt4EMPFlow") );
      ANA_CHECK( m_JetPflowCalibrationTool.setProperty("ConfigFile","PreRec_R22_PFlow_ResPU_EtaJES_GSC_February23_230215.config") );
      ANA_CHECK( m_JetPflowCalibrationTool.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC") ); //add _Insitu for data
      ANA_CHECK( m_JetPflowCalibrationTool.setProperty("CalibArea","00-04-82") );
      ANA_CHECK( m_JetPflowCalibrationTool.setProperty("IsData",false) );
      ANA_CHECK( m_JetPflowCalibrationTool.retrieve() );
      }
      m_JetCleaningTool = new JetCleaningTool(JetCleaningTool::LooseBad,false);
      ANA_CHECK(m_JetCleaningTool->initialize());
      
      ToolHandle<TauAnalysisTools::ITauSmearingTool> m_TauSmearingTool("TauAnalysisTools::TauSmearingtool/TauSmearingTool");
      //m_TauSmearingTool.setTypeAndNameToolHandle<TauAnalysisTools::ITauSmearingTool> m_TauSmearingTool;;
      ANA_CHECK( m_TauSmearingTool.setProperty("RecommendationTag","2022-prerec"));
      ANA_CHECK( m_TauSmearingTool.setProperty("Campaign","mc23"));
      ANA_CHECK( m_TauSmearingTool.retrieve() );
      
    }
    */
      
    
    if ( !m_trigDecTool.empty() ) {
      ANA_CHECK( m_trigDecTool.retrieve() );
      ANA_MSG_DEBUG( "TDT retrieved" );
      if (!m_r3MatchingTool.empty()) {
	ANA_CHECK(m_r3MatchingTool.retrieve());
	ANA_MSG_DEBUG( "MT retrieved" );
      }
    }
    
    //Efficiency Tools - get scale factors
    //Photons: PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map1.txt
    m_gEffSFTool_reco.setTypeAndName("AsgPhotonEfficiencyCorrectionTool/AsgPhotonEfficiencyCorrectionTool_reco");
    ANA_CHECK(m_gEffSFTool_reco.setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map1.txt"));
    ANA_CHECK(m_gEffSFTool_reco.setProperty("RecoKey","Reconstruction"));
    ANA_CHECK(m_gEffSFTool_reco.setProperty("ForceDataType",1));
    ANA_CHECK(m_gEffSFTool_reco.retrieve());
    m_gEffSFTool_ID.setTypeAndName("AsgPhotonEfficiencyCorrectionTool/AsgPhotonEfficiencyCorrectionTool_ID");
    ANA_CHECK(m_gEffSFTool_ID.setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map1.txt"));
    ANA_CHECK(m_gEffSFTool_ID.setProperty("IdKey","Tight"));
    ANA_CHECK(m_gEffSFTool_ID.setProperty("ForceDataType",1));
    ANA_CHECK(m_gEffSFTool_ID.retrieve());
    m_gEffSFTool_iso.setTypeAndName("AsgPhotonEfficiencyCorrectionTool/AsgPhotonEfficiencyCorrectionTool_iso");
    ANA_CHECK(m_gEffSFTool_iso.setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map1.txt"));
    ANA_CHECK(m_gEffSFTool_iso.setProperty("IdKey","Tight"));
    ANA_CHECK(m_gEffSFTool_iso.setProperty("IsoKey","TightCaloOnly"));
    ANA_CHECK(m_gEffSFTool_iso.setProperty("ForceDataType",1));
    ANA_CHECK(m_gEffSFTool_iso.retrieve());
    
    /*
    m_gEffSFTool_trig.setTypeAndName("AsgPhotonEfficiencyCorrectionTool/AsgPhotonEfficiencyCorrectionTool_trig"); 
    ANA_CHECK(m_gEffSFTool_trig.setProperty("OutputLevel", MSG::DEBUG) );
    if( !m_gEffSFTool_trig.isUserConfigured() ){
      //ANA_CHECK(m_gEffSFTool_trig.setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map1.txt"));
      ANA_CHECK(m_gEffSFTool_trig.setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map0.txt"));
      //"PhotonEfficiencyCorrection/2015_2018/rel21.2/Summer2020_Rec_v1/map1.txt"
      //"PhotonEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map0.txt"
      //ANA_CHECK(m_gEffSFTool_trig.setProperty("TriggerKey","Trigger_2017_2018_g12_loose_TightCaloOnly")); 
      //ANA_CHECK(m_gEffSFTool_trig.setProperty("IdKey","Loose"));
      ANA_CHECK(m_gEffSFTool_trig.setProperty("IsoKey","Loose"));
      ANA_CHECK(m_gEffSFTool_trig.setProperty("TriggerKey","DI_PH_2015_2016_g25_loose_2017_2018_g50_loose_L1EM20VH_Loose"));
      
      
      ANA_CHECK(m_gEffSFTool_trig.setProperty("ForceDataType",1));
      //ANA_CHECK(m_gEffSFTool_trig.setProperty("CorrelationModel", "FULL") );
      ANA_CHECK(m_gEffSFTool_trig.initialize());
    }
    */
    
    
    //Electrons: ElectronEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map4.txt
    m_eEffSFTool_reco.setTypeAndName("AsgElectronEfficiencyCorrectionTool/AsgElectronEfficiencyCorrectionTool_reco");
    ANA_CHECK(m_eEffSFTool_reco.setProperty("MapFilePath", "ElectronEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map4.txt"));
    ANA_CHECK(m_eEffSFTool_reco.setProperty("RecoKey","Reconstruction"));
    ANA_CHECK(m_eEffSFTool_reco.retrieve());
    m_eEffSFTool_ID.setTypeAndName("AsgElectronEfficiencyCorrectionTool/AsgElectronEfficiencyCorrectionTool_ID");
    ANA_CHECK(m_eEffSFTool_ID.setProperty("MapFilePath", "ElectronEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map4.txt"));
    ANA_CHECK(m_eEffSFTool_ID.setProperty("IdKey","Medium"));
    ANA_CHECK(m_eEffSFTool_ID.retrieve());
    m_eEffSFTool_iso.setTypeAndName("AsgElectronEfficiencyCorrectionTool/AsgElectronEfficiencyCorrectionTool_iso");
    ANA_CHECK(m_eEffSFTool_iso.setProperty("MapFilePath", "ElectronEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map4.txt"));
    ANA_CHECK(m_eEffSFTool_iso.setProperty("IdKey","Medium"));
    ANA_CHECK(m_eEffSFTool_iso.setProperty("IsoKey","Loose_VarRad"));
    ANA_CHECK(m_eEffSFTool_iso.retrieve());
    
    m_eEffSFTool_trig.setTypeAndName("AsgElectronEfficiencyCorrectionTool/AsgElectronEfficiencyCorrectionTool_trig"); //fails sometimes, deactivated for now
    if( !m_eEffSFTool_trig.isUserConfigured() ){
      ANA_CHECK(m_eEffSFTool_trig.setProperty("MapFilePath", "ElectronEfficiencyCorrection/2015_2025/rel22.2/2022_Summer_Prerecom_v1/map4.txt"));
      ANA_CHECK(m_eEffSFTool_trig.setProperty("TriggerKey","2022_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI"));
      ANA_CHECK(m_eEffSFTool_trig.setProperty("IdKey","Tight"));
      ANA_CHECK(m_eEffSFTool_trig.setProperty("IsoKey","Tight_VarRad"));
      ANA_CHECK(m_eEffSFTool_trig.setProperty("CorrelationModel", "FULL") );
      ANA_CHECK(m_eEffSFTool_trig.setProperty("ForceDataType",1));
      //ANA_CHECK(m_eEffSFTool_trig.setProperty("OutputLevel", MSG::DEBUG) );
      ANA_CHECK(m_eEffSFTool_trig.initialize());
    }
    
    //Muons: MuonEfficiencyCorrections/*_Preliminary_r22run3/
    m_muEffSFTool_ID.setTypeAndName("CP::MuonEfficiencyScaleFactors/MuonEfficiencyCorrectionTool_ID");
    ANA_CHECK(m_muEffSFTool_ID.setProperty("WorkingPoint","Medium"));
    ANA_CHECK(m_muEffSFTool_ID.setProperty("CalibrationRelease","230309_Preliminary_r22run3"));
    ANA_CHECK(m_muEffSFTool_ID.retrieve());
    m_muEffSFTool_TTVA.setTypeAndName("CP::MuonEfficiencyScaleFactors/MuonEfficiencyCorrectionTool_TTVA");
    ANA_CHECK(m_muEffSFTool_TTVA.setProperty("WorkingPoint","TTVA"));
    ANA_CHECK(m_muEffSFTool_TTVA.setProperty("CalibrationRelease","230309_Preliminary_r22run3"));
    ANA_CHECK(m_muEffSFTool_TTVA.retrieve());
    m_muEffSFTool_iso.setTypeAndName("CP::MuonEfficiencyScaleFactors/MuonEfficiencyCorrectionTool_iso");
    ANA_CHECK(m_muEffSFTool_iso.setProperty("WorkingPoint","Loose_VarRadIso"));
    ANA_CHECK(m_muEffSFTool_iso.setProperty("CalibrationRelease","220817_Preliminary_r22run3"));
    ANA_CHECK(m_muEffSFTool_iso.retrieve());
    
    
    m_TrigGlobEffTool.setTypeAndName("TrigGlobalEfficiencyCorrectionTool/TrigGlobal");
    const char* triggers2022;
    if (m_ElectronChannel){
      triggers2022 = "e24_lhmedium_nod0_2g12_loose"; // || e24_lhmedium_nod0_L1EM20VH || g12_loose || g12_loose";
      //"e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH";
    } else if (m_MuonChannel){
      triggers2022 = "2g10_loose_mu20_L1MU18VFCH"; //2g10_loose_mu20_L1MU14FCH  || g10_loose || g10_loose || mu20_L1MU14FCH
    }
    ANA_CHECK(m_TrigGlobEffTool.setProperty("TriggerCombination2022", triggers2022));
    //ANA_CHECK(m_TrigGlobEffTool.setProperty("TriggerMatchingTool", m_r3MatchingTool.getHandle()));
    ANA_CHECK(m_TrigGlobEffTool.setProperty("OutputLevel", MSG::DEBUG));
    ANA_CHECK(m_TrigGlobEffTool.initialize());
    

    
    m_muEffSFTool_trig.setTypeAndName("CP::MuonTriggerScaleFactors/MuonEfficiencyCorrectionTool_trig");
    ANA_CHECK(m_muEffSFTool_trig.setProperty("MuonQuality","Medium"));
    //ANA_CHECK(m_muEffSFTool_trig.setProperty("isData",false));
    ANA_CHECK(m_muEffSFTool_trig.retrieve());
    
    m_IsoCorrTool.setTypeAndName("CP::IsolationCorrectionTool/IsoCorrTool");
    ANA_CHECK(m_IsoCorrTool.setProperty("ToolVer","REL22"));
    ANA_CHECK(m_IsoCorrTool.setProperty("IsMC",true));
    ANA_CHECK(m_IsoCorrTool.setProperty("CorrFile","IsolationCorrections/v6/isolation_ptcorrections_rel22_mc20.root"));
    ANA_CHECK(m_IsoCorrTool.retrieve());
    
    ANA_CHECK(m_gPointingTool.retrieve());
    
    m_BTaggingSelectionTool.setTypeAndName("BTaggingSelectionTool/BTaggerTool");
    ANA_CHECK( m_BTaggingSelectionTool.setProperty("OperatingPoint","FixedCutBEff_77"));
    ANA_CHECK( m_BTaggingSelectionTool.setProperty("TaggerName","DL1dv01"));
    ANA_CHECK( m_BTaggingSelectionTool.setProperty("JetAuthor", "AntiKt4EMPFlowJets"));
    ANA_CHECK( m_BTaggingSelectionTool.setProperty("MinPt", 20000.));
    ANA_CHECK( m_BTaggingSelectionTool.setProperty("FlvTagCutDefinitionsFileName","xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root"));
    ANA_CHECK( m_BTaggingSelectionTool.retrieve() );
    
    m_BTaggingEfficiencyTool.setTypeAndName("BTaggingEfficiencyTool/BTaggerEffTool");
    ANA_CHECK( m_BTaggingEfficiencyTool.setProperty("OperatingPoint","FixedCutBEff_77"));
    ANA_CHECK( m_BTaggingEfficiencyTool.setProperty("TaggerName","DL1dv01"));
    ANA_CHECK( m_BTaggingEfficiencyTool.setProperty("JetAuthor", "AntiKt4EMPFlowJets"));
    ANA_CHECK( m_BTaggingEfficiencyTool.setProperty("MinPt", 20000.));
    ANA_CHECK( m_BTaggingEfficiencyTool.setProperty("ScaleFactorFileName","xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root"));
    ANA_CHECK( m_BTaggingEfficiencyTool.retrieve() );
    
    m_NNJvtEfficiencyTool.setTypeAndName("CP::JetJvtEfficiency/NNJvtEffTool");
    ANA_CHECK( m_NNJvtEfficiencyTool.setProperty("JetContainer","AntiKt4EMPFlowJets"));
    ANA_CHECK( m_NNJvtEfficiencyTool.setProperty("WorkingPoint","FixedEffPt")); //There is no Tight WP
    ANA_CHECK( m_NNJvtEfficiencyTool.setProperty("MaxPtForJvt",60000.));
    ANA_CHECK( m_NNJvtEfficiencyTool.setProperty("SFFile","JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlowJets.root"));
    ANA_CHECK( m_NNJvtEfficiencyTool.retrieve());

    //GENERAL
    ANA_CHECK (book (TH1F ("h_EvtWeights","Event Weights distribution", 50 , -1.e6,1.5e6)));
    ANA_CHECK (book (TH1F ("h_PUWeights","Pileup Weights distribution", 60 , 0.,6.)));
    ANA_CHECK (book (TH1F ("h_EvtWeights_1bin","Sum of event weights", 1 , 0,1)));
    ANA_CHECK (book (TH1F ("h_Cutflow_selection_lyy", "Cutflow", 13, 0, 13)));
    
    if (m_PHYS){
      ANA_CHECK (book (TH1F ("h_AllPhotonspt_noCalib", "All Photons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllMuonspt_noCalib", "All Muons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllElectronspt_noCalib", "All Electrons before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllTauJetspt_noCalib", "All Tau Jets", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllJetspt_noCalib", "All Jets before calibration", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_AllTopoJetspt_noCalib", "All Jets - EMTopo", 50, 0., 200)));
    }
      
    ANA_CHECK (book (TH1F ("h_AllPhotonspt", "All Photons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllPhotonseta", "All Photons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllPhotonsphi", "All Photons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberPhotons", "Number of Photons", 13, 0, 13)));
    ANA_CHECK (book (TH1F ("h_basephotonspt", "Baseline photons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlygampt", "Baseline Photons - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllElectronspt", "All Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllElectronseta", "All Electrons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllElectronsphi", "All Electrons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_AllElectronscharge", "All Electrons Charge", 3, -1.0, 2.0)));
    ANA_CHECK (book (TH1F ("h_NumberElectrons", "Number of Electrons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_NumberPreselectElectrons", "Number of Preselect Electrons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_NumberbaseElectrons", "Number of Baseline Electrons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_preselelectronspt", "Preselect Electrons - before OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlyelept", "Preselect Electrons - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlyelept_noTrigSF", "Preselect Electrons - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_baseelectronspt", "Baseline Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllMuonspt", "All Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllMuonseta", "All Muons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllMuonsphi", "All Muons Phi", 32, -3.2, 3.2))); 
    ANA_CHECK (book (TH1F ("h_AllMuonscharge", "All Muons Charge", 3, -1.0, 2.0)));
    ANA_CHECK (book (TH1F ("h_NumberMuons", "Number of Muons", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_NumberPreselectMuons", "Number of Preselect Muons", 6, 0, 6)));
    ANA_CHECK (book (TH1F ("h_NumberbaseMuons", "Number of Baseline Muons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_preselmuonspt", "Preselect Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_preselmuonseta", "All Muons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_onlymupt", "Preselect Muons - after OR)", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlymupt_noTrigSF", "Preselect Muons - after OR)", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_basemuonspt", "Baseline Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllTauJetspt", "All Tau Jets", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllTauJetseta", "All Tau Jets Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllTauJetsphi", "All Tau Jets Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTauJets", "Number of Tau Jets", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_AllJetspt", "All Jets - PFlow", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllJetseta", "All PFlow Jets Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_AllJetsphi", "All PFlow Jets Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberJets", "Number of PFlow Jets", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_NumberbaseJets", "Number of Baseline PFlow Jets", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_basejetspt", "Baseline PFlow Jets - before OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_onlyjetpt", "Baseline PFlow Jets - after OR", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_NumberJets_noPU", "Number of Jets - no pileup", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_NumberBtaggedJets", "Number of b Jets", 5, 0, 5)));
    
    //Truth level plots
    ANA_CHECK (book (TH1F ("h_TruthPhotonspt", "All Truth Photons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthPhotonspt_baseline", "Baseline Truth Photons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthPhotonspt_baseline_prompt", "Baseline Truth Photons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthPhotonseta", "All Truth Photons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthPhotonsphi", "All Truth Photons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTruthPhotons", "Number of Truth Photons", 20, 0, 20)));
    ANA_CHECK (book (TH1F ("h_TruthElectronspt", "All Truth Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthElectronspt_preselect", "Preselect Truth Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthElectronspt_preselect_prompt", "Preselect Truth Electrons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthElectronseta", "All Truth Electrons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthElectronsphi", "All Truth Electrons Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTruthElectrons", "Number of Truth Electrons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_TruthMuonspt", "All Truth Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthMuonspt_preselect", "Preselect Truth Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthMuonspt_preselect_trig", "Preselect Truth Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthMuonspt_preselect_prompt", "Preselect Prompt Truth Muons", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthMuonseta", "All Truth Muons Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthMuonsphi", "All Truth Muons Phi", 32, -3.2, 3.2))); 
    ANA_CHECK (book (TH1F ("h_NumberTruthMuons", "Number of Truth Muons", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_TruthTauspt", "All Truth Taus", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthTauseta", "All Truth Taus Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthTausphi", "All Truth Taus Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTruthTaus", "Number of Truth Taus", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_TruthJetspt", "All Truth Jets", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthJetspt_baseline", "Baseline Truth Jets", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthJetseta", "All Truth Jets Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthJetsphi", "All Truth Jets Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTruthJets", "Number of Truth Jets", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_TruthNeutrinospt", "All Truth Neutrinos", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthNeutrinospt_prompt", "All Truth Prompt Neutrinos", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthNeutrinoseta", "All Truth Neutrinos Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_TruthNeutrinosphi", "All Truth Neutrinos Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_NumberTruthNeutrinos", "Number of Truth Neutrinos", 10, 0, 10)));
    ANA_CHECK (book (TH1F ("h_TruthMETmet_exp", "Truth MET - Sum of MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_TruthMETphi", "All Truth MET Phi", 32, -3.2, 3.2)));
    
    ANA_CHECK (book (TH1F ("h_muSFtrig", "Single Muon Trigger Scale Factors - HLT_mu24_ivarmedium_L1MU14FCH", 50, 0., 1.5)));
    ANA_CHECK (book (TH2F ("h_muSFtrig_pt","Single muon trigger SFs",50,20.,200.,30,0.,1.5)));
    ANA_CHECK (book (TH1F ("h_NNJvtSF_eff", "NNJvt Scale Factors - PU-tagged jets", 50, 0.5, 1.5)));
    ANA_CHECK (book (TH1F ("h_NNJvtSF_ineff", "NNJvt Scale Factors (Inefficiency) - non PU-tagged jets", 50, 0.5, 1.5)));
    ANA_CHECK (book (TH1F ("h_nonBTagSF", "BTag Scale Factors", 50, 0.95, 1.15)));
    ANA_CHECK (book (TH1F ("h_btagInefficiency", "BTag Inefficiency per event", 50, 0.5, 1.5)));
    
    //Overlaps
    ANA_CHECK (book (TH1F("h_DRej", "DR electrons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgj", "DR photons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRge", "DR photons to electrons", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgmu", "DR photons to muons", 42,-0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmue", "DR muons to electrons", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmuj", "DR muons to jets", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgt", "DR photons to taus", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmut", "DR muons to taus", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRtj", "DR jets to taus", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRet", "DR electrons to taus", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRej_noOL", "DR electrons to jets - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgj_noOL", "DR photons to jets - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRge_noOL", "DontainerR photons to electrons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgmu_noOL", "DR photons to muons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmue_noOL", "DR muons to electrons - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmuj_noOL", "DR muons to jets - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRgt_noOL", "DR photons to taus - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRmut_noOL", "DR muons to taus - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRtj_noOL", "DR jets to taus - after OR", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DRet_noOL", "DR electrons to taus - after OR", 42, -0.2, 4.)));
    
    
    
    if (m_ElectronChannel){
      ANA_CHECK (book (TH1F ("h_Cutflow_events", "HLT Chains ", 5, 0, 5)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_e", "Track Isolation ", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_e", "Calorimeter Isolation ", 120, -0.5, 0.1)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_eiso", "Track Isolation Tight_VarRad", 30, -0.1, 0.05)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_eiso", "Calorimeter Isolation Tight_VarRad", 120, -0.5, 0.1)));
      
      ANA_CHECK (book (TH1F ("h_eCutflow_selection", "Electron Selection Cutflow", 13, 0, 13)));
      ANA_CHECK (book (TH1F ("h_EleID_Tests", "Electron ID ", 6, 0, 6)));
      ANA_CHECK (book (TH1F ("h_EleAuthor_Tests", "Ambiguities in Electron Container", 4, 0, 4)));
      ANA_CHECK (book (TH1F ("h_NumbergoodElectrons", "Number of Electrons after selection", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_goodelectronspt", "Electrons that pass SR selection", 50, 0., 200)));
      
      //leading and subleading electrons
      ANA_CHECK (book (TH1F ("h_eLeadingpt", "Leading pt", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_eLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
      ANA_CHECK (book (TH1F ("h_eLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
      
      
    } else if (m_MuonChannel) {
      ANA_CHECK (book (TH1F ("h_Cutflow_events", "HLT Chains ", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_mu", "Track Isolation ", 60, -0.05, 0.01)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_mu", "Calorimeter Isolation ", 120, -0.5, 0.1)));
      ANA_CHECK (book (TH1F ("h_ptvarcone30_muiso", "Track Isolation ", 60, -0.05, 0.01)));
      ANA_CHECK (book (TH1F ("h_topoetcone20_muiso", "Calorimeter Isolation", 120, -0.5, 0.1)));
      
      ANA_CHECK (book (TH1F ("h_muCutflow_selection", "Muons Selection Cutflow", 9, 0, 9)));
      ANA_CHECK (book (TH1F ("h_NumbergoodMuons", "Number of Muons after selection", 7, 0, 7)));
      ANA_CHECK (book (TH1F ("h_goodmuonspt", "Muons that pass SR selection", 50, 0., 200)));
      

      //leading and subleading muons
      ANA_CHECK (book (TH1F ("h_muLeadingpt", "Leading pt", 50, 0., 200)));
      ANA_CHECK (book (TH1F ("h_muLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
      ANA_CHECK (book (TH1F ("h_muLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
      
    }

    //Isolation 
    ANA_CHECK (book (TH1F ("h_topoetcone40_giso", "Calorimeter Isolation TightCaloOnly", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_giso", "Track Isolation TightCaloOnly", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_gcorr", "Calorimeter Isolation - After Isolation Correction", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_gcorr", "Track Isolation - After Isolation Correction", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_g", "Calorimeter Isolation", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_g", "Track Isolation", 60, -0.5, 0.1)));
    //checks
    ANA_CHECK (book (TH1F ("h_topoetcone40_gcorr_tightID", "Calorimeter Isolation - After Isolation Correction", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_gcorr_tightID", "Track Isolation - After Isolation Correction", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_g_tightID", "Calorimeter Isolation", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_g_tightID", "Track Isolation", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_glead_tightID", "Calorimeter Isolation - After Isolation Correction - Lead photon", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_glead_tightID", "Track Isolation - After Isolation Correction- Lead photon", 60, -0.5, 0.1)));
    ANA_CHECK (book (TH1F ("h_topoetcone40_gsublead_tightID", "Calorimeter Isolation - After Isolation Correction - Sublead photon", 80, -10, 30)));
    ANA_CHECK (book (TH1F ("h_ptcone20_gsublead_tightID", "Track Isolation - After Isolation Correction - Sublead photon", 60, -0.5, 0.1)));
    
    //Leading and subleading photons
    ANA_CHECK (book (TH1F ("h_gLeadingpt", "Leading pt", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gLeadingeta", "Leading Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_gLeadingphi", "Leading Phi", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_gSubleadingpt", "Subleading pt", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gSubleadingeta", "Subleading Eta", 30, -3.0, 3.0)));
    ANA_CHECK (book (TH1F ("h_gSubleadingphi", "Subleading Phi", 32, -3.2, 3.2)));   
    
    //Baseline and signal
    ANA_CHECK (book (TH1F ("h_gCutflow_selection", "Photon Selection Cutflow", 9, 0, 9)));
    ANA_CHECK (book (TH1F ("h_NumberbasePhotons", "Number of Baseline Photons", 7, 0, 7)));
    ANA_CHECK (book (TH1F ("h_NumbergoodPhotons", "Number of Photons after selection", 7, 0, 7)));
    ANA_CHECK (book (TH1F ("h_goodphotonspt", "Photons after SR selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gamID_Tests", "Photon ID - DFCommonPhotonsIsEM", 5, 0, 5)));
    ANA_CHECK (book (TH1F ("h_gamAuthor_Tests", "Ambiguities in Photon Container", 4, 0, 4)));
    
    
    //MET plots
    ANA_CHECK (book (TH1F ("h_AllMETpt_SoftClus", "All MET - SoftClus", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllMETpt_PVSoftTrk", "All MET - PVSoftTrk", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_addPVTrknClus", "All MET - PVSoftTrk and SoftClus combined", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_AllMETphi_SoftClus", "All MET - SoftClus", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_AllMETphi_PVSoftTrk", "All MET - PVSoftTrk", 32, -3.2, 3.2)));
    
    ANA_CHECK (book (TH1F ("h_RecoMET_met", "Reconstructed MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_RecoMET_sumet", "Reconstructed MET sum", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_RecoMET_phi", "Reconstructed MET phi", 32, -3.2, 3.2)));
    
    //Understanding TruthMETvsRecoMET - Resolution
    ANA_CHECK (book (TH1F ("h_relMETdiff_0_20_truth", "Relative MET difference - (0,20) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_20_40_truth", "Relative MET difference - (20,40) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_40_60_truth", "Relative MET difference - (40,60) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_60_80_truth", "Relative MET difference - (60,80) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_80_120_truth", "Relative MET difference - (80,120) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_120_160_truth", "Relative MET difference - (120,160) GeV", 50, -1., 1.)));
    ANA_CHECK (book (TH1F ("h_relMETdiff_160_200_truth", "Relative MET difference - (160,200) GeV", 50, -1., 1.)));
    
    ANA_CHECK (book (TH1F ("h_gMETpt", "Photon MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_eMETpt", "Electron MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_muMETpt", "Muon MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_tMETpt", "Tau Jets MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_jMETpt", "Jet EMPFlow MET", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_expMETsum", "Sum of all MET components - explicit sum", 50, 0., 200)));
    
    //W Boson plots
    ANA_CHECK (book (TH1F ("h_WmT_nocuts", "W Boson transverse mass before cuts", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_WmT", "W Boson transverse mass", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_WpT_nocuts", "W Boson transverse momentum before cuts", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_WpT", "W Boson transverse momentum", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_Wcharge", "W Boson charge", 3, -1., 2.)));
    
    ANA_CHECK (book (TH1F("h_DR_glead_leplead", "DR leading photon to leading lepton", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DR_gsublead_leplead", "DR subleading photon to leading lepton", 42, -0.2, 4.)));
    ANA_CHECK (book (TH1F("h_DR_glead_gsublead", "DR leading photon to subleading lepton", 42, -0.2, 4.)));
  
    
    //Final plots, after all selection
    ANA_CHECK (book (TH1F ("h_ExpectedEvents_exp", "Expected events after event selection", 1,0,1)));
    ANA_CHECK (book (TH1F ("h_WpT_final", "W Boson transverse momentum - After selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_WmT_final", "W Boson transverse mass - After selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_Wcharge_final", "W Boson charge - After selection", 3, -1., 2.)));
    if (m_ElectronChannel){
      ANA_CHECK (book (TH1F ("h_eLeadingpt_final", "Leading Electron pt - After selection", 50, 0., 200)));
    } else if (m_MuonChannel){
      ANA_CHECK (book (TH1F ("h_muLeadingpt_final", "Leading Muon pt - After selection", 50, 0., 200)));
    }
    ANA_CHECK (book (TH1F ("h_gLeadingpt_final", "Leading Photon pt - After selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_gSubleadingpt_final", "Subleading Photon pt - After selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_RecoMET_met_final", "All MET - FinalTrk - After selection", 50, 0., 200)));
    ANA_CHECK (book (TH1F ("h_RecoMET_phi_final", "All MET - FinalTrk - After selection", 32, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_AllMETpt_PVSoftTrk_final", "All MET - PVSoftTrk - After selection", 50, 0., 200)));
    
    ANA_CHECK (book (TH1F ("h_myy", "Diphoton invariant mass", 30, 0., 300)));
    ANA_CHECK (book (TH1F ("h_mlyy", "Lepton and diphoton invarian mass", 30, 0., 300)));
    ANA_CHECK (book (TH1F ("h_mly1", "Leading photon and leading muon invariant mass", 30, 0., 300)));
    ANA_CHECK (book (TH1F ("h_mly2", "Subleading photon and leading muon invariant mass", 30, 0., 300)));
    
    return StatusCode::SUCCESS;
  }


  StatusCode WyyMCAnalysis :: execute ()
  {
    // Everything that needs to be done in every event
    ANA_MSG_INFO ("in execute");
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
    
    ANA_MSG_INFO("Luminosity (fb-1): " << m_Luminosity << ", Cross Section (fb): " << m_xsection <<", Sum of Weights of this file: " << getSumOfWeights() << ", Sum of weights of whole sample: " << m_SoW);
    ANA_MSG_INFO("Scale of the histograms: " << m_Luminosity*m_xsection/m_SoW);
    
    bool isMC = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
    double evtWeight = eventInfo->mcEventWeight(); //this one will be modified by extra SF
    double mcWeight = eventInfo->mcEventWeight(); //this one won't be modified
    hist("h_Cutflow_events")->Fill(0.5,evtWeight);
    ANA_MSG_DEBUG("Is this a simulation? : " << isMC);
    ANA_MSG_INFO ("In execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber() << ", eventWeight = " << evtWeight);
    //const Float_t eventWeight = evtWeight;
    hist("h_EvtWeights")->Fill(evtWeight);
    hist("h_EvtWeights_1bin")->Fill(0.5,evtWeight);
    
    /*
    if (m_PHYS){
      m_isBG = false;
      m_isLArBurst = false;
      
      m_isBG = eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->HaloMuonTwoSided);
      m_isLArBurst = eventInfo->isEventFlagBitSet(eventInfo->LAr,eventInfo->LArECTimeDiffHalo);
      if ( m_isBG || m_isLArBurst ) return StatusCode::SUCCESS;
      //+Pileup reweighting
    } else {
    */
    float puWeight = eventInfo->auxdata<float>("PileupWeight_NOSYS");
    evtWeight*=puWeight;
    mcWeight*=puWeight;
    hist("h_PUWeights")->Fill(puWeight);
    //std::cout << "Pileup weight: " << puWeight << "\n";
    
    ANA_MSG_DEBUG("Setting up flags for OR Tool");
    static ort::inputDecorator_t selDec(m_selectionLabel);
    static ort::outputAccessor_t overlapAcc(m_overlapLabel);

    
    ANA_MSG_DEBUG ("Checking triggers that fired ");
    if (m_ElectronChannel){
      //Legacy: HLT_e26_lhtight_ivarloose_L1EM22VHI
      //Phase I: HLT_e26_lhtight_ivarloose_L1eEM26M
      if(m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1EM22VHI")){
	hist("h_Cutflow_events")->Fill(1.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_e26_lhtight_ivarloose_L1eEM26M")){
	hist("h_Cutflow_events")->Fill(2.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      
      //Legacy: HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH
      //Phase I: HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L
      if(m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH")){
	hist("h_Cutflow_events")->Fill(3.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L")){
	hist("h_Cutflow_events")->Fill(4.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel){
      //Legacy: HLT_mu24_ivarmedium_L1MU14FCH
      if(m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(1.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Legacy A: "HLT_2g10_loose_mu20_L1MU14FCH"
      if(m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(3.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      if(m_trigDecTool->isPassed("HLT_mu24_ivarmedium_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(2.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Legacy B: HLT_2g10_loose_mu20_L1MU18VFCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_mu20_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(4.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      
      //Phase I single mu doesnt change?
      
      //Phase I A: HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU14FCH")){
	hist("h_Cutflow_events")->Fill(5.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      //Phase I B: HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH
      if(m_trigDecTool->isPassed("HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH")){
	hist("h_Cutflow_events")->Fill(6.5,evtWeight);
      } else {
	//return StatusCode::SUCCESS;
      }
      
    }
      
    
    
    /*****************************
    * 
    * OBJECT RETRIEVAL - RECO AND TRUTH INFORMATION
    * 
    *****************************/
    ANA_MSG_DEBUG("Looking for the primary vertex");
    const xAOD::Vertex* pVtx(0);
    SG::ReadHandle<xAOD::VertexContainer> vertices(m_VertexContainerKey);
    if (vertices.isValid()) {
      ANA_MSG_DEBUG("Collection with name " << m_VertexContainerKey << " with size " << vertices->size() << " found in evtStore()");
      xAOD::VertexContainer::const_iterator vxItr = vertices->begin();
      xAOD::VertexContainer::const_iterator vxItrE = vertices->end();
      for (; vxItr != vxItrE; ++vxItr) {
        if ((*vxItr)->vertexType() == xAOD::VxType::PriVtx) {
          pVtx = *vxItr;
        } 
      }
    } else {
      ANA_MSG_WARNING("No collection with name " << m_VertexContainerKey << " found in evtStore()");
    }
    hist("h_Cutflow_selection_lyy")->Fill(0.5,evtWeight); //All of the events
    if (pVtx){
      hist("h_Cutflow_selection_lyy")->Fill(1.5,evtWeight); //Events with primary vertex
    } else {
      return StatusCode::SUCCESS;
    }
    
    ANA_MSG_DEBUG("Container retrieval - Reconstruction level");
    SG::ReadHandle<xAOD::PhotonContainer> gammaTES(m_PhotonContainerKey);
    if ( ! gammaTES.isValid() ) {
      ANA_MSG_ERROR("No photon container" <<  m_PhotonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("PhotonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::ElectronContainer> elecTES(m_ElectronContainerKey);
    if ( ! elecTES.isValid() ) {
      ANA_MSG_ERROR("No electron container" <<  m_ElectronContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("ElectronContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::MuonContainer> muTES(m_MuonContainerKey);
    if ( ! muTES.isValid() ) {
      ANA_MSG_ERROR("No muon container" <<  m_MuonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("MuonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::TauJetContainer> TauJetsCon(m_TauJetContainerKey);
    if ( ! TauJetsCon.isValid() ) {
      ANA_MSG_ERROR("No tau jet container" <<  m_TauJetContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TauJetContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::JetContainer> jetPflowCon(m_JetPflowContainerKey);
    if(!jetPflowCon.isValid()){
      ANA_MSG_ERROR("No jet container" << m_JetPflowContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("JetPflowContainer successfully retrieved");
    
    
    
    ANA_MSG_DEBUG("Container retrieval - Truth Containers");
    SG::ReadHandle<xAOD::TruthParticleContainer> gammaTruth(m_TruthPhotonContainerKey);
    if ( ! gammaTruth.isValid() ) {
      ANA_MSG_ERROR("No truth photon container" <<  m_TruthPhotonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TruthPhotonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::TruthParticleContainer> elecTruth(m_TruthElectronContainerKey);
    if ( ! elecTruth.isValid() ) {
      ANA_MSG_ERROR("No truth electron container" <<  m_TruthElectronContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TruthElectronContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::TruthParticleContainer> muTruth(m_TruthMuonContainerKey);
    if ( ! muTruth.isValid() ) {
      ANA_MSG_ERROR("No truth muon container" <<  m_TruthMuonContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TruthMuonContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::TruthParticleContainer> tauTruth(m_TruthTauContainerKey);
    if ( ! tauTruth.isValid() ) {
      ANA_MSG_ERROR("No truth tau container" <<  m_TruthTauContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TruthTauContainer successfully retrieved");
    
    const xAOD::JetContainer* jetTruth(0);
    ANA_CHECK(evtStore()->retrieve(jetTruth,"AntiKt4TruthDressedWZJets"));
    
    SG::ReadHandle<xAOD::TruthParticleContainer> nuTruth(m_TruthNeutrinoContainerKey);
    if ( ! nuTruth.isValid() ) {
      ANA_MSG_ERROR("No truth neutrino container" <<  m_TruthNeutrinoContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("TruthNeutrinoContainer successfully retrieved");
    
    SG::ReadHandle<xAOD::MissingETContainer> metTruth(m_METTruthContainerKey);
    if ( ! metTruth.isValid() ) {
      ANA_MSG_ERROR("No truth MET container" <<  m_METTruthContainerKey << " found in evtStore");
      return StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("METTruthContainer successfully retrieved");
    
    
    
    ANA_MSG_DEBUG ("Creating copies of containers and different selection level containers");
    //Copying containers to add flags, calibrate, sort, etc.
    Int_t Allg_N = 0;
    Int_t Baseg_N = 0;
    double basegsf = 1;
    auto gcopy = xAOD::shallowCopyContainer(*gammaTES);
    ANA_CHECK(evtStore()->record(gcopy.first,"PhotonsCopy"));
    ANA_CHECK(evtStore()->record(gcopy.second, "PhotonsCopyAux"));
    if(!xAOD::setOriginalObjectLink(*gammaTES, *gcopy.first)){
      ANA_MSG_WARNING("Shallow copy photons - Could not set original object link!");
    }
    xAOD::PhotonContainer* baselinephotons = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* baselinephotons_aux = new xAOD::PhotonAuxContainer;
    baselinephotons->setStore(baselinephotons_aux);
    ANA_CHECK(evtStore()->record(baselinephotons,"BaselinePhotons"));
    ANA_CHECK(evtStore()->record(baselinephotons_aux, "BaselinePhotonsAux"));
    
    Int_t Alle_N = 0;
    Int_t PreselEle_N = 0;
    Int_t BaseEle_N = 0;
    double baseesf = 1.;
    auto ecopy = xAOD::shallowCopyContainer(*elecTES);
    ANA_CHECK(evtStore()->record(ecopy.first,"ElectronsCopy"));
    ANA_CHECK(evtStore()->record(ecopy.second, "ElectronsCopyAux"));
    if(!xAOD::setOriginalObjectLink(*elecTES, *ecopy.first)){
      ANA_MSG_WARNING("Shallow copy electrons - Could not set original object link!");
    }
    xAOD::ElectronContainer* preselectelectrons = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* preselectelectrons_aux = new xAOD::ElectronAuxContainer;
    preselectelectrons->setStore(preselectelectrons_aux);
    ANA_CHECK(evtStore()->record(preselectelectrons,"PreselectElectrons"));
    ANA_CHECK(evtStore()->record(preselectelectrons_aux, "PreselectElectronsAux"));
    
    xAOD::ElectronContainer* baselineelectrons = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* baselineelectrons_aux = new xAOD::ElectronAuxContainer;
    baselineelectrons->setStore(baselineelectrons_aux);
    ANA_CHECK(evtStore()->record(baselineelectrons,"BaselineElectrons"));
    ANA_CHECK(evtStore()->record(baselineelectrons_aux, "BaselineElectronsAux"));
    
    Int_t Allmu_N = 0;
    Int_t PreselMu_N = 0;
    Int_t BaseMu_N = 0;
    float basemusf = 1.;
    double musf_trig=1.; //this is applied to the container
    auto mucopy = xAOD::shallowCopyContainer(*muTES);
    ANA_CHECK(evtStore()->record(mucopy.first,"MuonsCopy"));
    ANA_CHECK(evtStore()->record(mucopy.second, "MuonsCopyAux"));
    if(!xAOD::setOriginalObjectLink(*muTES, *mucopy.first)){
      ANA_MSG_WARNING("Shallow copy muons - Could not set original object link!");
    }
    xAOD::MuonContainer* preselectmuons = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* preselectmuons_aux = new xAOD::MuonAuxContainer;
    preselectmuons->setStore(preselectmuons_aux);
    ANA_CHECK(evtStore()->record(preselectmuons,"PreselectMuons"));
    ANA_CHECK(evtStore()->record(preselectmuons_aux, "PreselectMuonsAux"));
    
    xAOD::MuonContainer* baselinemuons = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* baselinemuons_aux = new xAOD::MuonAuxContainer;
    baselinemuons->setStore(baselinemuons_aux);
    ANA_CHECK(evtStore()->record(baselinemuons,"BaselineMuons"));
    ANA_CHECK(evtStore()->record(baselinemuons_aux, "BaselineMuonsAux"));
    
    Int_t Alltj_N = 0;
    auto tjcopy = xAOD::shallowCopyContainer(*TauJetsCon);
    ANA_CHECK(evtStore()->record(tjcopy.first,"TauJetsCopy"));
    ANA_CHECK(evtStore()->record(tjcopy.second, "TauJetsCopyAux"));
    if(!xAOD::setOriginalObjectLink(*TauJetsCon, *tjcopy.first)){
      ANA_MSG_WARNING("Shallow copy taujets - Could not set original object link!");
    }
    Int_t Allj_N = 0;
    Int_t Basej_N = 0;
    Int_t noPUjets_N = 0;
    double jetWeight= 1.;
    auto jPflowcopy = xAOD::shallowCopyContainer(*jetPflowCon);
    ANA_CHECK(evtStore()->record(jPflowcopy.first,"JetsPflowCopy"));
    ANA_CHECK(evtStore()->record(jPflowcopy.second, "JetsPflowCopyAux"));
    if(!xAOD::setOriginalObjectLink(*jetPflowCon, *jPflowcopy.first)){
      ANA_MSG_WARNING("Shallow copy PFlow jets - Could not set original object link!");
    }
    xAOD::JetContainer* baselinejets = new xAOD::JetContainer;
    xAOD::JetAuxContainer* baselinejets_aux = new xAOD::JetAuxContainer;
    baselinejets->setStore(baselinejets_aux);
    ANA_CHECK(evtStore()->record(baselinejets,"BaselineJets"));
    ANA_CHECK(evtStore()->record(baselinejets_aux, "BaselineJetsAux"));
    
    xAOD::JetContainer* noPUjets = new xAOD::JetContainer;
    xAOD::JetAuxContainer* noPUjets_aux = new xAOD::JetAuxContainer;
    noPUjets->setStore(noPUjets_aux);
    ANA_CHECK(evtStore()->record(noPUjets,"nonPUJets"));
    ANA_CHECK(evtStore()->record(noPUjets_aux, "nonPUJetsAux"));
    
    
    
    ANA_MSG_INFO ("Starting basic object selection: calibration (IF NEEDED) and OR flags");
    /*****************************
    *           PHOTONS
    *****************************/
    ANA_MSG_DEBUG ("Reco Photons");
    //if (m_gPointingTool->updatePointingAuxdata(*gcopy.first) !=  StatusCode::SUCCESS){ANA_MSG_WARNING("Cannot apply photon pointing tool!");} //physlite doesnt carry this info?
    for (auto gam: *gcopy.first){
      Allg_N++;
      /*
      if (m_PHYS){
	hist("h_AllPhotonspt_noCalib")->Fill(((gam)->pt())/1000,evtWeight);
	if (m_EgammaCalibrationAndSmearingTool->applyCorrection(*gam, *eventInfo) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");}
      }
      */
      hist("h_gCutflow_selection")->Fill(0.5,evtWeight); 
      hist("h_AllPhotonspt")->Fill(((gam)->pt())/1e3,evtWeight);
      hist("h_AllPhotonseta")->Fill((gam)->caloCluster()->etaBE(2),evtWeight);
      hist("h_AllPhotonsphi")->Fill((gam)->phi(),evtWeight);
      
      //Ambiguities
      hist("h_gamAuthor_Tests")->Fill(0.5,evtWeight);
      
      if (gam->author()==xAOD::EgammaParameters::AuthorPhoton){ 
        hist("h_gamAuthor_Tests")->Fill(1.5,evtWeight);
      }  
      if (gam->author()==xAOD::EgammaParameters::AuthorElectron){ 
        hist("h_gamAuthor_Tests")->Fill(2.5,evtWeight);
      }
      if (gam->author()==xAOD::EgammaParameters::AuthorAmbiguous){
        hist("h_gamAuthor_Tests")->Fill(3.5,evtWeight);
      }
      
      ANA_MSG_DEBUG("Isolation distributions - no WP specified");
      Float_t topoetcone40_g;
      Float_t ptcone20_g;
      (gam)->isolation(topoetcone40_g,xAOD::Iso::topoetcone40);
      (gam)->isolation(ptcone20_g,xAOD::Iso::ptcone20);
      hist("h_topoetcone40_g")->Fill(topoetcone40_g/1e3 -0.022*(gam)->pt()/1e3,evtWeight);
      hist("h_ptcone20_g")->Fill(ptcone20_g/1e3 - 0.05*(gam)->pt()/1e3,evtWeight);
      
      //Plots to check that PU reweighting is done properly
      if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){
	hist("h_topoetcone40_g_tightID")->Fill(topoetcone40_g/1e3 -0.022*(gam)->pt()/1e3,evtWeight);
	hist("h_ptcone20_g_tightID")->Fill(ptcone20_g/1e3 - 0.05*(gam)->pt()/1e3,evtWeight);
      }
      
      if (m_IsoCorrTool->applyCorrection(*gam) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot apply isolation correction!");}
      ANA_MSG_DEBUG("Isolation distributions after Isolation Correction- no WP specified");
      
      Float_t topoetcone40_gcorr;
      Float_t ptcone20_gcorr;
      (gam)->isolation(topoetcone40_gcorr,xAOD::Iso::topoetcone40);
      (gam)->isolation(ptcone20_gcorr,xAOD::Iso::ptcone20);
      hist("h_topoetcone40_gcorr")->Fill(topoetcone40_gcorr/1e3 -0.022*(gam)->pt()/1e3,evtWeight);
      hist("h_ptcone20_gcorr")->Fill(ptcone20_gcorr/1e3 - 0.05*(gam)->pt()/1e3,evtWeight);
      
      //Plots to check that PU reweighting is done properly
      if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){
	hist("h_topoetcone40_gcorr_tightID")->Fill(topoetcone40_gcorr/1e3 -0.022*(gam)->pt()/1e3,evtWeight);
	hist("h_ptcone20_gcorr_tightID")->Fill(ptcone20_gcorr/1e3 - 0.05*(gam)->pt()/1e3,evtWeight);
      }
      
      ANA_MSG_DEBUG("Photon ID tests - including LoosePrime WPs");      
      const unsigned int LoosePrime5 = egammaPID::PhotonLoose | 0x1 << egammaPID::ClusterStripsEratio_Photon;
      const unsigned int LoosePrime4 = egammaPID::PhotonLoose | 0x1 << egammaPID::ClusterStripsWtot_Photon;
      bool passLoosePrime5 = !((gam)->selectionisEM("DFCommonPhotonsIsEMTightIsEMValue") & LoosePrime5);
      bool passLoosePrime4 = !((gam)->selectionisEM("DFCommonPhotonsIsEMTightIsEMValue") & LoosePrime4);
      
      hist("h_gamID_Tests")->Fill(0.5,evtWeight);       
      if ((gam)->passSelection("DFCommonPhotonsIsEMLoose")){
        hist("h_gamID_Tests")->Fill(1.5,evtWeight);
      }  
      if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){
        hist("h_gamID_Tests")->Fill(2.5,evtWeight);
      }
      if (passLoosePrime4){
        hist("h_gamID_Tests")->Fill(3.5,evtWeight);
      }
      if (passLoosePrime5){
        hist("h_gamID_Tests")->Fill(4.5,evtWeight);
      }
      
      if (passAmbCuts(gam)){ 
	hist("h_gCutflow_selection")->Fill(1.5,evtWeight);
	if ((gam)->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON) && (gam)->passSelection("DFCommonPhotonsCleaning") && (gam)->passSelection("DFCommonPhotonsCleaningNoTime")){
	  hist("h_gCutflow_selection")->Fill(2.5,evtWeight);
	  if (passEtaCuts(gam) && ((gam)->passSelection("DFCommonPhotonsIsEMTight") || passLoosePrime5)){ //temporally added here
	    hist("h_gCutflow_selection")->Fill(3.5,evtWeight);
	    if (((gam)->pt()/1e3 > 20.)){ 
	      hist("h_gCutflow_selection")->Fill(4.5,evtWeight);
	    }
	  }
	}
      }
      ANA_MSG_DEBUG("Baseline photons");
      if ( ((gam)->pt()/1e3 > 20.)  && (gam)->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON) && 
	(gam)->passSelection("DFCommonPhotonsCleaning") && (gam)->passSelection("DFCommonPhotonsCleaningNoTime") &&
	passAmbCuts(gam) && ((gam)->passSelection("DFCommonPhotonsIsEMTight") || passLoosePrime5) && passEtaCuts(gam)){ 
	xAOD::Photon* baselinephoton = new xAOD::Photon();
	*baselinephoton = *gam;
	if(!xAOD::setOriginalObjectLink(*gam, *baselinephoton)){
	  ANA_MSG_WARNING("Photons - Could not set original object link!");
	  delete [] baselinephoton;
	  continue;
	}
	selDec(*baselinephoton) = true; //Overlap removal - Input decorator
	baselinephotons->push_back(baselinephoton);
	double gsf_reco= 1.;
	double gsf_ID= 1.;
	double gsf_iso= 1.;
	double gsf_trig= 1.;
	ANA_CHECK(m_gEffSFTool_reco->getEfficiencyScaleFactor(*gam,gsf_reco));
	ANA_CHECK(m_gEffSFTool_ID->getEfficiencyScaleFactor(*gam,gsf_ID));
	//ANA_CHECK(m_gEffSFTool_iso->getEfficiencyScaleFactor(*gam,gsf_iso)); iso is not yet applied
	//ANA_CHECK(m_gEffSFTool_trig->getEfficiencyScaleFactor(*gam,gsf_trig));
	hist("h_basephotonspt")->Fill((gam)->pt()/1e3,evtWeight*gsf_reco*gsf_ID*gsf_iso*gsf_trig);
	Baseg_N++;
	basegsf*= gsf_reco*gsf_ID*gsf_iso*gsf_trig;
      }
    }
    
    baselinephotons->sort([](xAOD::Photon* gam1, xAOD::Photon* gam2){return gam1->pt() > gam2->pt();});
    hist("h_NumberPhotons")->Fill(Allg_N,evtWeight);
    hist("h_NumberbasePhotons")->Fill(Baseg_N,evtWeight*basegsf);
    
    
    /*****************************
    *          ELECTRONS 
    *****************************/
    ANA_MSG_DEBUG ("Reco Electrons");
    for (auto ele: *ecopy.first){
      Alle_N++;
      /*
      if (m_PHYS){
	hist("h_AllElectronspt_noCalib")->Fill((ele)->pt()/1000,evtWeight);
	if (m_EgammaCalibrationAndSmearingTool->applyCorrection(*ele,*eventInfo) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");} 
      }
      */
      hist("h_AllElectronspt")->Fill((ele)->pt()/1e3,evtWeight);
      hist("h_AllElectronseta")->Fill((ele)->caloCluster()->etaBE(2),evtWeight);
      hist("h_AllElectronsphi")->Fill((ele)->phi(),evtWeight);
      hist("h_AllElectronscharge")->Fill((ele)->charge(),evtWeight);
    
      if (m_ElectronChannel){
	hist("h_eCutflow_selection")->Fill(0.5,evtWeight);
	ANA_MSG_DEBUG("Electron Isolation and ID Studies - No WP");
	Float_t topoetcone20_eloose;
	Float_t ptvarcone30_eloose;
	(ele)->isolation(ptvarcone30_eloose,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
	(ele)->isolation(topoetcone20_eloose,xAOD::Iso::topoetcone20);
	hist("h_ptvarcone30_e")->Fill(ptvarcone30_eloose/((ele)->pt())-0.06,evtWeight);
	hist("h_topoetcone20_e")->Fill(topoetcone20_eloose/((ele)->pt())-0.06,evtWeight);
      
	ANA_MSG_DEBUG("Electron Identification");
	hist("h_EleID_Tests")->Fill(0.5,evtWeight);
	if ((ele)->passSelection("DFCommonElectronsLHVeryLoose")){
	  hist("h_EleID_Tests")->Fill(1.5,evtWeight);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHLoose")){
	  hist("h_EleID_Tests")->Fill(2.5,evtWeight);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHLooseBL")){
	  hist("h_EleID_Tests")->Fill(3.5,evtWeight);
	}
	if ((ele)->passSelection("DFCommonElectronsLHMedium")){
	  hist("h_EleID_Tests")->Fill(4.5,evtWeight);
	}  
	if ((ele)->passSelection("DFCommonElectronsLHTight")){
	  hist("h_EleID_Tests")->Fill(5.5,evtWeight);
	} 
	ANA_MSG_DEBUG("Electron Author");
	hist("h_EleAuthor_Tests")->Fill(0.5,evtWeight);
	if (ele->author()==xAOD::EgammaParameters::AuthorPhoton){ 
	  hist("h_EleAuthor_Tests")->Fill(1.5,evtWeight);
	}  
	if (ele->author()==xAOD::EgammaParameters::AuthorElectron){ 
	  hist("h_EleAuthor_Tests")->Fill(2.5,evtWeight);
	}
	if (ele->author()==xAOD::EgammaParameters::AuthorAmbiguous){ 
	  hist("h_EleAuthor_Tests")->Fill(3.5,evtWeight);
	}
      } 
      std::vector<bool> TTVA = TTVAele(ele,pVtx,eventInfo);
      ANA_MSG_DEBUG("Preselect electrons");
      if (m_ElectronChannel){ //Filling Cutflow
	if (passAmbCuts(ele)){
	  hist("h_eCutflow_selection")->Fill(1.5,evtWeight);
	  if ((ele)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)){
	    hist("h_eCutflow_selection")->Fill(2.5,evtWeight);
	    if (passEtaCuts(ele)){
	      hist("h_eCutflow_selection")->Fill(3.5,evtWeight);
	      if ((ele)->passSelection("DFCommonElectronsLHLooseBL")){
		hist("h_eCutflow_selection")->Fill(4.5,evtWeight);
		if ((ele)->pt()/1e3>6.){
		  hist("h_eCutflow_selection")->Fill(5.5,evtWeight);
		  if (TTVA[0]){
		    hist("h_eCutflow_selection")->Fill(6.5,evtWeight);
		  }
		}
	      }
	    }
	  }
	}
      }
      if ( ((ele)->pt()/1e3 > 6.) && (ele)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) && passAmbCuts(ele) && TTVA[0] && (ele)->passSelection("DFCommonElectronsLHLooseBL") && passEtaCuts(ele) ){
	PreselEle_N++;
	xAOD::Electron* preselectelectron = new xAOD::Electron();
	*preselectelectron = (*ele);
	/*
	if(!xAOD::setOriginalObjectLink(*ele, *preselectelectron)){
	  ANA_MSG_WARNING("Preselect electrons - Could not set original object link!");
	  delete [] preselectelectron;
	  continue;
	}
	*/
	selDec(*preselectelectron) = true; //Overlap removal - Input decorator
	preselectelectrons->push_back(preselectelectron);
	hist("h_preselelectronspt")->Fill((ele)->pt()/1e3,evtWeight);
      }
      
    }
    preselectelectrons->sort([](xAOD::Electron* ele1, xAOD::Electron* ele2){return ele1->pt() > ele2->pt();});
    hist("h_NumberElectrons")->Fill(Alle_N,evtWeight);
    hist("h_NumberPreselectElectrons")->Fill(PreselEle_N,evtWeight);
    
   
    /*****************************
    *          MUONS 
    *****************************/
    ANA_MSG_DEBUG ("Reco Muons");
    for (auto mu: *mucopy.first){
      Allmu_N++;
      /*
      if (m_PHYS){
	hist("h_AllMuonspt_noCalib")->Fill((mu)->pt()/1000,evtWeight);
	if (m_MuonCalibTool->applyCorrection(*mu) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");}
      }
      */
      hist("h_AllMuonspt")->Fill((mu)->pt()/1000,evtWeight);
      hist("h_AllMuonseta")->Fill((mu)->eta(),evtWeight);
      hist("h_AllMuonsphi")->Fill((mu)->phi(),evtWeight);
      hist("h_AllMuonscharge")->Fill((mu)->charge(),evtWeight);
      std::vector<bool> TTVA = TTVAmuon(mu,pVtx,eventInfo);
      if (m_MuonChannel){
	hist("h_muCutflow_selection")->Fill(0.5,evtWeight);
	Float_t ptvarcone30_mu;
	Float_t topoetcone20_mu;
	(mu)->isolation(ptvarcone30_mu,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt1000);
	(mu)->isolation(topoetcone20_mu,xAOD::Iso::topoetcone20);
	hist("h_ptvarcone30_mu")->Fill(ptvarcone30_mu/((mu)->pt()) - 0.04,evtWeight);
	hist("h_topoetcone20_mu")->Fill(topoetcone20_mu/((mu)->pt()) -0.15,evtWeight);
	
	if (m_MuonSelectionTool->accept(*mu)){ //eta and ID
	  hist("h_muCutflow_selection")->Fill(1.5,evtWeight);
	  if ((mu)->pt()/1e3 > 6.){
	    hist("h_muCutflow_selection")->Fill(2.5,evtWeight);
	    if (TTVA[0]){
	      hist("h_muCutflow_selection")->Fill(3.5,evtWeight);
	      
	    }
	  }
	}
      }
      ANA_MSG_DEBUG("Preselect muons selection");
      if ((m_MuonSelectionTool->accept(*mu)) && (mu)->pt()/1e3 > 6. && TTVA[0]){
	PreselMu_N++;
	xAOD::Muon* preselectmuon = new xAOD::Muon();
	*preselectmuon = (*mu);
	/*
	if(!xAOD::setOriginalObjectLink(*mu, *preselectmuon)){
	  ANA_MSG_WARNING("Preselect muons - Could not set original object link!");
	  delete [] preselectmuon;
	  continue;
	}
	*/
	selDec(*preselectmuon) = true; //Overlap removal - Input decorator
	preselectmuons->push_back(preselectmuon);
	hist("h_preselmuonspt")->Fill((mu)->pt()/1e3,evtWeight);
	hist("h_preselmuonseta")->Fill((mu)->eta(),evtWeight);
      }
    }
    preselectmuons->sort([](xAOD::Muon* mu1, xAOD::Muon* mu2){return mu1->pt() > mu2->pt();});
    hist("h_NumberMuons")->Fill(Allmu_N,evtWeight);
    hist("h_NumberPreselectMuons")->Fill(PreselMu_N,evtWeight);

    /*****************************
    *          TAU JETS 
    *****************************/
    ANA_MSG_DEBUG ("Reco Tau Jets");
    for (auto tj: *tjcopy.first){
      Alltj_N++;
      /*
      if (m_PHYS){
	hist("h_AllTauJetspt_noCalib")->Fill((tj)->pt()/1000,evtWeight);
	//if (m_TauSmearingTool->applyCorrection(*tj) != CP::CorrectionCode::Ok){ANA_MSG_WARNING("Cannot calibrate particle!");}
      }
      */
      selDec(*tj) = true; //Directly in the copy - all taujets included
      hist("h_AllTauJetspt")->Fill((tj)->pt()/1000,evtWeight);
      hist("h_AllTauJetseta")->Fill((tj)->eta(),evtWeight);
      hist("h_AllTauJetsphi")->Fill((tj)->phi(),evtWeight);
    }
    hist("h_NumberTauJets")->Fill(Alltj_N,evtWeight);
    
    
    /*****************************
    *          JETS 
    *****************************/
    ANA_MSG_DEBUG ("Reco PFlow Jets"); //Mainly removing pileup jets
    /*
    if (m_PHYS){
      for(auto jet: *jetPflowCon){ //this fails, misses variable HFrac
	hist("h_AllJetspt_noCalib")->Fill((jet)->pt()/1000,jetWeight);
	if(!static_cast<bool>(m_JetCleaningTool->accept(*jet))){
	    return StatusCode::SUCCESS;
	}
      }
      if(m_JetPflowCalibrationTool->applyCalibration(*jPflowcopy.first) != StatusCode::SUCCESS){ANA_MSG_WARNING("Cannot calibrate jet!");}
    }
    */
    
    ANA_CHECK(m_NNJvtEfficiencyTool->recalculateScores(*jPflowcopy.first));
    ANA_CHECK(m_NNJvtEfficiencyTool->tagTruth(jPflowcopy.first,jetTruth));
    for (auto jet: *jPflowcopy.first){
      Allj_N++;
      hist("h_AllJetspt")->Fill((jet)->pt()/1e3,mcWeight);
      hist("h_AllJetseta")->Fill((jet)->eta(),mcWeight);
      hist("h_AllJetsphi")->Fill((jet)->phi(),mcWeight);
      float NNJvtsf = 1.;
      ANA_MSG_DEBUG("Baseline jets");
      if ( ((jet)->pt()/1e3 > 20.)  && fabs((jet)->eta())<4.5){ 
	xAOD::Jet* baselinejet = new xAOD::Jet();
	*baselinejet = *jet;
	/*
	if(!xAOD::setOriginalObjectLink(*jet, *baselinejet)){
	  ANA_MSG_WARNING("Jets - Could not set original object link!");
	  delete [] baselinejet;
	  continue;
	}
	*/
	baselinejets->push_back(baselinejet);
	CP::CorrectionCode code;
	//if(m_NNJvtEfficiencyTool->passesJvtCut(*jet) ){ //Later we will use these nonPU jets to veto the b-jets 
	if (m_NNJvtEfficiencyTool->passesJvtCut(*jet) ){ //&&  (jet)->pt()/1e3>20. && (jet)->pt()/1e3<60. && fabs((jet)->eta())<2.4
	  noPUjets_N++;
	  xAOD::Jet* noPUjet = new xAOD::Jet();
	  /*
	  if(!xAOD::setOriginalObjectLink(*jet, *noPUjet)){
	    ANA_MSG_WARNING("Non PU Jets - Could not set original object link!");
	    delete [] noPUjet;
	    continue;
	  }
	  */
	  *noPUjet = *jet;
	  selDec(*noPUjet) = true; //Overlap removal - Input decorator
	  noPUjets->push_back(noPUjet);
	  code = m_NNJvtEfficiencyTool->getEfficiencyScaleFactor(*jet,NNJvtsf);
	  if (code == CP::CorrectionCode::Ok){  
	    hist("h_NNJvtSF_eff")->Fill(NNJvtsf);
	  } else { 
	    ANA_MSG_DEBUG( "NNJvtEffTool - Cannot retrieve an SF for this jet \n");
	  }
	} else {
	  code = m_NNJvtEfficiencyTool->getInefficiencyScaleFactor(*jet,NNJvtsf);
	  if (code == CP::CorrectionCode::Ok){  
	    hist("h_NNJvtSF_ineff")->Fill(NNJvtsf);
	  } else { 
	    ANA_MSG_DEBUG( "NNJvtEffTool - Cannot retrieve an SF for this jet \n");
	  }
	}
	jetWeight*=NNJvtsf;
	Basej_N++;
	hist("h_basejetspt")->Fill((jet)->pt()/1e3,evtWeight*NNJvtsf);
      }
    }
    hist("h_NumberJets")->Fill(Allj_N,mcWeight);
    hist("h_NumberbaseJets")->Fill(Basej_N,mcWeight*jetWeight);
    hist("h_NumberJets_noPU")->Fill(noPUjets_N,mcWeight*jetWeight);
    
   
    /*****************************
    *           TRUTH LEVEL
    *****************************/
    ANA_MSG_DEBUG("Retrieving Truth Containers");
    ANA_MSG_DEBUG ("Truth Photons");
    Int_t AllgTruth_N = 0;
    for (auto gam: *gammaTruth){
      AllgTruth_N++;
      hist("h_TruthPhotonspt")->Fill((gam)->pt()/1e3,evtWeight);
      hist("h_TruthPhotonseta")->Fill((gam)->eta(),evtWeight);
      hist("h_TruthPhotonsphi")->Fill((gam)->phi(),evtWeight);
      unsigned int result = gam->auxdata<unsigned int>("Classification");
      if (((gam)->pt()/1e3>20.) && ((fabs(gam->eta())<1.37 || fabs(gam->eta()) > 1.52) && fabs(gam->eta()) < 2.37)){
	hist("h_TruthPhotonspt_baseline")->Fill(gam->pt()/1e3,evtWeight);
	if (MCTruthClassifier::isPrompt(result)){
	  hist("h_TruthPhotonspt_baseline_prompt")->Fill(gam->pt()/1e3,evtWeight);
	}
      }
    }
    hist("h_NumberTruthPhotons")->Fill(AllgTruth_N,evtWeight);

    ANA_MSG_DEBUG ("Electrons");
    Int_t AlleTruth_N = 0;
    for (auto ele: *elecTruth){
      AlleTruth_N++;
      hist("h_TruthElectronspt")->Fill((ele)->pt()/1e3,evtWeight);
      hist("h_TruthElectronseta")->Fill((ele)->eta(),evtWeight);
      hist("h_TruthElectronsphi")->Fill((ele)->phi(),evtWeight);
      //Equivalent to reco preselect selection
      unsigned int result = ele->auxdata<unsigned int>("Classification");
      if (((ele)->pt()/1e3>6.) && ((fabs(ele->eta())<1.37 || fabs(ele->eta()) > 1.52) && fabs(ele->eta()) < 2.47)){
	hist("h_TruthElectronspt_preselect")->Fill(ele->pt()/1e3,evtWeight);
	if (MCTruthClassifier::isPrompt(result)){
	  hist("h_TruthElectronspt_preselect_prompt")->Fill(ele->pt()/1e3,evtWeight);
	}
      }
    }
    hist("h_NumberTruthElectrons")->Fill(AlleTruth_N,evtWeight);
    
    ANA_MSG_DEBUG ("Truth Muons");
    Int_t AllmuTruth_N = 0;
    for (auto mu: *muTruth){
      AllmuTruth_N++;
      hist("h_TruthMuonspt")->Fill((mu)->pt()/1e3,evtWeight);
      hist("h_TruthMuonseta")->Fill((mu)->eta(),evtWeight);
      hist("h_TruthMuonsphi")->Fill((mu)->phi(),evtWeight);
      //Equivalent to reco preselect selection
      unsigned int result = mu->auxdata<unsigned int>("Classification");
      if (((mu)->pt()/1e3>6.) && (fabs(mu->eta())<2.4) ){
	hist("h_TruthMuonspt_preselect")->Fill(mu->pt()/1e3,evtWeight);
	if (singletrigger(m_Legacy,m_PhaseI,mu)){
	  hist("h_TruthMuonspt_preselect_trig")->Fill(mu->pt()/1e3,evtWeight);
	}
	if (MCTruthClassifier::isPrompt(result)){
	  hist("h_TruthMuonspt_preselect_prompt")->Fill(mu->pt()/1e3,evtWeight);
	}
      }
    }
    hist("h_NumberTruthMuons")->Fill(AllmuTruth_N,evtWeight);
    
    ANA_MSG_DEBUG ("Truth Taus");
    Int_t AlltjTruth_N = 0;
    for (auto tau: *tauTruth){
      AlltjTruth_N++;
      hist("h_TruthTauspt")->Fill((tau)->pt()/1e3,evtWeight);
      hist("h_TruthTauseta")->Fill((tau)->eta(),evtWeight);
      hist("h_TruthTausphi")->Fill((tau)->phi(),evtWeight);
    }
    hist("h_NumberTruthTaus")->Fill(AlltjTruth_N,evtWeight);
    
    ANA_MSG_DEBUG ("Truth Jets");
    Int_t AlljTruth_N = 0;
    for (auto jet: *jetTruth){
      AlljTruth_N++;
      hist("h_TruthJetspt")->Fill((jet)->pt()/1e3,evtWeight);
      hist("h_TruthJetseta")->Fill((jet)->eta(),evtWeight);
      hist("h_TruthJetsphi")->Fill((jet)->phi(),evtWeight);
      //Equivalent to reco baseline jet selection
      if( ((jet)->pt()/1e3 > 20.)  && fabs((jet)->eta())<4.5){
	hist("h_TruthJetspt_baseline")->Fill((jet)->pt()/1e3,evtWeight);
      }
    }
    hist("h_NumberTruthJets")->Fill(AlljTruth_N,evtWeight);

    ANA_MSG_DEBUG ("Truth Neutrinos");
    Int_t AllnuTruth_N = 0;
    for (auto nu: *nuTruth){
      AllnuTruth_N++;
      hist("h_TruthNeutrinospt")->Fill((nu)->pt()/1e3,evtWeight);
      hist("h_TruthNeutrinoseta")->Fill((nu)->eta(),evtWeight);
      hist("h_TruthNeutrinosphi")->Fill((nu)->phi(),evtWeight);
    }
    hist("h_NumberTruthNeutrinos")->Fill(AllnuTruth_N,evtWeight);
    
    for (const auto &p : *nuTruth){
      unsigned int result = p->auxdata<unsigned int>("Classification");
      if (MCTruthClassifier::isPrompt(result)){
	hist("h_TruthNeutrinospt_prompt")->Fill((p)->pt()/1e3,evtWeight);
      }
    }
    
    ANA_MSG_INFO ("Truth MET");
    //hist("h_TruthMETsumet")->Fill(metTruth->sumet()/1e3,evtWeight);
    Float_t mpx = 0.;
    Float_t mpy = 0.;
    for (auto met: *metTruth){
      //std::cout << "MET source and met: " << met->source() << " and " << met->met() << "\n";
      //metTruth contains 4 entries: NonInt (non interacting particles), Int (interacting, stable, within accept), IntOut (interacting, stable, no accept), IntMuons (fs muons, accept)
      //when getting met->source(), the values are: NonInt 135168; Int 139264; IntOut 147456; Intmuon 163840
      //ideally we would check for MissingETBase::Source::TruthType::NonInt, for now, keeping it like this:
      if (met->source()==135168){ // || met->source()==163840 met->source()==147456 met->source()==147456
	mpx+=(met)->mpx();
	mpy+=(met)->mpy();
      }
    }
    Float_t Tmet_exp = std::sqrt(pow(mpx,2) + pow(mpy,2));
    hist("h_TruthMETmet_exp")->Fill(Tmet_exp/1e3,evtWeight);
    
   
    
    
    /*****************************
    *           MET and METMaker
    *****************************/
    ANA_MSG_INFO("Retrieving MET Core and Association Map");
    const xAOD::MissingETContainer* METPFlowCon(0);
    ANA_CHECK(evtStore()->retrieve(METPFlowCon,"MET_Core_AnalysisMET"));
    const xAOD::MissingETAssociationMap* METPFlowMap(0);
    ANA_CHECK(evtStore()->retrieve(METPFlowMap,"METAssoc_AnalysisMET"));
    xAOD::MissingETAssociationHelper METPFlowHelper(METPFlowMap);
    
    ANA_MSG_DEBUG ("Creating new MET containers");
    xAOD::MissingETContainer* newMET = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* newMET_aux = new xAOD::MissingETAuxContainer();
    newMET->setStore(newMET_aux);
    ANA_CHECK(evtStore()->record(newMET,"newMET"));
    ANA_CHECK(evtStore()->record(newMET_aux, "newMET"));
    
    ANA_MSG_DEBUG ("Other objects for MET rebuilding");
    xAOD::MuonContainer* METmu = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* METmu_aux = new xAOD::MuonAuxContainer;
    METmu->setStore(METmu_aux);
    ANA_CHECK(evtStore()->record(METmu,"METMuons"));
    ANA_CHECK(evtStore()->record(METmu_aux, "METmuAux"));
    
    xAOD::PhotonContainer* METgam = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* METgam_aux = new xAOD::PhotonAuxContainer;
    METgam->setStore(METgam_aux);
    ANA_CHECK(evtStore()->record(METgam,"METPhotons"));
    ANA_CHECK(evtStore()->record(METgam_aux, "METgamAux"));
    
    xAOD::ElectronContainer* METele = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* METele_aux = new xAOD::ElectronAuxContainer;
    METele->setStore(METele_aux);
    ANA_CHECK(evtStore()->record(METele,"METElectrons"));
    ANA_CHECK(evtStore()->record(METele_aux, "METeleAux"));
    
    
    ANA_MSG_INFO("MET selection");
    //Elements fed into the METMaker: Everything that is calibrated but without having the OR tool applied
    ANA_MSG_DEBUG("MET photon selection");
    TLorentzVector gamP;
    for (auto gam: *gcopy.first){
      xAOD::Photon* METphoton = new xAOD::Photon;
      *METphoton = *gam;
      if(!xAOD::setOriginalObjectLink(*gam, *METphoton)){
	ANA_MSG_WARNING("MET photons - Could not set original object link!");
	delete [] METphoton;
	continue;
      }
      METgam->push_back(METphoton);
    }
    
    ANA_MSG_DEBUG("MET electron selection");
    TLorentzVector eleP;
    for (auto ele: *ecopy.first){
      xAOD::Electron* METelectron = new xAOD::Electron;
      *METelectron = *ele;
      if(!xAOD::setOriginalObjectLink(*ele, *METelectron)){
	ANA_MSG_WARNING("MET electrons - Could not set original object link!");
	delete [] METelectron;
	continue;
      }
      METele->push_back(METelectron);
    }
    
    ANA_MSG_DEBUG("Muon MET selection");
    TLorentzVector muP;
    for (auto mu: *mucopy.first){
      xAOD::Muon* METmuon = new xAOD::Muon;
      *METmuon = *mu;
      /*
      auto muTrk = (mu)->primaryTrackParticle();
      if (!muTrk) {
	ANA_MSG_WARNING("Muon with no track " << eventInfo->runNumber() << " " << eventInfo->eventNumber());
	continue;
      }
      */
      if(!xAOD::setOriginalObjectLink(*mu, *METmuon)){
	ANA_MSG_WARNING("MET muons - Could not set original object link!");
	delete [] METmuon;
	continue;
      }
      METmu->push_back(METmuon);
    }
    
    ANA_MSG_DEBUG("Tau and Jet MET - use original containers");
    
    ANA_MSG_DEBUG ("Rebuilding MET");
    METPFlowHelper.resetObjSelectionFlags();
    //MissingETBase::UsageHandler::Policy p = MissingETBase::UsageHandler::PhysicsObject;
    
    /*
    //Example
    //ANA_CHECK(m_METMaker->rebuildMET("RefPhoton", //name of metphotons in the met container
      xAOD::Type::Photon, //telling the rebuilder that this is photon met
      newMET, //filling this met container
      METgam, //using this selection of photons
      METHelper);} //using this association map- 
    */
   
    
    //ANA_MSG_DEBUG ("MET Systematics");
    //if (isMC){
    /*
    //get the set of recommended systematics
    const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
    const CP::SystematicSet&      recSysList  = registry.recommendedSystematics();
    */
    
    /*
    //const CP::SystematicSet
    ANA_MSG_DEBUG ("Trying to retrieve affecting systematics");
    //CP::SystematicSet affectingSystematics() const = CP::SystematicsTool::affectingSystematics();
    
    ANA_MSG_DEBUG ("Starting the loop for the different affecting systematics");
    //for (const auto& sysName : METSys.name()){
    
    for(CP::SystematicSet::const_iterator isys = METSys.begin();
      isys != METSys.end();
      ++isys) {// print the systematics on the first event
      // When only using the METSystematicsTool, this will be the list of recommended systematics for that tool
     
      // apply systematic soft term variation
      //ANA_MSG_DEBUG ("Systematic applied: MET_SoftTrk_Scale__1up");
      ANA_MSG_DEBUG ("Doing systematic: "<< (*isys).name());
    */  
      
      
      ANA_CHECK(m_METMaker->rebuildMET("RefGamma",xAOD::Type::Photon,newMET,METgam,METPFlowHelper));
      ANA_CHECK(m_METMaker->rebuildMET("RefEle",xAOD::Type::Electron,newMET,METele,METPFlowHelper)); 
      ANA_CHECK(m_METMaker->rebuildMET("Muons",xAOD::Type::Muon,newMET,METmu,METPFlowHelper));
      //ANA_CHECK(m_METMaker->rebuildMET("RefTau",xAOD::Type::Tau,newMET,tjcopy.first,METPFlowHelper));
      ANA_CHECK(m_METMaker->rebuildJetMET("RefJet","SoftClus", "PVSoftTrk",newMET,jPflowcopy.first,METPFlowCon,METPFlowHelper, true));
    
      
      /*
      //const CP::SystematicSet iSysSet( (*isys).name() );
      //ANA_CHECK(m_METSystTool->applySystematicVariation((*isys).name()));
      xAOD::MissingET *softClusMET = (*newMET)["SoftClus"];
      if(softClusMET){
	ANA_CHECK(m_METSystTool->applyCorrection(*softClusMET, METHelper));
      } else {
	ANA_MSG_ERROR("No SoftClus term");
      }
      xAOD::MissingET *softTrkMET = (*newMET)["PVSoftTrk"];
      if(softTrkMET){
	ANA_CHECK(m_METSystTool->applyCorrection(*softTrkMET, METHelper));
      } else {
	ANA_MSG_ERROR("No SoftTrk term");
      }
      */
      
      ANA_MSG_INFO ("Building MET");
      ANA_CHECK(met::buildMETSum("FinalTrk", newMET, (*newMET)["PVSoftTrk"]->source())); 
      ANA_CHECK(met::buildMETSum("FinalClus", newMET, (*newMET)["SoftClus"]->source()));
      
      //Final reco MET
      Float_t finalMET = (*newMET)["FinalTrk"]->met();
      Float_t finalMETmpx = (*newMET)["FinalTrk"]->mpx();
      Float_t finalMETmpy = (*newMET)["FinalTrk"]->mpy();
      Float_t finalMETphi = (*newMET)["FinalTrk"]->phi();
      
      hist("h_RecoMET_sumet")->Fill((*newMET)["FinalTrk"]->sumet()/1e3,evtWeight);
      hist("h_RecoMET_met")->Fill(finalMET/1e3,evtWeight);
      hist("h_RecoMET_phi")->Fill(finalMETphi,evtWeight);
      
      //ANA_CHECK(evtStore()->record( newMET,    "FinalMETContainer")); // + (*isys).name()      
      //ANA_CHECK(evtStore()->record( newMET_aux, "FinalMETContainer" ));//+ (*isys).name() + "Aux"

      //Track Soft term
      hist("h_AllMETpt_PVSoftTrk")->Fill(((*newMET)["PVSoftTrk"]->met())/1e3,evtWeight);
      hist("h_AllMETphi_PVSoftTrk")->Fill(((*newMET)["PVSoftTrk"]->phi()),evtWeight);
            
      
      //Plots for Understanding MET - Individual components
      ANA_MSG_DEBUG("Photon MET term: " << (*newMET)["RefGamma"]->met()/1000);
      ANA_MSG_DEBUG("Electron MET term: " << (*newMET)["RefEle"]->met()/1000);
      ANA_MSG_DEBUG("Muon MET term: " << (*newMET)["Muons"]->met()/1000);
      //ANA_MSG_DEBUG("Tau MET term: " << (*newMET)["RefTau"]->met()/1000);
      ANA_MSG_DEBUG("Jet MET term: " << (*newMET)["RefJet"]->met()/1000);
      ANA_MSG_DEBUG("PVSoftTrk MET term: " << (*newMET)["PVSoftTrk"]->met()/1000);
      ANA_MSG_DEBUG("SoftClus MET term: " << (*newMET)["SoftClus"]->met()/1000);
      hist("h_eMETpt")->Fill((*newMET)["RefEle"]->met()/1000,evtWeight);
      hist("h_gMETpt")->Fill((*newMET)["RefGamma"]->met()/1000,evtWeight);
      hist("h_muMETpt")->Fill((*newMET)["Muons"]->met()/1000,evtWeight);
      //hist("h_tMETpt")->Fill((*newMET)["RefTau"]->met()/1000,evtWeight);
      hist("h_jMETpt")->Fill((*newMET)["RefJet"]->met()/1000,evtWeight);
      
      //soft Cluster term - not used
      hist("h_AllMETpt_SoftClus")->Fill(((*newMET)["SoftClus"]->met())/1000,evtWeight);
      hist("h_AllMETphi_SoftClus")->Fill(((*newMET)["SoftClus"]->phi()),evtWeight);
      
      
      //How MET is calculated
      Float_t expMETsumx = (*newMET)["RefEle"]->mpx()+(*newMET)["RefGamma"]->mpx()+(*newMET)["Muons"]->mpx()+(*newMET)["RefJet"]->mpx(); // +(*newMET)["RefTau"]->mpx(); 
      Float_t expMETsumy= (*newMET)["RefEle"]->mpy()+(*newMET)["RefGamma"]->mpy()+(*newMET)["Muons"]->mpy()+(*newMET)["RefJet"]->mpy(); // + (*newMET)["RefTau"]->mpy();
      Float_t expMETsumet = std::sqrt(pow(expMETsumx,2) + pow(expMETsumy,2));
      hist("h_expMETsum")->Fill(expMETsumet/1000,evtWeight);
      
      
      hist("h_addPVTrknClus")->Fill((*newMET)["FinalClus"]->met()/1000,evtWeight);
      hist("h_addPVTrknClus")->Fill((*newMET)["FinalTrk"]->met()/1000,evtWeight);
               
  
      ANA_MSG_INFO("Explicit sum MET terms (x,y decomposition): " << expMETsumet/1000);
      ANA_MSG_INFO("Final reco MET (MET Maker): " << finalMET/1e3);
      
     //Reco vs Truth comparison for bins of truth MET
     Float_t relMETdiff = (Tmet_exp - finalMET)/Tmet_exp;
     if (Tmet_exp/1e3< 20.){
       hist("h_relMETdiff_0_20_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3 > 20.) && (Tmet_exp/1e3< 40.)){
      hist("h_relMETdiff_20_40_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3 > 40.) && (Tmet_exp/1e3< 60.)){
       hist("h_relMETdiff_40_60_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3> 60.) && (Tmet_exp/1e3< 80.)){
       hist("h_relMETdiff_60_80_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3> 80.) && (Tmet_exp/1e3< 120.)){
       hist("h_relMETdiff_80_120_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3> 120.) && (Tmet_exp/1e3< 160.)) {
       hist("h_relMETdiff_120_160_truth")->Fill(relMETdiff, evtWeight);
     } else if ((Tmet_exp/1e3> 160.) && (Tmet_exp/1e3< 200.)){
       hist("h_relMETdiff_160_200_truth")->Fill(relMETdiff, evtWeight);
     } 
      
     
    /*****************************
    *      OVERLAP REMOVAL TOOL
    *****************************/
    ANA_MSG_INFO ("Getting ready to remove overlaps");
    //We create containers of the other types of particles (as empty dynamical objects) bc we need inputs of the OR tool
    std::vector<boost::any> otherContainers;
    xAOD::JetContainer* fatjets_empty = new xAOD::JetContainer;
    xAOD::TauJetContainer* taus_empty = new xAOD::TauJetContainer;
    otherContainers.push_back(fatjets_empty);
    otherContainers.push_back(taus_empty);
    //tjcopy.first
    ANA_CHECK( m_orTool->removeOverlaps(preselectelectrons, preselectmuons, noPUjets, taus_empty, baselinephotons, fatjets_empty) );
    
    
    //New containers without the overlaps
    xAOD::PhotonContainer* onlygam = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* onlygam_aux = new xAOD::PhotonAuxContainer; 
    onlygam->setStore(onlygam_aux);
    ANA_CHECK(evtStore()->record(onlygam,"SelectedPhotons_noOL"));
    ANA_CHECK(evtStore()->record(onlygam_aux, "SelectedPhotons_noOLAux"));
    
    xAOD::ElectronContainer* onlyele = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* onlyele_aux = new xAOD::ElectronAuxContainer; 
    onlyele->setStore(onlyele_aux);
    ANA_CHECK(evtStore()->record(onlyele,"SelectedElectrons_noOL"));
    ANA_CHECK(evtStore()->record(onlyele_aux, "SelectedElectrons_noOLAux"));
    
    xAOD::JetContainer* onlyjet = new xAOD::JetContainer;
    xAOD::JetAuxContainer* onlyjet_aux = new xAOD::JetAuxContainer; 
    onlyjet->setStore(onlyjet_aux);
    ANA_CHECK(evtStore()->record(onlyjet,"SelectedJets_noOL"));
    ANA_CHECK(evtStore()->record(onlyjet_aux, "SelectedJets_noOLAux"));
    
    xAOD::MuonContainer* onlymu = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* onlymu_aux = new xAOD::MuonAuxContainer; 
    onlymu->setStore(onlymu_aux);
    ANA_CHECK(evtStore()->record(onlymu,"SelectedMuons_noOL"));
    ANA_CHECK(evtStore()->record(onlymu_aux, "SelectedMuons_noOLAux"));
    
    xAOD::TauJetContainer* onlytau = new xAOD::TauJetContainer;
    xAOD::TauJetAuxContainer* onlytau_aux = new xAOD::TauJetAuxContainer; 
    onlytau->setStore(onlytau_aux);
    ANA_CHECK(evtStore()->record(onlytau,"SelectedTau_noOL"));
    ANA_CHECK(evtStore()->record(onlytau_aux, "SelectedTau_noOLAux"));
    
    
    ANA_MSG_INFO ("After calling tool, accessing overlaps flags...");
    for (xAOD::PhotonContainer::const_iterator itr=baselinephotons->begin(); itr != baselinephotons->end(); ++itr) {
      if(!overlapAcc(**itr)){
	xAOD::Photon* noOL_gam = new xAOD::Photon();
	*noOL_gam = (**itr);
	onlygam->push_back(noOL_gam);
      } else if (overlapAcc(**itr)){
	  ANA_MSG_DEBUG ("Overlap found in Photon Container");
      }
    }
    
    for (xAOD::ElectronContainer::const_iterator itr=preselectelectrons->begin(); itr != preselectelectrons->end(); ++itr) {
      if(!overlapAcc(**itr)){
	xAOD::Electron* noOL_ele = new xAOD::Electron();
	*noOL_ele = (**itr);
	onlyele->push_back(noOL_ele);
      } else if (overlapAcc(**itr)){
	  ANA_MSG_DEBUG ("Overlap found in Electron Container");
      } 
    }
    
    for (xAOD::MuonContainer::const_iterator itr=preselectmuons->begin(); itr != preselectmuons->end(); ++itr) {
      if(!overlapAcc(**itr)){
	xAOD::Muon* noOL_mu = new xAOD::Muon();
	*noOL_mu = (**itr);
	onlymu->push_back(noOL_mu);
      } else if (overlapAcc(**itr)){
	  ANA_MSG_DEBUG ("Overlap found in Muon Container");
      } 
    }
    
    /*
    for (xAOD::TauJetContainer::const_iterator itr=tjcopy.first->begin(); itr != tjcopy.first->end(); ++itr) {
      if(!overlapAcc(**itr)){
	xAOD::TauJet* noOL_tau = new xAOD::TauJet();
	*noOL_tau = (**itr);
	onlytau->push_back(noOL_tau);
      } else if (overlapAcc(**itr)){
	  ANA_MSG_DEBUG ("Overlap found in TauJet Container");
      } 
    }
    */
    for (xAOD::JetContainer::const_iterator itr=noPUjets->begin(); itr != noPUjets->end(); ++itr) {
      if(!overlapAcc(**itr)){
	xAOD::Jet* noOL_jet = new xAOD::Jet();
	*noOL_jet = (**itr);
	onlyjet->push_back(noOL_jet);
      } else if (overlapAcc(**itr)){
	  ANA_MSG_DEBUG ("Overlap found in Jet Container");
      }
    }
    
    
    //Control plots for Overlaps
    for(xAOD::JetContainer::const_iterator jet=noPUjets->begin(); jet != noPUjets->end(); ++jet) {
      for (xAOD::ElectronContainer::const_iterator ele=preselectelectrons->begin(); ele != preselectelectrons->end(); ++ele) {
	hist("h_DRej")->Fill(DR((*ele)->caloCluster()->etaBE(2), (*ele)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
      }
      for (xAOD::PhotonContainer::const_iterator gam=baselinephotons->begin(); gam != baselinephotons->end(); ++gam) {
	hist("h_DRgj")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=preselectelectrons->begin(); ele != preselectelectrons->end(); ++ele) {
	  hist("h_DRge")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	for (xAOD::MuonContainer::const_iterator mu=preselectmuons->begin(); mu !=preselectmuons->end(); ++mu) {
	  hist("h_DRgmu")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*mu)->eta(), (*mu)->phi()),evtWeight);
	}
      }
      for (xAOD::MuonContainer::const_iterator mu=preselectmuons->begin(); mu != preselectmuons->end(); ++mu) {
	hist("h_DRmuj")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=preselectelectrons->begin(); ele != preselectelectrons->end(); ++ele) {
	  hist("h_DRmue")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	/*
	for (xAOD::TauJetContainer::const_iterator tau=tjcopy.first->begin(); tau != tjcopy.first->end(); ++tau) {
	  hist("h_DRmut")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*mu)->eta(), (*mu)->phi()),evtWeight);
	}
	*/
      }
      /*
      for (xAOD::TauJetContainer::const_iterator tau=tjcopy.first->begin(); tau != tjcopy.first->end(); ++tau) {
	hist("h_DRtj")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=preselectelectrons->begin(); ele != preselectelectrons->end(); ++ele) {
	  hist("h_DRet")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	for (xAOD::PhotonContainer::const_iterator gam=baselinephotons->begin(); gam != baselinephotons->end(); ++gam) {
	  hist("h_DRgt")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*gam)->caloCluster()->etaBE(2), (*gam)->phi()),evtWeight);
	}
      }
      */
    }
    
    //After OverlapRemoval
    for(xAOD::JetContainer::const_iterator jet=onlyjet->begin(); jet != onlyjet->end(); ++jet) {
      for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	hist("h_DRej_noOL")->Fill(DR((*ele)->caloCluster()->etaBE(2), (*ele)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
      }
      for (xAOD::PhotonContainer::const_iterator gam=onlygam->begin(); gam != onlygam->end(); ++gam) {
	hist("h_DRgj_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	    hist("h_DRge_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	for (xAOD::MuonContainer::const_iterator mu=onlymu->begin(); mu != onlymu->end(); ++mu) {
	  hist("h_DRgmu_noOL")->Fill(DR((*gam)->caloCluster()->etaBE(2), (*gam)->phi(), (*mu)->eta(), (*mu)->phi()),evtWeight);
	}
      }
      for (xAOD::MuonContainer::const_iterator mu=onlymu->begin(); mu != onlymu->end(); ++mu) {
	hist("h_DRmuj_noOL")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	  hist("h_DRmue_noOL")->Fill(DR((*mu)->eta(), (*mu)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	/*
	for (xAOD::TauJetContainer::const_iterator tau=onlytau->begin(); tau != onlytau->end(); ++tau) {
	  hist("h_DRmut_noOL")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*mu)->eta(), (*mu)->phi()),evtWeight);
	}
	*/
	
      }
      /*
      for (xAOD::TauJetContainer::const_iterator tau=onlytau->begin(); tau != onlytau->end(); ++tau) {
	hist("h_DRtj_noOL")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*jet)->eta(), (*jet)->phi()),evtWeight);
	for (xAOD::ElectronContainer::const_iterator ele=onlyele->begin(); ele != onlyele->end(); ++ele) {
	  hist("h_DRet_noOL")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*ele)->caloCluster()->etaBE(2), (*ele)->phi()),evtWeight);
	}
	for (xAOD::PhotonContainer::const_iterator gam=onlygam->begin(); gam != onlygam->end(); ++gam) {
	  hist("h_DRgt_noOL")->Fill(DR((*tau)->eta(), (*tau)->phi(), (*gam)->caloCluster()->etaBE(2), (*gam)->phi()),evtWeight);
	}
      }
      */
    }
    
    
    /*****************************
    * 
    *       OBJECT SELECTION - Reco
    * 
    *****************************/
    ANA_MSG_INFO("Baseline electron and muon selection"); 
    for (auto ele: *onlyele) { //Preselect + OR
      double esf_reco= 1.;
      double esf_ID= 1.;
      double esf_iso= 1.;
      double esf_trig= 1.;
      ANA_CHECK(m_eEffSFTool_reco->getEfficiencyScaleFactor(*ele,esf_reco));
      ANA_CHECK(m_eEffSFTool_ID->getEfficiencyScaleFactor(*ele,esf_ID));
      //ANA_CHECK(m_eEffSFTool_iso->getEfficiencyScaleFactor(*ele,esf_iso));
      /*
      if (m_r3MatchingTool->match(*ele, "HLT_e26_lhtight_ivarloose_L1EM22VHI",0.1,false)){
	ANA_CHECK(m_eEffSFTool_trig->getEfficiencyScaleFactor(*ele,esf_trig));
      }
      */
      hist("h_onlyelept")->Fill((ele)->pt()/1e3,evtWeight*esf_reco*esf_ID*esf_iso*esf_trig); 
      hist("h_onlyelept_noTrigSF")->Fill((ele)->pt()/1e3,evtWeight*esf_reco*esf_ID*esf_iso); 
      if (m_ElectronChannel){
        hist("h_eCutflow_selection")->Fill(7.5,evtWeight); 
        ANA_MSG_DEBUG("Checking electron isolation");
        if (m_isolationSelectionTool->accept(*ele)){
          Float_t topoetcone20_eiso;
          Float_t ptvarcone30_eiso;
          (ele)->isolation(ptvarcone30_eiso,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000);
          (ele)->isolation(topoetcone20_eiso,xAOD::Iso::topoetcone20);
          hist("h_ptvarcone30_eiso")->Fill(ptvarcone30_eiso/((ele)->pt())-0.06,evtWeight);
          hist("h_topoetcone20_eiso")->Fill(topoetcone20_eiso/((ele)->pt())-0.06,evtWeight);
        }
        if ((ele)->passSelection("DFCommonElectronsLHMedium")){
          hist("h_eCutflow_selection")->Fill(8.5,evtWeight);
          if ((ele)->pt()/1e3>25.){
            hist("h_eCutflow_selection")->Fill(9.5,evtWeight);
          }
        }
      }
      if ((ele)->passSelection("DFCommonElectronsLHMedium") && (ele)->pt()/1e3>25.){
        BaseEle_N++;  
        xAOD::Electron* baselineelectron = new xAOD::Electron();
        *baselineelectron = (*ele);
        baselineelectrons->push_back(baselineelectron);
        hist("h_baseelectronspt")->Fill((ele)->pt()/1e3,evtWeight*esf_reco*esf_ID*esf_iso*esf_trig);
        baseesf*=esf_reco*esf_ID*esf_iso*esf_trig;
      }
    }
    hist("h_NumberbaseElectrons")->Fill(BaseEle_N,evtWeight*baseesf);
    
    
    
    musf_trig=1.; //this is applied to the container
    if (onlymu->size()>0){
      ANA_CHECK(m_muEffSFTool_trig->getTriggerScaleFactor(*onlymu,musf_trig,"HLT_mu24_ivarmedium_L1MU14FCH"));
      hist("h_muSFtrig")->Fill(musf_trig);
    }
   
    for (auto mu: *onlymu) { //Preselect + OR
      
      float musf_TTVA= 1.;
      float musf_ID= 1.;
      float musf_iso= 1.;
      ANA_CHECK(m_muEffSFTool_TTVA->getEfficiencyScaleFactor(*mu,musf_TTVA,eventInfo));
      ANA_CHECK(m_muEffSFTool_ID->getEfficiencyScaleFactor(*mu,musf_ID,eventInfo));
      ANA_CHECK(m_muEffSFTool_iso->getEfficiencyScaleFactor(*mu,musf_iso,eventInfo));
      hist("h_onlymupt")->Fill((mu)->pt()/1e3,evtWeight*musf_TTVA*musf_ID*musf_iso*musf_trig);
      hist("h_onlymupt_noTrigSF")->Fill((mu)->pt()/1e3,evtWeight*musf_TTVA*musf_ID*musf_iso);
      
      
      if (m_MuonChannel){
        hist("h_muCutflow_selection")->Fill(4.5,evtWeight); 
        ANA_MSG_DEBUG("Checking muon isolation");
        if (m_isolationSelectionTool->accept(*mu)){
          Float_t topoetcone20_muiso;
          Float_t ptvarcone30_muiso;
          (mu)->isolation(ptvarcone30_muiso,xAOD::Iso::ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt1000);
          (mu)->isolation(topoetcone20_muiso,xAOD::Iso::topoetcone20);
          hist("h_ptvarcone30_muiso")->Fill(ptvarcone30_muiso/((mu)->pt()) - 0.04,evtWeight);
          hist("h_topoetcone20_muiso")->Fill(topoetcone20_muiso/((mu)->pt()) -0.15,evtWeight);
        }
        if ((mu)->pt()/1e3 > 25.){ 
          hist("h_muCutflow_selection")->Fill(5.5,evtWeight); 
        }
      }
      if ((mu)->pt()/1e3 > 25.){
        BaseMu_N++;
        xAOD::Muon* baselinemuon = new xAOD::Muon();
        *baselinemuon = (*mu);
        baselinemuons->push_back(baselinemuon);
        hist("h_basemuonspt")->Fill((mu)->pt()/1e3,evtWeight*musf_TTVA*musf_ID*musf_iso*musf_trig);
        basemusf*=musf_TTVA*musf_ID*musf_iso*musf_trig;
      }
    }
    hist("h_NumberbaseMuons")->Fill(BaseMu_N,evtWeight*basemusf);
    
    float NNJvtsf=1.;
    CP::CorrectionCode code;
    for (auto jet: *onlyjet){ //Baseline + noPU + OR
      code = m_NNJvtEfficiencyTool->getEfficiencyScaleFactor(*jet,NNJvtsf);
      if (code == CP::CorrectionCode::Ok){  
	  hist("h_NNJvtSF_eff")->Fill(NNJvtsf);
      } else { 
	  ANA_MSG_DEBUG( "NNJvtEffTool - Cannot retrieve an SF for this jet \n");
      }
      hist("h_onlyjetpt")->Fill(jet->pt()/1e3,mcWeight*NNJvtsf*(-1)); 
    }
    
    const xAOD::IParticle* leadGamBaseline(0);
    const xAOD::IParticle* subleadGamBaseline(0);
    const xAOD::IParticle* leadLepBaseline(0);
    bool Wyyismatched;
   
    
    ANA_MSG_INFO("Start signal photons selection");
    Int_t Goodg_N = 0;
    double goodgamWeight = 1.;
    xAOD::PhotonContainer* signalphotons = new xAOD::PhotonContainer;
    xAOD::PhotonAuxContainer* signalphotons_aux = new xAOD::PhotonAuxContainer;
    signalphotons->setStore(signalphotons_aux);
    ANA_CHECK(evtStore()->record(signalphotons,"GoodPhotons"));
    ANA_CHECK(evtStore()->record(signalphotons_aux, "GoodPhotonsAux"));
    for (auto gam: *onlygam) { //baseline photons after OR
      double gsf_reco= 1.;
      double gsf_ID= 1.;
      double gsf_iso= 1.;
      double gsf_trig= 1.;
      ANA_CHECK(m_gEffSFTool_reco->getEfficiencyScaleFactor(*gam,gsf_reco));
      ANA_CHECK(m_gEffSFTool_ID->getEfficiencyScaleFactor(*gam,gsf_ID));
      //ANA_CHECK(m_gEffSFTool_iso->getEfficiencyScaleFactor(*gam,gsf_iso));
      //ANA_CHECK(m_gEffSFTool_trig->getEfficiencyScaleFactor(*gam,gsf_trig));
      hist("h_onlygampt")->Fill((gam)->pt()/1e3,evtWeight*gsf_reco*gsf_ID*gsf_iso*gsf_trig);
      hist("h_gCutflow_selection")->Fill(5.5,evtWeight);  
      //ISOLATION 
      if ( m_isolationSelectionTool->accept(*gam)){ 
	Float_t topoetcone40_giso;
	Float_t ptcone20_giso;
	(gam)->isolation(topoetcone40_giso,xAOD::Iso::topoetcone40);
	(gam)->isolation(ptcone20_giso,xAOD::Iso::ptcone20);
	hist("h_topoetcone40_giso")->Fill(topoetcone40_giso/1e3 -0.022*(gam)->pt()/1e3,evtWeight);
	hist("h_ptcone20_giso")->Fill(ptcone20_giso/1e3 - 0.05*(gam)->pt()/1e3,evtWeight);
      }
      if ((gam)->passSelection("DFCommonPhotonsIsEMTight")){  //ID
	hist("h_gCutflow_selection")->Fill(6.5,evtWeight);
	
	if (m_isolationSelectionTool->accept(*gam)){
	  hist("h_gCutflow_selection")->Fill(7.5,evtWeight);
	  double gsf_reco= 1.;
	  double gsf_ID= 1.;
	  double gsf_iso= 1.;
	  double gsf_trig= 1.;
	  ANA_CHECK(m_gEffSFTool_reco->getEfficiencyScaleFactor(*gam,gsf_reco));
	  ANA_CHECK(m_gEffSFTool_ID->getEfficiencyScaleFactor(*gam,gsf_ID));
	  ANA_CHECK(m_gEffSFTool_iso->getEfficiencyScaleFactor(*gam,gsf_iso));
	  //ANA_CHECK(m_gEffSFTool_trig->getEfficiencyScaleFactor(*gam,gsf_trig));
	  xAOD::Photon* signalphoton = new xAOD::Photon();
	  *signalphoton = (*gam);
	  signalphotons->push_back(signalphoton);
	  hist("h_goodphotonspt")->Fill((gam)->pt()/1e3,evtWeight*gsf_reco*gsf_ID*gsf_iso*gsf_trig); //these are particular for this photon
	  hist("h_gCutflow_selection")->Fill(8.5,evtWeight*gsf_reco*gsf_ID*gsf_iso*gsf_trig);
	  Goodg_N++;
	  goodgamWeight*=gsf_reco*gsf_ID*gsf_iso*gsf_trig;
	}
      }
    }
    hist("h_NumbergoodPhotons")->Fill(Goodg_N,goodgamWeight*evtWeight);
    ANA_MSG_DEBUG("End photon selection");
    
    ANA_MSG_INFO("Start signal leptons selection");
    Int_t GoodEle_N = 0;
    double goodeleWeight = 1.;
    xAOD::ElectronContainer* signalelectrons = new xAOD::ElectronContainer;
    xAOD::ElectronAuxContainer* signalelectrons_aux = new xAOD::ElectronAuxContainer;
    signalelectrons->setStore(signalelectrons_aux);
    ANA_CHECK(evtStore()->record(signalelectrons,"SignalElectrons"));
    ANA_CHECK(evtStore()->record(signalelectrons_aux, "SignalElectronsAux"));
    Int_t GoodMu_N = 0;
    double goodmuWeight = 1.;
    xAOD::MuonContainer* signalmuons = new xAOD::MuonContainer;
    xAOD::MuonAuxContainer* signalmuons_aux = new xAOD::MuonAuxContainer;
    signalmuons->setStore(signalmuons_aux);
    ANA_CHECK(evtStore()->record(signalmuons,"SignalMuons"));
    ANA_CHECK(evtStore()->record(signalmuons_aux, "SignalMuonsAux"));
    
    if (m_ElectronChannel){
      for (auto ele: *baselineelectrons){
        std::vector<bool> TTVA = TTVAele(ele,pVtx,eventInfo);
        if (m_isolationSelectionTool->accept(*ele)){
          hist("h_eCutflow_selection")->Fill(10.5,evtWeight);
          if (TTVA[1]){
            hist("h_eCutflow_selection")->Fill(11.5,evtWeight);
            double esf_reco= 1.;
            double esf_ID= 1.;
            double esf_iso= 1.;
            double esf_trig= 1.;
            ANA_CHECK(m_eEffSFTool_reco->getEfficiencyScaleFactor(*ele,esf_reco));
            ANA_CHECK(m_eEffSFTool_ID->getEfficiencyScaleFactor(*ele,esf_ID));
            ANA_CHECK(m_eEffSFTool_iso->getEfficiencyScaleFactor(*ele,esf_iso));
            /*
            if (m_r3MatchingTool->match(*ele, "HLT_e26_lhtight_ivarloose_L1EM22VHI",0.1,false)){
              ANA_CHECK(m_eEffSFTool_trig->getEfficiencyScaleFactor(*ele,esf_trig));
            }
            */
            xAOD::Electron* signalelectron = new xAOD::Electron();
            *signalelectron = (*ele);
            signalelectrons->push_back(signalelectron);
            hist("h_goodelectronspt")->Fill((ele)->pt()/1e3,evtWeight*esf_reco*esf_ID*esf_iso*esf_trig);
            hist("h_eCutflow_selection")->Fill(12.5,evtWeight*esf_reco*esf_ID*esf_iso*esf_trig);
            GoodEle_N++;
            goodeleWeight*=esf_reco*esf_ID*esf_iso*esf_trig;
          }
        }
      }
      hist("h_NumbergoodElectrons")->Fill(GoodEle_N,evtWeight*goodeleWeight);
    }
    if (m_MuonChannel){
      musf_trig=1.; //this is applied to the container
      //ANA_CHECK(m_muEffSFTool_trig->getTriggerScaleFactor(*baselinemuons,musf_trig,"HLT_mu24_ivarmedium_L1MU14FCH"));
      
      for (auto mu: *baselinemuons){
        std::vector<bool> TTVA = TTVAmuon(mu,pVtx,eventInfo);
        if (TTVA[1]){
          hist("h_muCutflow_selection")->Fill(6.5,evtWeight); 
          if (m_isolationSelectionTool->accept(*mu)){ 
            hist("h_muCutflow_selection")->Fill(7.5,evtWeight);
            float musf_TTVA= 1.;
            float musf_ID= 1.;
            float musf_iso= 1.;
            ANA_CHECK(m_muEffSFTool_TTVA->getEfficiencyScaleFactor(*mu,musf_TTVA,eventInfo));
            ANA_CHECK(m_muEffSFTool_ID->getEfficiencyScaleFactor(*mu,musf_ID,eventInfo));
            ANA_CHECK(m_muEffSFTool_iso->getEfficiencyScaleFactor(*mu,musf_iso,eventInfo));
            xAOD::Muon* signalmuon = new xAOD::Muon();
            *signalmuon = (*mu);
            signalmuons->push_back(signalmuon);
            hist("h_goodmuonspt")->Fill((mu)->pt()/1e3,evtWeight*musf_TTVA*musf_ID*musf_iso*musf_trig);
            hist("h_muCutflow_selection")->Fill(8.5,evtWeight*musf_TTVA*musf_ID*musf_iso*musf_trig);
	    hist("h_muSFtrig_pt")->Fill((mu)->pt()/1e3,musf_trig);
            GoodMu_N++;
            goodmuWeight*=musf_TTVA*musf_ID*musf_iso; //musf_trig;
          }
        }
      }
      hist("h_NumbergoodMuons")->Fill(GoodMu_N,evtWeight*goodmuWeight);
    }
    
    /*Summary of object selection so far:
     * onlygam = baseline photons after OR
     * onlyele = preselect electrons after OR
     * onlymu = preselect muons after OR
     * onlyjet = baseline non PU jets after OR 
     * noPUjets = jets that dont come from pileup
    */
    double WyyTrigSF = 1.;
    if (m_ElectronChannel){
      ANA_MSG_DEBUG("At least 1 baseline lepton and 2 baseline photons");
      if (onlygam->size()>1 && baselineelectrons->size()>0){ //onlygam = baseline photons after OR
	      hist("h_Cutflow_selection_lyy")->Fill(2.5,evtWeight);
      } else {
	      return StatusCode::SUCCESS;
      } 
      
      ANA_MSG_INFO("Trigger Matching - Wyy trigger chain"); 
      leadGamBaseline = (*onlygam)[0];
      subleadGamBaseline = (*onlygam)[1];
      leadLepBaseline = (*baselineelectrons)[0];
      std::vector<const xAOD::IParticle*> Vec{leadGamBaseline,subleadGamBaseline,leadLepBaseline};
      //HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH
      
      Wyyismatched = WyyTrigger (m_Legacy, m_PhaseI, Vec);
      //Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH",0.1,false);
      //code = CP::CorrectionCode::Ok;
      if (Wyyismatched){
        //ANA_CHECK(m_TrigGlobEffTool->getEfficiencyScaleFactor(Vec, WyyTrigSF));
	evtWeight*=WyyTrigSF;
	hist("h_Cutflow_selection_lyy")->Fill(3.5,evtWeight);
      } else {
	      return StatusCode::SUCCESS;
      }
      
      
      ANA_MSG_DEBUG("No baseline opposite-flavour lepton");
      if (baselinemuons->size() > 0){
	return StatusCode::SUCCESS;
      } else {
	hist("h_Cutflow_selection_lyy")->Fill(4.5,evtWeight);
      }
      
      ANA_MSG_DEBUG("No second preselect same-flavour lepton");
      if (onlyele->size() > 1){ //
	return StatusCode::SUCCESS;
      } else {
	hist("h_Cutflow_selection_lyy")->Fill(5.5,evtWeight);
      }
      
    } else if (m_MuonChannel){
       ANA_MSG_DEBUG("At least 1 baseline lepton and 2 baseline photons");
      if (onlygam->size()>1 && baselinemuons->size()>0){ //onlygam = baseline photons after OR
	hist("h_Cutflow_selection_lyy")->Fill(2.5,evtWeight);
      } else {
	return StatusCode::SUCCESS;
      } 
      
      ANA_MSG_INFO("Trigger Matching - Wyy trigger chain"); 
      leadGamBaseline = (*onlygam)[0];
      subleadGamBaseline = (*onlygam)[1];
      leadLepBaseline = (*baselinemuons)[0];
      std::vector<const xAOD::IParticle*> Vec{leadGamBaseline,subleadGamBaseline,leadLepBaseline};
      Wyyismatched = WyyTrigger (m_Legacy, m_PhaseI, Vec);
      //Wyyismatched = m_r3MatchingTool->match(Vec, "HLT_2g10_loose_mu20_L1MU14FCH",0.1,false);
      
      
    
      if (Wyyismatched){
	//ANA_CHECK(m_TrigGlobEffTool->getEfficiencyScaleFactor(Vec, WyyTrigSF));
	evtWeight*=WyyTrigSF;
	hist("h_Cutflow_selection_lyy")->Fill(3.5,evtWeight);
      } else {
	return StatusCode::SUCCESS;
      }
      
      ANA_MSG_DEBUG("No baseline opposite-flavour lepton");
      if (baselineelectrons->size() > 0){
	return StatusCode::SUCCESS;
      } else {
	hist("h_Cutflow_selection_lyy")->Fill(4.5,evtWeight);
      }
      
      ANA_MSG_DEBUG("No second preselect same-flavour lepton");
      if (onlymu->size() > 1){
	return StatusCode::SUCCESS;
      } else {
	hist("h_Cutflow_selection_lyy")->Fill(5.5,evtWeight);
      }
    }
    
    if ((finalMET/1e3)>25.){
      hist("h_Cutflow_selection_lyy")->Fill(6.5,evtWeight);
    } else {
      return StatusCode::SUCCESS;
    }
   
   
    
    const xAOD::IParticle* leadingGam(0);
    const xAOD::IParticle* subleadingGam(0);
    const xAOD::IParticle* leadingLep(0);
    double leadgamWeight=1.;
    double subgamWeight=1.;
    double lepWeight=1.;
    TLorentzVector lep_p4;
    float Wcharge = 0.;
    ANA_MSG_DEBUG("At least 2 signal leptons");
    if (m_ElectronChannel){
      if (signalelectrons->size()>0){
        double esf_reco= 1.;
        double esf_ID= 1.;
        double esf_iso= 1.;
        double esf_trig= 1.;
        ANA_CHECK(m_eEffSFTool_reco->getEfficiencyScaleFactor(*(*signalelectrons)[0],esf_reco));
        ANA_CHECK(m_eEffSFTool_ID->getEfficiencyScaleFactor(*(*signalelectrons)[0],esf_ID));
        ANA_CHECK(m_eEffSFTool_iso->getEfficiencyScaleFactor(*(*signalelectrons)[0],esf_iso));
	/*
        if (m_r3MatchingTool->match(*(*signalelectrons)[0], "HLT_e26_lhtight_ivarloose_L1EM22VHI",0.1,false)){
          ANA_CHECK(m_eEffSFTool_trig->getEfficiencyScaleFactor(*(*signalelectrons)[0],esf_trig));
        }
        */
        lepWeight*=esf_reco*esf_ID*esf_iso*esf_trig;
        evtWeight*=lepWeight;
        hist("h_Cutflow_selection_lyy")->Fill(7.5,evtWeight);
        leadingLep = (*signalelectrons)[0];
        lep_p4 = (*signalelectrons)[0]->p4();
        Wcharge = (*signalelectrons)[0]->charge();
        hist("h_eLeadingpt")->Fill(leadingLep->pt()/1e3,mcWeight*lepWeight);
        hist("h_eLeadingeta")->Fill(leadingLep->eta(),mcWeight*lepWeight);
        hist("h_eLeadingphi")->Fill(leadingLep->phi(),mcWeight*lepWeight);
      } else {
        return StatusCode::SUCCESS;
      }
    } else if (m_MuonChannel) {
      
      musf_trig=1.; //this is applied to the container
      //ANA_CHECK(m_muEffSFTool_trig->getTriggerScaleFactor(*signalmuons,musf_trig,"HLT_mu24_ivarmedium_L1MU14FCH"));
      if (signalmuons->size() > 0){
	float musf_TTVA= 1.;
	float musf_ID= 1.;
	float musf_iso= 1.;
	ANA_CHECK(m_muEffSFTool_TTVA->getEfficiencyScaleFactor(*(*signalmuons)[0],musf_TTVA,eventInfo));
	ANA_CHECK(m_muEffSFTool_ID->getEfficiencyScaleFactor(*(*signalmuons)[0],musf_ID,eventInfo));
	ANA_CHECK(m_muEffSFTool_iso->getEfficiencyScaleFactor(*(*signalmuons)[0],musf_iso,eventInfo));
	lepWeight*=musf_TTVA*musf_ID*musf_iso*musf_trig;
	evtWeight*=lepWeight;
	hist("h_Cutflow_selection_lyy")->Fill(7.5,evtWeight);
	leadingLep = (*signalmuons)[0];
	lep_p4 = (*signalmuons)[0]->p4();
	Wcharge = (*signalmuons)[0]->charge();
	hist("h_muLeadingpt")->Fill(leadingLep->pt()/1e3,mcWeight*lepWeight);
	hist("h_muLeadingeta")->Fill(leadingLep->eta(),mcWeight*lepWeight);
	hist("h_muLeadingphi")->Fill(leadingLep->phi(),mcWeight*lepWeight);
	
      } else {
	return StatusCode::SUCCESS;
      }
    }
    
    
    ANA_MSG_DEBUG("At least 1 signal photon");
    if (signalphotons->size()>1){ //At least 2 signal photons
      leadingGam = (*signalphotons)[0];
      subleadingGam = (*signalphotons)[1];
      double gsf_reco= 1.;
      double gsf_ID= 1.;
      double gsf_iso= 1.;
      double gsf_trig= 1.;
      //I have to do this annoying thing for now bc eff tools only want EGam objects, no IParticle
      ANA_CHECK(m_gEffSFTool_reco->getEfficiencyScaleFactor(*(*signalphotons)[0],gsf_reco)); //function cannot be called for IParticle
      ANA_CHECK(m_gEffSFTool_ID->getEfficiencyScaleFactor(*(*signalphotons)[0],gsf_ID));
      ANA_CHECK(m_gEffSFTool_iso->getEfficiencyScaleFactor(*(*signalphotons)[0],gsf_iso));
      //ANA_CHECK(m_gEffSFTool_trig->getEfficiencyScaleFactor(*(*signalphotons)[0],gsf_trig));
      leadgamWeight*=gsf_reco*gsf_ID*gsf_iso*gsf_trig;
      ANA_CHECK(m_gEffSFTool_reco->getEfficiencyScaleFactor(*(*signalphotons)[1],gsf_reco)); //function cannot be called for IParticle
      ANA_CHECK(m_gEffSFTool_ID->getEfficiencyScaleFactor(*(*signalphotons)[1],gsf_ID));
      ANA_CHECK(m_gEffSFTool_iso->getEfficiencyScaleFactor(*(*signalphotons)[1],gsf_iso));
      //ANA_CHECK(m_gEffSFTool_trig->getEfficiencyScaleFactor(*(*signalphotons)[1],gsf_trig));
      subgamWeight*=gsf_reco*gsf_ID*gsf_iso*gsf_trig;
	
      hist("h_gLeadingpt")->Fill(leadingGam->pt()/1e3,mcWeight*leadgamWeight);
      hist("h_gLeadingeta")->Fill(leadingGam->eta(),mcWeight*leadgamWeight);
      hist("h_gLeadingphi")->Fill(leadingGam->phi(),mcWeight*leadgamWeight);
      hist("h_gSubleadingpt")->Fill(subleadingGam->pt()/1e3,mcWeight*subgamWeight);
      hist("h_gSubleadingeta")->Fill(subleadingGam->eta(),mcWeight*subgamWeight);
      hist("h_gSubleadingphi")->Fill(subleadingGam->phi(),mcWeight*subgamWeight);
      
      evtWeight*=leadgamWeight*subgamWeight;
      TLorentzVector yy = leadingGam->p4() + subleadingGam->p4();
      hist("h_Cutflow_selection_lyy")->Fill(8.5,evtWeight);
    } else {
      return StatusCode::SUCCESS;
    } 
    
    
    
    
    ANA_MSG_INFO("Distance check between leading objects");
    Float_t DRg1l = DR(leadingGam->eta(), leadingGam->phi(), leadingLep->eta(), leadingLep->phi());
    Float_t DRg2l = DR(subleadingGam->eta(), subleadingGam->phi(), leadingLep->eta(), leadingLep->phi());
    Float_t DRg1g2 = DR(leadingGam->eta(), leadingGam->phi(), subleadingGam->eta(), subleadingGam->phi());
    hist("h_DR_glead_leplead")->Fill(DRg1l,evtWeight);
    hist("h_DR_gsublead_leplead")->Fill(DRg2l,evtWeight);
    hist("h_DR_glead_gsublead")->Fill(DRg1g2,evtWeight);
   
    if (DRg1g2>0.4 && DRg1l>0.4 && DRg2l>0.4){
      hist("h_Cutflow_selection_lyy")->Fill(9.5,evtWeight);
    } else {
      return StatusCode::SUCCESS;
    }
    
   
    
    ANA_MSG_DEBUG("Z(y) veto cuts");
    TLorentzVector yy = leadingGam->p4() + subleadingGam->p4();
    TLorentzVector lyy = leadingLep->p4() + leadingGam->p4() + subleadingGam->p4();
    TLorentzVector ly1 = leadingLep->p4() + leadingGam->p4();
    TLorentzVector ly2 = leadingLep->p4()  + subleadingGam->p4();
      
    hist("h_myy")->Fill(yy.M()/1e3,evtWeight);
    hist("h_mlyy")->Fill(lyy.M()/1e3,evtWeight);
    hist("h_mly1")->Fill(ly1.M()/1e3,evtWeight);
    hist("h_mly2")->Fill(ly2.M()/1e3,evtWeight);
      
    if (lyy.Pt()/1e3 > 30. && (lyy.M()/1e3 < 81. || lyy.M()/1e3 >100.)  && (ly1.M()/1e3 < 81. || ly1.M()/1e3 >100.) && (ly2.M()/1e3 < 81. || ly2.M()/1e3 >100.)){
      hist("h_Cutflow_selection_lyy")->Fill(10.5,evtWeight);
    } else {
      return StatusCode::SUCCESS;
    }
      
    ANA_MSG_DEBUG("Cuts on W boson transverse mass");  
    double dphiW = leadingLep->phi() - finalMETphi;
    if(dphiW>M_PI){dphiW=2*M_PI-dphiW;}
    double WmT = std::sqrt( 2 * leadingLep->pt()/1e3 * finalMET/1e3 * (1. - cos(dphiW) ) );
    double WpT = std::sqrt(pow(lep_p4.Px() + finalMETmpx,2) + pow(lep_p4.Py() + finalMETmpy, 2))/1e3;
    hist("h_WmT_nocuts")->Fill(WmT,evtWeight);
    hist("h_WpT_nocuts")->Fill(WpT,evtWeight);
    
    if (WmT>40.){
      hist("h_Cutflow_selection_lyy")->Fill(11.5,evtWeight);
    } else {
      return StatusCode::SUCCESS;
    }
      
    hist("h_WmT")->Fill(WmT,evtWeight);
    hist("h_WpT")->Fill(WpT,evtWeight);
    hist("h_Wcharge")->Fill(Wcharge,evtWeight);
      
    
    ANA_MSG_DEBUG("b-jet veto (Top background)");
    float totalbtag_ineff = 1.;
    //CP::CorrectionCode code;
    for(auto jet: *noPUjets){
      //const xAOD::BTagging* btag = xAOD::BTaggingUtilities::getBTagging( *jet )
      //Float_t dl1d = btag->auxdata("DL1dv01");
      bool btagged = static_cast<bool>(m_BTaggingSelectionTool->accept(*jet));
      if (btagged){
	float jetsf_btagtrue = 1.;
	code=m_BTaggingEfficiencyTool->getScaleFactor(*jet,  jetsf_btagtrue);
	if (code == CP::CorrectionCode::Ok){  
	  hist("h_NumberBtaggedJets")->Fill(1,mcWeight*jetsf_btagtrue);
	} else { 
	  ANA_MSG_DEBUG( "BTaggingEff - Cannot retrieve an SF for this jet \n");
	}
	
	return StatusCode::SUCCESS;
      } else {
	float jetsf_btag = 1.;
	code = m_BTaggingEfficiencyTool->getInefficiencyScaleFactor(*jet,jetsf_btag);
	if (code == CP::CorrectionCode::Ok){  
	  hist("h_nonBTagSF")->Fill(jetsf_btag);
	} else { 
	  ANA_MSG_DEBUG( "BTaggingEff - Cannot retrieve an SF for this jet \n");
	}
	/*
	float NNJvtsf=1.;
	code = m_NNJvtEfficiencyTool->getInefficiencyScaleFactor(*jet,NNJvtsf);
	if (code != CP::CorrectionCode::Ok){  
	  ANA_MSG_DEBUG( "NNJvtEffTool - Cannot retrieve an SF for this jet \n");
	}
	*/
	totalbtag_ineff*=jetsf_btag;
	evtWeight*=jetsf_btag; //*NNJvtsf;
      }
    }
    hist("h_btagInefficiency")->Fill(totalbtag_ineff);
    hist("h_Cutflow_selection_lyy")->Fill(12.5,evtWeight);
    

    ANA_MSG_DEBUG("End of event selection");
    hist("h_WmT_final")->Fill(WmT,evtWeight);
    hist("h_WpT_final")->Fill(WpT,evtWeight);
    hist("h_Wcharge_final")->Fill(Wcharge,evtWeight);
    if (m_ElectronChannel){
      hist("h_eLeadingpt_final")->Fill(leadingLep->pt()/1e3,evtWeight);
    } else if (m_MuonChannel){
      hist("h_muLeadingpt_final")->Fill(leadingLep->pt()/1e3,evtWeight);
    }
    hist("h_gLeadingpt_final")->Fill(leadingGam->pt()/1e3,evtWeight);
    hist("h_gSubleadingpt_final")->Fill(subleadingGam->pt()/1e3,evtWeight);
    hist("h_RecoMET_met_final")->Fill(finalMET/1e3,evtWeight);
    hist("h_RecoMET_phi_final")->Fill(finalMETphi,evtWeight);
    hist("h_AllMETpt_PVSoftTrk_final")->Fill(((*newMET)["PVSoftTrk"]->met())/1e3,evtWeight);
    
    hist("h_ExpectedEvents_exp")->Fill(0.5,evtWeight);
    
    return StatusCode::SUCCESS;
  }


  StatusCode WyyMCAnalysis :: finalize ()
  {
    //Photon cutflow
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(2, "Ambiguities");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(3, "Quality");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(4, "Eta");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(5, "pt > 20 GeV");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(6, "OR");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(7, "TightID");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(8, "TightCaloOnly");
    hist("h_gCutflow_selection")->GetXaxis()->SetBinLabel(9, "SF");
    
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(2, "Loose");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(3, "Tight");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(4, "LoosePrime4");
    hist("h_gamID_Tests")->GetXaxis()->SetBinLabel(5, "LoosePrime5");
    
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(2, "Photons");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(3, "Electrons");
    hist("h_gamAuthor_Tests")->GetXaxis()->SetBinLabel(4, "Ambiguous");
    
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(1, "All");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(2, "PV");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(3, "1l+2y");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(4, "Trigger");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(5, "No OF base lep");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(6, "No SF signal lep");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(7, "MET>25 GeV");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(8, ">0 signal lep");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(9, ">1 signal y");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(10, "DRij > 0.4");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(11, "Z(y) veto");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(12, "mTW>40 GeV");
    hist("h_Cutflow_selection_lyy")->GetXaxis()->SetBinLabel(13, "b-jet veto");
  
    if (m_ElectronChannel){
      //Cutflow at event level
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(2, "Single e Legacy");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(3, "Single e Phase I");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(4, "eyy Legacy");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(5, "eyy Phase I");
      
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(2, "DFCommonElectronsLHVeryLoose");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(3, "DFCommonElectronsLHLoose");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(4, "DFCommonElectronsLHLooseBL");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(5, "DFCommonElectronsLHMedium");
      hist("h_EleID_Tests")->GetXaxis()->SetBinLabel(6, "DFCommonElectronsLHTight");
      
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(2, "Photons");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(3, "Electrons");
      hist("h_EleAuthor_Tests")->GetXaxis()->SetBinLabel(4, "Ambiguous");
      
      //Electron cutflow
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(2, "Ambiguities");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(3, "Quality");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(4, "Eta");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(5, "LHLooseBL ID");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(6, "pt > 6 GeV");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(7, "TTVA");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(8, "OR");
      //hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(9, "Trigger");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(9, "LHMediumID");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(10, "pt > 25 GeV");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(11, "Tight_VarRad");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(12, "Track sig < 5");
      hist("h_eCutflow_selection")->GetXaxis()->SetBinLabel(13, "SF");
    
    } else if (m_MuonChannel){
       //Cutflow at event level
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(2, "Single mu L+PI14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(3, "Single mu L+PI18");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(4, "muyy Legacy14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(5, "muyy Legacy18");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(6, "muyy PhaseI14");
      hist("h_Cutflow_events")->GetXaxis()->SetBinLabel(7, "muyy PhaseI18");
      //Muon cutflow
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(1, "All");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(2, "LHMedium ID & eta");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(3, "pt > 6 GeV");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(4, "TTVA");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(5, "OR");
      //hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(6, "Trigger");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(6, "pt > 25 GeV");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(7, "Track sig < 3");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(8, "Tight_VarRad");
      hist("h_muCutflow_selection")->GetXaxis()->SetBinLabel(9, "SF");
    }
    
    hist("h_Wcharge")->GetXaxis()->SetBinLabel(1, "-1");
    hist("h_Wcharge")->GetXaxis()->SetBinLabel(3, "+1");
    hist("h_Wcharge_final")->GetXaxis()->SetBinLabel(1, "-1");
    hist("h_Wcharge_final")->GetXaxis()->SetBinLabel(3, "+1");
    hist("h_AllElectronscharge")->GetXaxis()->SetBinLabel(1, "-1");
    hist("h_AllElectronscharge")->GetXaxis()->SetBinLabel(3, "+1");
    hist("h_AllMuonscharge")->GetXaxis()->SetBinLabel(1, "-1");
    hist("h_AllMuonscharge")->GetXaxis()->SetBinLabel(3, "+1");
    
    
    double histoScale = m_xsection*m_Luminosity/m_SoW;
    hist("h_ExpectedEvents_exp")->Scale(histoScale);
    hist("h_AllPhotonspt")->Scale(histoScale);
    hist("h_AllPhotonseta")->Scale(histoScale);
    hist("h_AllPhotonsphi")->Scale(histoScale);
    hist("h_NumberPhotons")->Scale(histoScale);
    hist("h_NumberbasePhotons")->Scale(histoScale);
    hist("h_basephotonspt")->Scale(histoScale);
    hist("h_onlygampt")->Scale(histoScale);
    
    hist("h_AllElectronspt")->Scale(histoScale);
    hist("h_AllElectronseta")->Scale(histoScale);
    hist("h_AllElectronsphi")->Scale(histoScale);
    hist("h_AllElectronscharge")->Scale(histoScale);
    hist("h_NumberElectrons")->Scale(histoScale);
    hist("h_NumberPreselectElectrons")->Scale(histoScale);
    hist("h_NumberbaseElectrons")->Scale(histoScale);
    hist("h_preselelectronspt")->Scale(histoScale);
    hist("h_baseelectronspt")->Scale(histoScale);
    hist("h_onlyelept")->Scale(histoScale);
    hist("h_onlyelept_noTrigSF")->Scale(histoScale);
    
    hist("h_AllMuonspt")->Scale(histoScale);
    hist("h_AllMuonseta")->Scale(histoScale);
    hist("h_AllMuonsphi")->Scale(histoScale);
    hist("h_AllMuonscharge")->Scale(histoScale);
    hist("h_NumberMuons")->Scale(histoScale);
    hist("h_NumberPreselectMuons")->Scale(histoScale);
    hist("h_NumberbaseMuons")->Scale(histoScale);
    hist("h_preselmuonspt")->Scale(histoScale);
    hist("h_preselmuonseta")->Scale(histoScale);
    hist("h_onlymupt")->Scale(histoScale);
    hist("h_onlymupt_noTrigSF")->Scale(histoScale);
    hist("h_basemuonspt")->Scale(histoScale);
    
    hist("h_AllTauJetspt")->Scale(histoScale);
    hist("h_AllTauJetseta")->Scale(histoScale);
    hist("h_AllTauJetsphi")->Scale(histoScale);
    hist("h_NumberTauJets")->Scale(histoScale);
    hist("h_AllJetspt")->Scale(histoScale);
    hist("h_basejetspt")->Scale(histoScale);
    hist("h_AllJetseta")->Scale(histoScale);
    hist("h_AllJetsphi")->Scale(histoScale);
    hist("h_NumberJets")->Scale(histoScale);
    hist("h_NumberbaseJets")->Scale(histoScale);
    hist("h_NumberJets_noPU")->Scale(histoScale);
    hist("h_NumberBtaggedJets")->Scale(histoScale);
    hist("h_onlyjetpt")->Scale(histoScale);
 
    //Truth level plots
    hist("h_TruthPhotonspt")->Scale(histoScale);
    hist("h_TruthPhotonspt_baseline")->Scale(histoScale);
    hist("h_TruthPhotonspt_baseline_prompt")->Scale(histoScale);
    hist("h_TruthPhotonseta")->Scale(histoScale);
    hist("h_TruthPhotonsphi")->Scale(histoScale);
    hist("h_NumberTruthPhotons")->Scale(histoScale);
    hist("h_TruthElectronspt")->Scale(histoScale);
    hist("h_TruthElectronspt_preselect")->Scale(histoScale);
    hist("h_TruthElectronspt_preselect_prompt")->Scale(histoScale);
    hist("h_TruthElectronseta")->Scale(histoScale);
    hist("h_TruthElectronsphi")->Scale(histoScale);
    hist("h_NumberTruthElectrons")->Scale(histoScale);
    hist("h_TruthMuonspt")->Scale(histoScale);
    hist("h_TruthMuonspt_preselect")->Scale(histoScale);
    hist("h_TruthMuonspt_preselect_trig")->Scale(histoScale);
    hist("h_TruthMuonspt_preselect_prompt")->Scale(histoScale);
    hist("h_TruthMuonseta")->Scale(histoScale);
    hist("h_TruthMuonsphi")->Scale(histoScale); 
    hist("h_NumberTruthMuons")->Scale(histoScale);
    hist("h_TruthTauspt")->Scale(histoScale);
    hist("h_TruthTauseta")->Scale(histoScale);
    hist("h_TruthTausphi")->Scale(histoScale); 
    hist("h_NumberTruthTaus")->Scale(histoScale);
    hist("h_TruthJetspt")->Scale(histoScale);
    hist("h_TruthJetspt_baseline")->Scale(histoScale);
    hist("h_TruthJetseta")->Scale(histoScale);
    hist("h_TruthJetsphi")->Scale(histoScale); 
    hist("h_NumberTruthJets")->Scale(histoScale);
    hist("h_TruthNeutrinospt")->Scale(histoScale);
    hist("h_TruthNeutrinoseta")->Scale(histoScale);
    hist("h_TruthNeutrinosphi")->Scale(histoScale);
    hist("h_NumberTruthNeutrinos")->Scale(histoScale);
    hist("h_TruthMETphi")->Scale(histoScale);
    hist("h_TruthNeutrinospt_prompt")->Scale(histoScale);
    hist("h_TruthMETmet_exp")->Scale(histoScale);
    
    //MET plots
    hist("h_AllMETpt_SoftClus")->Scale(histoScale);
    hist("h_AllMETpt_PVSoftTrk")->Scale(histoScale);
    hist("h_AllMETphi_SoftClus")->Scale(histoScale);
    hist("h_AllMETphi_PVSoftTrk")->Scale(histoScale);
    hist("h_addPVTrknClus")->Scale(histoScale);
    
    
    hist("h_RecoMET_met")->Scale(histoScale);
    hist("h_RecoMET_sumet")->Scale(histoScale);
    hist("h_RecoMET_phi")->Scale(histoScale);
    
    //UnderstandingMET
    hist("h_relMETdiff_0_20_truth")->Scale(histoScale);
    hist("h_relMETdiff_20_40_truth")->Scale(histoScale);
    hist("h_relMETdiff_40_60_truth")->Scale(histoScale);
    hist("h_relMETdiff_60_80_truth")->Scale(histoScale);
    hist("h_relMETdiff_80_120_truth")->Scale(histoScale);
    hist("h_relMETdiff_120_160_truth")->Scale(histoScale);
    hist("h_relMETdiff_160_200_truth")->Scale(histoScale);
    
    
    hist("h_gMETpt")->Scale(histoScale);
    hist("h_eMETpt")->Scale(histoScale);
    hist("h_muMETpt")->Scale(histoScale);
    hist("h_tMETpt")->Scale(histoScale);
    hist("h_jMETpt")->Scale(histoScale);
    hist("h_expMETsum")->Scale(histoScale);
    
   
    //Overlaps
    hist("h_DRej")->Scale(histoScale);
    hist("h_DRgj")->Scale(histoScale);
    hist("h_DRge")->Scale(histoScale);
    hist("h_DRgmu")->Scale(histoScale);
    hist("h_DRmue")->Scale(histoScale);
    hist("h_DRmuj")->Scale(histoScale);
    hist("h_DRtj")->Scale(histoScale);
    hist("h_DRmut")->Scale(histoScale);
    hist("h_DRet")->Scale(histoScale);
    hist("h_DRgt")->Scale(histoScale);
    hist("h_DRej_noOL")->Scale(histoScale);
    hist("h_DRgj_noOL")->Scale(histoScale);
    hist("h_DRge_noOL")->Scale(histoScale);
    hist("h_DRgmu_noOL")->Scale(histoScale);
    hist("h_DRmue_noOL")->Scale(histoScale);
    hist("h_DRmuj_noOL")->Scale(histoScale);
    hist("h_DRtj_noOL")->Scale(histoScale);
    hist("h_DRmut_noOL")->Scale(histoScale);
    hist("h_DRet_noOL")->Scale(histoScale);
    hist("h_DRgt_noOL")->Scale(histoScale);
    
    //PHOTONS
    hist("h_gCutflow_selection")->Scale(histoScale);
    hist("h_NumbergoodPhotons")->Scale(histoScale);
    hist("h_NumberbasePhotons")->Scale(histoScale);
    hist("h_goodphotonspt")->Scale(histoScale);
    hist("h_basephotonspt")->Scale(histoScale);
    hist("h_gamID_Tests")->Scale(histoScale);
    hist("h_gamAuthor_Tests")->Scale(histoScale);
    //Isolation 
    hist("h_topoetcone40_giso")->Scale(histoScale);
    hist("h_ptcone20_giso")->Scale(histoScale);
    hist("h_topoetcone40_gcorr")->Scale(histoScale);
    hist("h_ptcone20_gcorr")->Scale(histoScale);
    hist("h_topoetcone40_g")->Scale(histoScale);
    hist("h_ptcone20_g")->Scale(histoScale);
    //checks
    hist("h_topoetcone40_gcorr_tightID")->Scale(histoScale);
    hist("h_ptcone20_gcorr_tightID")->Scale(histoScale);
    hist("h_topoetcone40_g_tightID")->Scale(histoScale);
    hist("h_ptcone20_g_tightID")->Scale(histoScale);
    hist("h_topoetcone40_glead_tightID")->Scale(histoScale);
    hist("h_ptcone20_glead_tightID")->Scale(histoScale);
    hist("h_topoetcone40_gsublead_tightID")->Scale(histoScale);
    hist("h_ptcone20_gsublead_tightID")->Scale(histoScale);
    //Leading and subleading photons
    hist("h_gLeadingpt")->Scale(histoScale);
    hist("h_gLeadingeta")->Scale(histoScale);
    hist("h_gLeadingphi")->Scale(histoScale);
    hist("h_gSubleadingpt")->Scale(histoScale);
    hist("h_gSubleadingeta")->Scale(histoScale);
    hist("h_gSubleadingphi")->Scale(histoScale);
    
    hist("h_Cutflow_events")->Scale(histoScale);
    hist("h_Cutflow_selection_lyy")->Scale(histoScale);
    hist("h_DR_glead_leplead")->Scale(histoScale);
    hist("h_DR_gsublead_leplead")->Scale(histoScale);
    hist("h_DR_glead_gsublead")->Scale(histoScale);
    if (m_ElectronChannel){
      hist("h_eCutflow_selection")->Scale(histoScale);
      hist("h_EleAuthor_Tests")->Scale(histoScale);
      hist("h_EleID_Tests")->Scale(histoScale);
      hist("h_NumbergoodElectrons")->Scale(histoScale);
      hist("h_goodelectronspt")->Scale(histoScale);
      //Isolation
      hist("h_ptvarcone30_e")->Scale(histoScale);
      hist("h_topoetcone20_e")->Scale(histoScale);
      hist("h_ptvarcone30_eiso")->Scale(histoScale);
      hist("h_topoetcone20_eiso")->Scale(histoScale);
      //leading electron
      hist("h_eLeadingpt")->Scale(histoScale);
      hist("h_eLeadingeta")->Scale(histoScale);
      hist("h_eLeadingphi")->Scale(histoScale);
      //Final
      hist("h_eLeadingpt_final")->Scale(histoScale);
      
    } else if (m_MuonChannel){
      hist("h_muCutflow_selection")->Scale(histoScale);
      hist("h_NumbergoodMuons")->Scale(histoScale);
      hist("h_goodmuonspt")->Scale(histoScale);
      //Isolation
      hist("h_ptvarcone30_mu")->Scale(histoScale);
      hist("h_topoetcone20_mu")->Scale(histoScale);
      hist("h_ptvarcone30_muiso")->Scale(histoScale);
      hist("h_topoetcone20_muiso")->Scale(histoScale);
      //leading muon
      hist("h_muLeadingpt")->Scale(histoScale);
      hist("h_muLeadingeta")->Scale(histoScale);
      hist("h_muLeadingphi")->Scale(histoScale);
      //Final
      hist("h_muLeadingpt_final")->Scale(histoScale);
    }
    
    //Invariant mass
    hist("h_myy")->Scale(histoScale);
    hist("h_mlyy")->Scale(histoScale);
    hist("h_mly1")->Scale(histoScale);
    hist("h_mly2")->Scale(histoScale);
    
    //W boson
    hist("h_WmT_nocuts")->Scale(histoScale);
    hist("h_WpT_nocuts")->Scale(histoScale);
    hist("h_WmT")->Scale(histoScale);
    hist("h_WpT")->Scale(histoScale);
    
    hist("h_gLeadingpt_final")->Scale(histoScale);
    hist("h_gSubleadingpt_final")->Scale(histoScale);
    hist("h_AllMETpt_PVSoftTrk_final")->Scale(histoScale);
    hist("h_WmT_final")->Scale(histoScale);
    hist("h_WpT_final")->Scale(histoScale);
    hist("h_Wcharge_final")->Scale(histoScale);
    hist("h_RecoMET_met_final")->Scale(histoScale);
    hist("h_RecoMET_phi_final")->Scale(histoScale);
    
    
    if (m_PHYS){
      hist("h_AllPhotonspt_noCalib")->Scale(histoScale);
      hist("h_AllMuonspt_noCalib")->Scale(histoScale);
      hist("h_AllElectronspt_noCalib")->Scale(histoScale);
      hist("h_AllTauJetspt_noCalib")->Scale(histoScale);
      hist("h_AllJetspt_noCalib")->Scale(histoScale);
      hist("h_AllTopoJetspt_noCalib")->Scale(histoScale);
    }
    
    return StatusCode::SUCCESS;
  }


  StatusCode WyyMCAnalysis::parseList(const std::string& line, std::vector<std::string>& result) const {
    //
    // Copied from AthMonitorAlgorithm - parse the supplied trigger chains
    //
    std::string item;
    std::stringstream ss(line);

    ANA_MSG_DEBUG( "WyyMCAnalysis::parseList()" );

    while ( std::getline(ss, item, ',') ) {
      std::stringstream iss(item); // remove whitespace
      iss >> item;
      result.push_back(item);
    }

    return StatusCode::SUCCESS;
  }
  
  
Float_t WyyMCAnalysis :: getSumOfWeights()
  {
    //Funtion to retrieve the total sum of weights of a file within a sample - NOT the total sum of weights of the sample!
    Float_t m_totalEvents = 0.;
    const xAOD::CutBookkeeperContainer* bks = nullptr;
    if ((inputMetaStore()->retrieve(bks, "CutBookkeepers")) == StatusCode::SUCCESS){
      const xAOD::CutBookkeeper* all = 0; int maxCycle=-1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
      for(auto cbk : *bks) {  //inputStream is AOD bc that was the original input
	if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle) { maxCycle=cbk->cycle(); all = cbk; } }
      m_totalEvents += all->sumOfEventWeights(); //also have all->nAcceptedEvents() which is simple event count, and all->sumOfEventWeightsSquared()
      return m_totalEvents;
    } else {
      ANA_MSG_ERROR("Could not retrieve sum of weights");
      return 0.0;
    }
  }

Float_t WyyMCAnalysis :: DR (Float_t eta1,Float_t phi1,Float_t eta2,Float_t phi2)
  {
    Float_t deta=fabs(eta1-eta2);
    Float_t dphi=fabs(phi1-phi2);
    if(dphi>M_PI){dphi=2*M_PI-dphi;}
    return sqrt(pow(deta,2)+pow(dphi,2));
  }

bool WyyMCAnalysis :: passEtaCuts(const xAOD::Egamma *p)
  {
    if (p->type()==xAOD::Type::Electron && ((fabs(p->caloCluster()->etaBE(2))<1.37 || fabs(p->caloCluster()->etaBE(2)) > 1.52) && fabs(p->caloCluster()->etaBE(2)) < 2.47)){
      return true;
    } else if (p->type()==xAOD::Type::Photon && ((fabs(p->caloCluster()->etaBE(2))<1.37 || fabs(p->caloCluster()->etaBE(2)) > 1.52) && fabs(p->caloCluster()->etaBE(2)) < 2.37)){
      return true;
    } else {
      //ANA_MSG_ERROR("Trying to apply acceptance cuts - not a photon or an electron");
      return false;
    }
  }
  
bool WyyMCAnalysis :: passAmbCuts(const xAOD::Egamma *p)
  {
    m_AmbiguityTool->ambiguityResolve(*p);
    //static const SG::AuxElement::Accessor<uint8_t> acc_amb("ambiguityType"); 
    if (p->type()==xAOD::Type::Electron && ((p->author()==xAOD::EgammaParameters::AuthorAmbiguous) || (p->author()==xAOD::EgammaParameters::AuthorElectron)) && m_AmbiguityTool->accept(*p)){ //its comaring pointer with integer
      return true;
    } else if (p->type()==xAOD::Type::Photon && ((p->author()==xAOD::EgammaParameters::AuthorAmbiguous) || (p->author()==xAOD::EgammaParameters::AuthorPhoton)) && m_AmbiguityTool->accept(*p)){
      return true;
    } else {
      return false;
    }
  }

std::vector<bool> WyyMCAnalysis :: TTVAmuon(const xAOD::Muon *p, const xAOD::Vertex* pVtx,const xAOD::EventInfo *eventInfo) 
  { //returns  d0sig,Dz0sin pass 
    Float_t d0sig;
    Float_t Dz0sin;
    auto *Trk = p->primaryTrackParticle();
    if (!Trk) {
      std::cout << "Muon with no track " << eventInfo->runNumber() << " " << eventInfo->eventNumber() << "\n";
      return {false,false};
    }
    Dz0sin = (Trk->z0()+Trk->vz()-pVtx->z())*std::sin(Trk->theta()); 
    try {
      d0sig = xAOD::TrackingHelpers::d0significance(Trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY());
    } catch (...) {
      ANA_MSG_DEBUG("Invalid beamspot");
      try {
	d0sig = xAOD::TrackingHelpers::d0significance(Trk);
      } catch (...) {
	ANA_MSG_WARNING("Ridiculous exception thrown");
	return {false,false};
      }
    } 
    if (fabs(Dz0sin)<0.5){
      if (fabs(d0sig)<3){
	return {true,true};
      } else {
	return {true,false};
      }
    } else {
      return {false,false};
    }
  }
  
std::vector<bool> WyyMCAnalysis :: TTVAele(const xAOD::Electron *p, const xAOD::Vertex* pVtx,const xAOD::EventInfo *eventInfo) 
  { //returns  d0sig,Dz0sin pass 
    Float_t d0sig;
    Float_t Dz0sin;
    auto *Trk = p->trackParticle();
    if (!Trk) {
      std::cout << "Muon with no track " << eventInfo->runNumber() << " " << eventInfo->eventNumber() << "\n";
      return {false,false};
    }
    Dz0sin = (Trk->z0()+Trk->vz()-pVtx->z())*std::sin(Trk->theta()); 
    try {
      d0sig = xAOD::TrackingHelpers::d0significance(Trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY());
    } catch (...) {
      ANA_MSG_DEBUG("Invalid beamspot");
      try {
	d0sig = xAOD::TrackingHelpers::d0significance(Trk);
      } catch (...) {
	ANA_MSG_WARNING("Ridiculous exception thrown");
	return {false,false};
      }
    }
    if (fabs(Dz0sin)<0.5){
      if (fabs(d0sig)<5){
	return {true,true};
      } else {
	return {true,false};
      }
    } else {
      return {false,false};
    }
  }
  
bool WyyMCAnalysis :: singletrigger (bool m_Legacy, bool m_PhaseI, const xAOD::IParticle *p)
  { //Lowest unprescaled triggers
    bool triggerpass = false;
    if (p->type()==xAOD::Type::Electron){
      if (m_PhaseI){
	triggerpass = m_r3MatchingTool->match(*p, "HLT_e26_lhtight_ivarloose_L1eEM26M",0.1,false);
      } else if (m_Legacy){
	triggerpass = m_r3MatchingTool->match(*p, "HLT_e26_lhtight_ivarloose_L1EM22VHI",0.1,false);
      }
    } else if (p->type()==xAOD::Type::Muon){
      if (m_PhaseI){
	triggerpass = m_r3MatchingTool->match(*p, "HLT_mu24_ivarmedium_L1MU14FCH",0.1,false); //NOT UPDATED
      } else if (m_Legacy){
	triggerpass = m_r3MatchingTool->match(*p, "HLT_mu24_ivarmedium_L1MU14FCH",0.1,false);
      }
    } else {
      return false;
    }
    return triggerpass;
  }
  
  
bool WyyMCAnalysis :: WyyTrigger (bool m_Legacy, bool m_PhaseI, std::vector<const xAOD::IParticle*> Vec) 
  {
    bool triggerpass = false;
    if (m_ElectronChannel){
      if (m_PhaseI){
	triggerpass = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1eEM24L_3eEM12L",0.1,false);
      } else if (m_Legacy){
	triggerpass = m_r3MatchingTool->match(Vec, "HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH",0.1,false);
      }
    } else if (m_MuonChannel){
      if (m_PhaseI){
	triggerpass = m_r3MatchingTool->match(Vec, "HLT_2g10_loose_L1eEM9_mu20_L1MU18VFCH",0.1,false); 
      } else if (m_Legacy){
	triggerpass = m_r3MatchingTool->match(Vec, "HLT_2g10_loose_mu20_L1MU18VFCH",0.1,false);
      }
    } else {
      return false;
    }
    return triggerpass;
    
  }

