#ifndef Analysis_WyyAnalysis_DICT_H
#define Analysis_WyyAnalysis_DICT_H

// Includes all the header files that you need to create dictionaries for.

#include <WyyAnalysis/WyyMCAnalysis.h>
#include <WyyAnalysis/WyyDataAnalysis.h>
#include <WyyAnalysis/WyyTrigEfficiency.h>
#endif
