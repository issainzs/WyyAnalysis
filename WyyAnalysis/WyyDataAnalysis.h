#ifndef Analysis_WyyDataAnalysis_H
#define Analysis_WyyDataAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>
#include "AsgTools/AnaToolHandle.h"
#include "AsgTools/IAsgTool.h"

#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/R3MatchingTool.h>
#include "IsolationSelection/IIsolationSelectionTool.h"
//#include "PhotonVertexSelection/IPhotonPointingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "AssociationUtils/IOverlapRemovalTool.h"

#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "MuonMomentumCorrections/MuonCalibTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "TauAnalysisTools/ITauSmearingTool.h"

#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "EgammaAnalysisInterfaces/IEGammaAmbiguityTool.h"

#include "xAODMissingET/MissingETContainer.h"
#include "METUtilities/METHelpers.h"
#include "METInterface/IMETMaker.h"
#include "METInterface/IMETRebuilder.h"
#include "METInterface/IMETSystematicsTool.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODCore/ShallowCopy.h"

#include <boost/any.hpp>

namespace CP {
  class IIsolationSelectionTool;
  //class IIsolationCorrectionTool;
  //class IPhotonPointingTool;
  class MuonSelectionTool;
  class EgammaCalibrationAndSmearingTool;
  class MuonCalibTool;
}
namespace ORUtils {
  class IOverlapRemovalTool;
}

class WyyDataAnalysis : public EL::AnaAlgorithm
{
 public:
  WyyDataAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  //Configuration of tools - examples:
  //ToolHandle<Trig::TrigDecisionTool> m_trigDecTool - when config is in JO or can be used with default settings
  //asg::AnaToolHandle<IBTaggingSelectionTool> m_BTaggingSelectionTool; - when config is set in the cxx
   
  //General
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool{this,"triggerDecTool1","","trigger decision tool"};
  ToolHandle<Trig::R3MatchingTool> m_r3MatchingTool{this, "R3MatchingTool", "Trig::R3MatchingTool", "R3MatchingTool"};
  ToolHandle<ORUtils::IOverlapRemovalTool> m_orTool{this, "OverlapRemovalTool", "", "ORTool"};
  
  
  //Selection Tools
  ToolHandle<IEGammaAmbiguityTool> m_AmbiguityTool{this, "AmbiguityTool", "EGammaAmbiguityTool/egammaambiguitytool", "Tool to resolve ambiguitites"};
  ToolHandle<CP::IIsolationSelectionTool> m_isolationSelectionTool;
  ToolHandle<CP::IMuonSelectionTool> m_MuonSelectionTool;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_BTaggingSelectionTool;
  asg::AnaToolHandle<CP::IJetJvtEfficiency> m_NNJvtEfficiencyTool;
  
  /*
  //Scale factors / Efficiency tools
  asg::AnaToolHandle<IBTaggingEfficiencyTool> m_BTaggingEfficiencyTool;
  //asg::AnaToolHandle<CP::IsolationCorrectionTool> m_IsolationCorrectionTool; //not available for rel 24?
  //asg::AnaToolHandle<CP::NNJvtSelectionTool> m_NNJvtSelectionTool;
  
  
  asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_eEffSFTool_reco;
  asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_eEffSFTool_ID;
  asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_eEffSFTool_trig;
  asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_eEffSFTool_iso;
  
  
  asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_gEffSFTool_reco;
  asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_gEffSFTool_ID;
  asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_gEffSFTool_trig;
  asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_gEffSFTool_iso;
  
  asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> m_muEffSFTool_TTVA;
  asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> m_muEffSFTool_ID;
  asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> m_muEffSFTool_iso;
  asg::AnaToolHandle<CP::IMuonTriggerScaleFactors> m_muEffSFTool_trig;
  
  asg::AnaToolHandle<CP::IIsolationCorrectionTool> m_IsoCorrTool;
  asg::AnaToolHandle<CP::IPhotonPointingTool> m_gPointingTool;
  */
  
  
  //Calibration Tools - the ones that are working well
  ToolHandle<CP::EgammaCalibrationAndSmearingTool> m_EgammaCalibrationAndSmearingTool{this, "EgammaCalibrationAndSmearingTool", "", "EgammaCalib"};
  ToolHandle<CP::MuonCalibTool> m_MuonCalibTool;
  asg::AnaToolHandle<IJetCalibrationTool> m_JetPflowCalibrationTool;
  JetCleaningTool *m_JetCleaningTool;
  //ToolHandle<TauAnalysisTools::ITauSmearingTool> m_TauSmearingTool; //this one gives problems
  //asg::AnaToolHandle<TauAnalysisTools::ITauSmearingTool> m_TauSmearingTool;
  
  
  
  //ToolHandle<CP::IJetJvtEfficiency> m_JvtToolHandle{"CP::IJetJvtEfficiency/NNJvt"};
  ToolHandle<IMETMaker> m_METMaker;
  /*
  //Tools that dont work - under investigation
  asg::AnaToolHandle<IMETSystematicsTool> m_METSystTool;
  StringArrayProperty m_METSysNames;
  */
  std::string m_selectionLabel;
  std::string m_overlapLabel;
  
  Gaudi::Property<bool> m_PHYS {this,"DAODPhys","","Boolean for DAOD PHYS format"};
  Gaudi::Property<bool> m_Legacy {this,"Legacy","","Boolean for Legacy system"};
  Gaudi::Property<bool> m_PhaseI {this,"PhaseI","","Boolean for PhaseI system"};
  Gaudi::Property<bool> m_ElectronChannel {this,"ElectronChannel","","Boolean for electron analysis"};
  Gaudi::Property<bool> m_MuonChannel {this,"MuonChannel","","Boolean for muon analysis"};
  Gaudi::Property<bool> m_doScaling {this,"doScaling","","Boolean for doing the histogram scaling"};
  Gaudi::Property<float> m_xsection {this,"xsection",{},"Cross section for this process"};
  Gaudi::Property<float> m_Luminosity {this,"Luminosity",{},"Luminosity"};
  
  //My functions
  Float_t DR (Float_t eta1,Float_t phi1,Float_t eta2,Float_t phi2);
  bool passEtaCuts(const xAOD::Egamma *p);
  bool passAmbCuts(const xAOD::Egamma *p);
  std::vector<bool> TTVAele(const xAOD::Electron *p, const xAOD::Vertex* pVtx,const xAOD::EventInfo *eventInfo);
  std::vector<bool> TTVAmuon(const xAOD::Muon *p, const xAOD::Vertex* pVtx,const xAOD::EventInfo *eventInfo);
  bool singletrigger (bool m_Legacy, bool m_PhaseI, const xAOD::IParticle *p);
  bool WyyTrigger (bool m_Legacy, bool m_PhaseI, std::vector<const xAOD::IParticle*> Vec);
  
  /*
  //DAOD EGAMx,PHYS naming - Consider moving this to script to avoid comentng out
  SG::ReadHandleKey<xAOD::ElectronContainer> m_ElectronContainerKey{ this, "ElectronContainerName", "Electrons", "" };
  SG::ReadHandleKey<xAOD::PhotonContainer> m_PhotonContainerKey{ this, "PhotonContainerName", "Photons", "" };
  SG::ReadHandleKey<xAOD::MuonContainer> m_MuonContainerKey{ this, "MuonContainerName", "Muons", "" };
  SG::ReadHandleKey<xAOD::TauJetContainer> m_TauJetContainerKey{ this, "TauJetContainerName", "TauJets", "" };
  SG::ReadHandleKey<xAOD::VertexContainer> m_VertexContainerKey{ "PrimaryVertices" };
  SG::ReadHandleKey<xAOD::JetContainer> m_JetPflowContainerKey {"AntiKt4EMPFlowJets"};
  //SG::ReadHandleKey<xAOD::JetContainer> m_JetTopoContainerKey {"AntiKt4EMTopoJets"};
  */
  
  
  //DAOD_PHYSLITE naming
  SG::ReadHandleKey<xAOD::ElectronContainer> m_ElectronContainerKey{ this, "ElectronContainerName", "AnalysisElectrons", "" };
  SG::ReadHandleKey<xAOD::PhotonContainer> m_PhotonContainerKey{ this, "PhotonContainerName", "AnalysisPhotons", "" };
  SG::ReadHandleKey<xAOD::JetContainer> m_JetPflowContainerKey{ this, "JetContainerName", "AnalysisJets", "" };
  SG::ReadHandleKey<xAOD::MuonContainer> m_MuonContainerKey{ this, "MuonContainerName", "AnalysisMuons", "" };
  SG::ReadHandleKey<xAOD::TauJetContainer> m_TauJetContainerKey{ this, "TauJetContainerName", "AnalysisTauJets", "" };
  SG::ReadHandleKey<xAOD::VertexContainer> m_VertexContainerKey{ "PrimaryVertices" };
  
  
  
  // borrowing this from AthMonitorAlgorithm
  Gaudi::Property<std::string> m_triggerChainString {this,"TriggerChain","","comma separated list of chains"}; ///< Trigger chain string pulled from the job option and parsed into a vector
  std::vector<std::string> m_vTrigChainNames; ///< Vector of trigger chain names parsed from trigger chain string
  StatusCode parseList( const std::string& line, std::vector<std::string>& result ) const;
  void unpackTriggerCategories( std::vector<std::string>& vTrigChainNames ) const;

};

#endif

