#ifndef Analysis_WyyTrigEfficiency_H
#define Analysis_WyyTrigEfficiency_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>
#include "AsgTools/AnaToolHandle.h"
#include "AsgTools/IAsgTool.h"
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/R3MatchingTool.h>
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "AssociationUtils/IOverlapRemovalTool.h"
#include "EgammaAnalysisInterfaces/IEGammaAmbiguityTool.h"

#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "MuonMomentumCorrections/MuonCalibTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"


#include "xAODEgamma/ElectronContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/MuonContainer.h"
//#include "xAODJet/JetAuxContainer.h"
//#include "xAODJet/JetContainer.h"
#include "xAODCore/ShallowCopy.h"

#include <boost/any.hpp>

namespace CP {
  class IIsolationSelectionTool;
  class IEgammaCalibrationAndSmearingTool;
  class MuonCalibTool;
  class IMuonSelectionTool;
    
}
namespace ORUtils {
  class IOverlapRemovalTool;
}

class WyyTrigEfficiency : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  WyyTrigEfficiency (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  Gaudi::Property<bool> m_PHYS {this,"DAODPhys","","Boolean for PHYS Format"};
  Gaudi::Property<bool> m_Legacy {this,"Legacy","","Boolean for Legacy"};
  Gaudi::Property<bool> m_PhaseI {this,"PhaseI","","Boolean for PhaseI"};
  Gaudi::Property<bool> m_ElectronChannel {this,"ElectronChannel","","Boolean for electron analysis"};
  Gaudi::Property<bool> m_MuonChannel {this,"MuonChannel","","Boolean for muon analysis"};
  Gaudi::Property<float> m_Luminosity {this,"Luminosity",{},"Luminosity"};
  
  
  bool m_isBG;
  bool m_isLArBurst;
  bool m_isL1_EM22VHIPassed;
  
  std::string m_selectionLabel;
  std::string m_overlapLabel;
  /*
  SG::ReadHandleKey<xAOD::ElectronContainer> m_ElectronContainerKey{ this, "ElectronContainerName", "Electrons", "" };
  SG::ReadHandleKey<xAOD::MuonContainer> m_MuonContainerKey{ this, "MuonContainerName", "Muons", "" };
  SG::ReadHandleKey<xAOD::PhotonContainer> m_PhotonContainerKey{ this, "PhotonContainerName", "Photons", "" };
  SG::ReadHandleKey<xAOD::VertexContainer> m_VertexContainerKey { "PrimaryVertices" };
  SG::ReadHandleKey<xAOD::JetContainer> m_JetContainerKey {"AntiKt4EMPFlowJets"};
  */
  
  //DAOD_PHYSLITE naming
  SG::ReadHandleKey<xAOD::ElectronContainer> m_ElectronContainerKey{ this, "ElectronContainerName", "AnalysisElectrons", "" };
  SG::ReadHandleKey<xAOD::PhotonContainer> m_PhotonContainerKey{ this, "PhotonContainerName", "AnalysisPhotons", "" };
  SG::ReadHandleKey<xAOD::JetContainer> m_JetPflowContainerKey{ this, "JetContainerName", "AnalysisJets", "" };
  SG::ReadHandleKey<xAOD::MuonContainer> m_MuonContainerKey{ this, "MuonContainerName", "AnalysisMuons", "" };
  //SG::ReadHandleKey<xAOD::TauJetContainer> m_TauJetContainerKey{ this, "TauJetContainerName", "AnalysisTauJets", "" };
  SG::ReadHandleKey<xAOD::VertexContainer> m_VertexContainerKey{ "PrimaryVertices" };
  
  
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool{this,"triggerDecTool1","","trigger decision tool"};
  ToolHandle<Trig::R3MatchingTool> m_r3MatchingTool{this, "R3MatchingTool", "Trig::R3MatchingTool", "R3MatchingTool"};
  ToolHandle<ORUtils::IOverlapRemovalTool> m_orTool{this, "OverlapRemovalTool", "", "ORTool"};
  
  ToolHandle<CP::IIsolationSelectionTool> m_isolationSelectionTool;
  
  ToolHandle<CP::IMuonSelectionTool> m_MuonSelectionTool;
  
  ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_EgammaCalibrationAndSmearingTool;
  ToolHandle<CP::MuonCalibTool> m_MuonCalibTool;
  asg::AnaToolHandle<IJetCalibrationTool> m_JetCalibrationTool;
  JetCleaningTool *m_JetCleaningTool;
  
  
  ToolHandle<IEGammaAmbiguityTool> m_AmbiguityTool{this, "AmbiguityTool", "EGammaAmbiguityTool/egammaambiguitytool", "Tool to resolve ambiguitites"};

  // borrowing this from AthMonitorAlgorithm
  Gaudi::Property<std::string> m_triggerChainString {this,"TriggerChain","","comma separated list of chains"}; ///< Trigger chain string pulled from the job option and parsed into a vector
  std::vector<std::string> m_vTrigChainNames; ///< Vector of trigger chain names parsed from trigger chain string
  StatusCode parseList( const std::string& line, std::vector<std::string>& result ) const;
  void unpackTriggerCategories( std::vector<std::string>& vTrigChainNames ) const;
  //m_isL1_EM22VHIPassed=false;

};

#endif
