# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def WyyAnalysisCfg(flags, analysisName, **kwargs):
    """
    Configure an analysis algorithm
    """
    acc = ComponentAccumulator()
   
    if analysisName == 'enuyyMC':
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs) 
        alg.ElectronChannel=True
        alg.MuonChannel=False
        alg.xsection = 2.1119*1e3
        alg.SoW = 1209805496349.081543
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'munuyyMC':
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=False
        alg.MuonChannel=True
        alg.xsection = 2.1119*1e3
        alg.SoW = 1064026335905.776001
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'eeyyMC': 
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=True
        alg.MuonChannel=False
        alg.xsection = 1.373*1e3
        alg.SoW = 295535410173.899292
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'mumuyyMC': 
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=False
        alg.MuonChannel=True
        alg.xsection = 1.3705*1e3
        alg.SoW = 499071825651.845276
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'eeyMC': 
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=True
        alg.MuonChannel=False
        alg.xsection = 102.74*1e3
        alg.SoW = 56795975498823.828125
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'mumuyMC': 
        alg = CompFactory.WyyMCAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=False
        alg.MuonChannel=True
        alg.xsection = 102.64*1e3
        alg.SoW = 57009666306413.898438
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'enuyyData': 
        alg = CompFactory.WyyDataAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=True
        alg.MuonChannel=False
        alg.doScaling=False
        alg.xsection = 102.64*1e3
        alg.Legacy = True
        alg.PhaseI = False
        
    elif analysisName == 'munuyyData': 
        alg = CompFactory.WyyDataAnalysis(analysisName, **kwargs)
        alg.ElectronChannel=False
        alg.MuonChannel=True
        alg.doScaling=False
        alg.xsection = 102.64*1e3
        alg.Legacy = True
        alg.PhaseI = False
    
    elif analysisName == 'eyyTrigLegacy':
      alg = CompFactory.WyyTrigEfficiency(analysisName, **kwargs)
      alg.ElectronChannel=True
      alg.MuonChannel=False
      alg.Legacy = True
      alg.PhaseI = False
      
    elif analysisName == 'eyyTrigPhaseI':
      alg = CompFactory.WyyTrigEfficiency(analysisName, **kwargs)
      alg.ElectronChannel=True
      alg.MuonChannel=False
      alg.Legacy = False
      alg.PhaseI = True
    
    elif analysisName == 'muyyTrigLegacy':
      alg = CompFactory.WyyTrigEfficiency(analysisName, **kwargs)
      alg.ElectronChannel=False
      alg.MuonChannel=True
      alg.Legacy = True
      alg.PhaseI = False
    
    elif analysisName == 'muyyTrigPhaseI':
      alg = CompFactory.WyyTrigEfficiency(analysisName, **kwargs)
      alg.ElectronChannel=False
      alg.MuonChannel=True
      alg.Legacy = False
      alg.PhaseI = True
    
  
        
    else:
        alg = None


    #General flags
    alg.Luminosity = 140.
    alg.DAODPhys = False
    
    
    # Trigger Decision Tool TDT
    # The hasattr() function returns True if the specified object has the specified attribute, otherwise False
    if hasattr(alg, 'triggerDecTool1'):
        from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
        tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(flags))
        tdt.AcceptMultipleInstance=True
        alg.triggerDecTool1 = tdt
    
    # Matching Tool
    if hasattr(alg, 'r3MatchingTool'):
        r3mt = acc.popToolsAndMerge(r3MatchingToolCfg(flags))
        alg.r3MatchingTool = r3mt
        
    # Energy Calib and Smearing Tools - Electrons and Photons
    if hasattr(alg, 'EgammaCalibrationAndSmearingTool'):
        from ElectronPhotonFourMomentumCorrection.EgammaCalibCfg import EgammaCalibToolCfg
        EgammaCalibrationAndSmearingTool = acc.popToolsAndMerge(EgammaCalibToolCfg(flags,ESModel = "es2022_R22_PRE",decorrelationModel= "FULL_v1",randomRunNumber=123456))
        #EgammaCalibrationAndSmearingTool = acc.popToolsAndMerge(EgammaCalibrationAndSmearingToolCfg(flags))
        alg.EgammaCalibrationAndSmearingTool = EgammaCalibrationAndSmearingTool
    
    
    if hasattr(alg, 'MuonSelectionTool'):
        from MuonSelectorTools.MuonSelectorToolsConfig import MuonSelectionToolCfg
        MuonSelectionTool = acc.popToolsAndMerge(MuonSelectionToolCfg(flags,MuQuality=1,MaxEta=2.4,IsRun3Geo= True,ExcludeNSWFromPrecisionLayers=True))
        alg.MuonSelectionTool = MuonSelectionTool
       
    if hasattr(alg, 'MuonCalibTool'):
        MuonCalibTool = acc.popToolsAndMerge(MuonCalibToolCfg(flags)) #MaxEta=2.7,MuQuality=1,ExcludeNSWFromPrecisionLayers=True
        alg.MuonCalibTool = MuonCalibTool

        
    if hasattr(alg, 'IsolationSelectionTool'):
        IsolationSelectionTool = acc.popToolsAndMerge(IsolationSelectionToolCfg(flags))
        alg.IsolationSelectionTool = IsolationSelectionTool  
        alg.IsolationSelectionTool.ElectronWP = "Tight_VarRad"  # Loose_VarRad
        alg.IsolationSelectionTool.PhotonWP = "TightCaloOnly" #FixedCutTight
        alg.IsolationSelectionTool.MuonWP = "Tight_FixedRad"


    #add Overlap Removal tool
    if hasattr(alg, 'OverlapRemovalTool'):
        from AssociationUtils.AssociationUtilsConfig import OverlapRemovalToolCfg
        OverlapRemovalTool = acc.popToolsAndMerge(OverlapRemovalToolCfg(flags,inputLabel='selected',outputLabel='overlaps',doMuons=True,doTaus=False,doElectrons=True,doPhotons=True,doJets=True))
        alg.OverlapRemovalTool = OverlapRemovalTool
  
        
    # Try some LAr config for reading/masking supercells
    from AthenaConfiguration import Enums
    if flags.Common.Project is Enums.Project.Athena:
        from LArGeoAlgsNV.LArGMConfig import LArGMCfg
        acc.merge(LArGMCfg(flags))
 
    """
    if alg.DAODPhyslite:   
        from LArBadChannelTool.LArBadChannelConfig import LArBadChannelCfg
        #acc.merge(LArBadChannelCfg(flags))
        acc.merge(LArBadChannelCfg(flags,isSC=True,tag="LARBadChannelsBadChannelsSC-RUN3-UPD1-00") )

        from DetDescrCnvSvc.DetDescrCnvSvcConfig import DetDescrCnvSvcCfg
        acc.merge(DetDescrCnvSvcCfg(flags))
    """
    
    
    # work out the file version if required
    if hasattr(alg, 'daodVersion'):
        vers = 100
        for file in flags.Input.Files:
            vpos = file.rfind('.v')
            vers_str = file[vpos+2:vpos+5] if (vpos > -1) else vers
            alg.daodVersion = int(vers_str)
            break

    # check primary?
    acc.addEventAlgo(alg, 'AthAlgSeq', primary=True)    

    print('Adding algorithm:',alg)

    return acc
  
  
  
def MuonSelectionToolCfg(flags):
    acc = ComponentAccumulator()
    
    MuonSelectionTool = CompFactory.CP.MuonSelectionTool 
    acc.setPrivateTools(MuonSelectionTool(MuQuality=1,MaxEta=2.4,IsRun3Geo= True,ExcludeNSWFromPrecisionLayers=True)) 
    return acc


def MuonCalibToolCfg(flags):
    acc = ComponentAccumulator()
    MuonCalibTool = CompFactory.CP.MuonCalibTool
    
    acc.setPrivateTools(MuonCalibTool(calibMode=1,IsRun3Geo= True)) #release="Recs2023_08_28_Run2Run3"
    return acc
  

def IsolationSelectionToolCfg(flags):
    acc = ComponentAccumulator()
    
    IsolationSelectionTool = CompFactory.CP.IsolationSelectionTool
    acc.setPrivateTools(IsolationSelectionTool())
    return acc
  
def r3MatchingToolCfg(flags):
    acc = ComponentAccumulator()

    r3MatchingTool = CompFactory.Trig.R3MatchingTool("R3MatchingTool")
    acc.setPrivateTools(r3MatchingTool())

    return acc

if __name__ == '__main__':
    from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags
    #from AthenaCommon.Logging import logging
    import glob
    import sys,os

    import argparse
    parser = argparse.ArgumentParser(prog='python -m WyyAnalysis.WyyAnalysisConfig',
                                   description="""For running Wyy Analyses.\n\n
                                   Example: python -m  WyyAnalysis.WyyAnalysisConfig --filesInput "mc23*" --evtMax 10""")
    parser.add_argument('--evtMax',type=int,default=-1,help="number of events")
    parser.add_argument('--filesInput',nargs='+',help="input files")
    parser.add_argument('--outputLevel',default="WARNING",choices={ 'INFO','WARNING','DEBUG','VERBOSE'})
    parser.add_argument('--analyses',nargs='+', \
                        choices={"enuyyMC","munuyyMC","eeyyMC","mumuyyMC","eeyMC","mumuyMC","enuyyData","munuyyData","eyyTrigLegacy","eyyTrigPhaseI","muyyTrigLegacy","muyyTrigPhaseI"}, \
                        default=["muyyTrigLegacy"], help="Which analysis to run")
    parser.add_argument('--analysesArgs',nargs='+',default=["ElectronChannel:False,,,MuonChannel:True"], help="'dictionary' of analysis arguments")
    args = parser.parse_args()

    print('analyses list:',args.analyses)
    print('analysesArgs list:',args.analysesArgs)

    # create a list of analysis argument dictionaries
    analysesArgsDictList = []
    for analysisArgs in args.analysesArgs:
        #print('analysisArgs',analysisArgs,type(analysisArgs))
        analysisDict = {}        
        args_split = analysisArgs.split(',,,')
        for aargs in args_split:            
            # analyses without arguments append empty dictionaries
            if not aargs:
                continue
            bargs = aargs.split(':')
            analysisDict[bargs[0]] = bargs[1]
        
        analysesArgsDictList.append(analysisDict)

    print('List of analysis argument dictionaries:  ',analysesArgsDictList)
    
    
    # Sending to farm configuration part
    if os.getenv('CONDOR_ATH_INPUT'):
        flags.Exec.MaxEvents = -1
        flags.Input.Files = os.getenv('CONDOR_ATH_INPUT').split(' ')
    else:
        flags.Input.Files = [file for x in args.filesInput for file in glob.glob(x)]
        flags.Exec.MaxEvents = args.evtMax
        from AthenaCommon import Constants
        algLogLevel = getattr(Constants,args.outputLevel)
        flags.Exec.OutputLevel = algLogLevel

    flags.Trigger.EDMVersion = 3

    flags.Output.HISTFileName = "Wyy_PHYSLITE.root"
    #flags.Output.HISTFileName = "Wyy_PHYS.root"
    #include("AthAnalysisBaseComps/SuppressLogging.py") #reduces printout from athena
    flags.lock()

    # Set up the main service "acc"
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(flags)
    
    # read the file
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(flags))

    # steer the printout level
    acc.getService(acc.getAppProps()["EventLoop"]).EventPrintoutInterval = 50000
    
    # ensure histsvc is set up
    THistSvc = CompFactory.THistSvc
    histsvc = THistSvc()
    if flags.Output.HISTFileName:
        histsvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % ('ANALYSIS',flags.Output.HISTFileName)]
    acc.addService(histsvc)

    # Loop over the supplied analyses list, append any arguments and merge analysis
    ianalysis = 0
    for analysis in args.analyses:        
        analysisArgs = analysesArgsDictList[ianalysis] if analysesArgsDictList else None
        analysis_kwargs={}
        if analysisArgs: 
            analysis_kwargs.update(analysisArgs) 
        analysisAlg = acc.merge(WyyAnalysisCfg(flags, analysis, **analysis_kwargs))
        ianalysis = ianalysis + 1

    print('About to run, the configuration is as follows: ')
    acc.printConfig(withDetails = True, summariseProps = True, printDefaults = True)

    if acc.run().isFailure():
        sys.exit(1)
